import React from 'react';
import {Footer, FooterTab, Button} from 'native-base';

import Styles from './styles';
import sitemap from '../../route/sitemap';

import {
  ActivePawIcon,
  InactivePawIcon,
  ActiveSyringeIcon,
  InactiveSyringeIcon,
  ActivePlateIcon,
  InactivePlateIcon,
} from '../../assets/icons';
import {SvgProps} from 'react-native-svg';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {AppRootReducer} from '../../reducers';
import {setBottomTab} from '../../reducers/animal.reducer';

/**
 * Footer Bottom Navigator
 *
 * @returns {JSX.Element}
 */
interface IBottomRoutes {
  activeIcon: React.FC<SvgProps>;
  inactiveIcon: React.FC<SvgProps>;
  route: string;
}

const FooterBottomNavigator: React.FC = (): JSX.Element => {
  /* Redux */
  const {
    animalReducer: {tab},
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* Router */
  const navigation = useNavigation();
  const bottomRoutes: IBottomRoutes[] = [
    {
      activeIcon: ActivePawIcon,
      inactiveIcon: InactivePawIcon,
      route: sitemap.animal,
    },
    {
      activeIcon: ActiveSyringeIcon,
      inactiveIcon: InactiveSyringeIcon,
      route: sitemap.report,
    },
    {
      activeIcon: ActivePlateIcon,
      inactiveIcon: InactivePlateIcon,
      route: sitemap.care,
    },
  ];

  /* Function */
  const chandleTab = (element: IBottomRoutes, currentTab: number) => {
    dispatch(setBottomTab(currentTab));
    navigation.navigate(element.route);
  };
  return (
    <Footer style={Styles.footer}>
      {bottomRoutes.map((element: IBottomRoutes, i: number) => {
        const BottomIcon =
          tab === i ? element.activeIcon : element.inactiveIcon;
        return (
          <FooterTab key={i} style={Styles.footerTab}>
            <Button full onPress={() => chandleTab(element, i)}>
              <BottomIcon width={35} height={35} />
            </Button>
          </FooterTab>
        );
      })}
    </Footer>
  );
};

export {FooterBottomNavigator};
