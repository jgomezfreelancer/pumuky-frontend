import {StyleSheet} from 'react-native';
import {color} from 'react-native-reanimated';
import colors from '../../styles/colors';

export default StyleSheet.create({
  footer: {borderTopColor: colors.greyBorder, borderTopWidth: 1},
  footerTab: {
    backgroundColor: colors.bgGrey,
  },
  icon: {
    borderColor: colors.grey,
    borderWidth: 2,
    borderRadius: 25,
    padding: 8,
    paddingLeft: 12,
  },
  activeIcon: {
    borderColor: colors.blue,
  },
  tabText: {
    color: colors.blue,
  },
});
