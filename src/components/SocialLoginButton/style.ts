import {StyleSheet, TextStyle, ViewStyle} from 'react-native';
import colors from '../../styles/colors';

interface Styles {
  google: ViewStyle;
  facebook: ViewStyle;
  buttonText: TextStyle;
  buttonContainer: ViewStyle;
}

const buttonStyles: ViewStyle = {
  borderRadius: 30,
  borderWidth: 1,
  alignItems: 'center',
  justifyContent: 'center',
  width: '100%',
  margin: 5,
};

const SocialLoginButtonStyles: Styles = {
  google: {
    ...buttonStyles,
    backgroundColor: '#ed6e6e',
    borderColor: '#ed6e6e',
  },
  facebook: {
    ...buttonStyles,
    backgroundColor: colors.blueFacebook,
    borderColor: colors.blueFacebook,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 15,
  },
};

export default StyleSheet.create(SocialLoginButtonStyles);
