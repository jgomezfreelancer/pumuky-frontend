import React from 'react';

import {Text, TouchableHighlight, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';
import styles from '../FooterBottomNavigator/styles';

import Style from './style';

interface IProps {
  socialNetwork: 'google' | 'facebook';
  handleAction: () => void;
  loading?: boolean;
  label: string;
  icon: string;
}

export const SocialLoginButton = (props: IProps) => {
  const {handleAction, socialNetwork, loading = false} = props;

  return (
    <TouchableOpacity
      style={Style[socialNetwork]}
      onPress={handleAction}
      disabled={loading}>
      <View style={Style.buttonContainer}>
        <View style={{width: '10%', paddingLeft: 5}}>
          <Icon name={props.icon} color="white" size={20} />
        </View>
        <View style={{width: '90%'}}>
          <Text style={Style.buttonText}>{props.label}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};
