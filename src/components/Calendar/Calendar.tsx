import {Body, Card, CardItem} from 'native-base';
import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {Agenda} from 'react-native-calendars';
import {TouchableOpacity} from 'react-native-gesture-handler';

const Calendar: React.FC = () => {
  const [items, setItems] = useState({});

  const timeToString = (time: any) => {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  };

  const loadItems = (day: any) => {
    setTimeout(() => {
      for (let i = -15; i < 85; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime: any = timeToString(time);
        if (!items[strTime]) {
          items[strTime] = [];
          const numItems = Math.floor(Math.random() * 3 + 1);
          for (let j = 0; j < numItems; j++) {
            items[strTime].push({
              name: 'Item for ' + strTime + ' #' + j,
              height: Math.max(50, Math.floor(Math.random() * 150))
            });
          }
        }
      }
      const newItems: any = {};
      Object.keys(items).forEach((key: any) => {
        newItems[key] = items[key];
      });
      setItems(newItems);
    }, 1000);
  };

  const renderItem = (item: any) => {
    return (
      <Card style={{padding: 10, marginRight: 10, marginTop: 17}}>
        <CardItem button>
          <Body>
            <Text>{item.name}</Text>
          </Body>
        </CardItem>
      </Card>
    );
  };

  return (
    <View style={{flex: 1}}>
      <Agenda
        items={items}
        loadItemsForMonth={loadItems}
        selected={'2017-05-16'}
        renderItem={renderItem}
        onDayPress={(day) => {}}
        onCalendarToggled={(calendarOpened) => {}}
      />
    </View>
  );
};

export {Calendar};
