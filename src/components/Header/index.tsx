import React, {useCallback} from 'react';

import {View, Text, TouchableHighlight} from 'react-native';
import {Header} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

import Styles from './style';

interface IProps {
  handleLeft?: (() => void) | null;
  handleRigth?: (() => void) | null;
  leftColor?: string;
  leftTitle?: string;
  rightTitle?: string;
  rightColor?: string;
  leftIcon?: string;
  rightIcon?: string;
  title?: string;
}

const CustomHeader = (props: IProps) => {
  const {
    handleLeft,
    handleRigth,
    leftColor = 'white',
    leftTitle,
    rightTitle,
    rightColor = '#e74c3c',
    leftIcon = 'user',
    rightIcon = 'power-off',
    title,
  } = props;

  const left = useCallback(() => {
    if (handleLeft) {
      handleLeft();
    }
  }, [handleLeft]);

  const right = useCallback(() => {
    if (handleRigth) {
      handleRigth();
    }
  }, [handleRigth]);

  return (
    <Header style={Styles.header}>
      <View style={Styles.headerContainer}>
        <View style={Styles.itemContainer}>
          {handleLeft && (
            <TouchableHighlight
              style={Styles.leftIcon}
              onPress={left}
              underlayColor="transparent">
              {leftTitle ? (
                <Text style={Styles.cornerLeftTitle}>{leftTitle}</Text>
              ) : (
                <Icon name={leftIcon} size={25} color={leftColor} />
              )}
            </TouchableHighlight>
          )}
        </View>

        <View style={Styles.titleContainer}>
          <Text style={Styles.title}>{title}</Text>
        </View>
        <View style={{...Styles.itemContainer, marginRight: 10}}>
          {handleRigth && (
            <TouchableHighlight
              style={Styles.rightIcon}
              onPress={right}
              underlayColor="transparent">
              {rightTitle ? (
                <Text style={{...Styles.cornerRightTitle, color: rightColor}}>
                  {rightTitle}
                </Text>
              ) : (
                <Icon name={rightIcon} size={25} color={rightColor} />
              )}
            </TouchableHighlight>
          )}
        </View>
      </View>
    </Header>
  );
};

export default CustomHeader;
