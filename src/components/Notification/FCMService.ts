import messaging, {
  FirebaseMessagingTypes
} from '@react-native-firebase/messaging';
import {localNotificationService} from './LocalNotificationService';

interface RegisterService {
  onRegister: (token: string) => void;
  onNotification: (remoteMessage: FirebaseMessagingTypes.RemoteMessage) => void;
  onOpenNotification: (
    remoteMessage: FirebaseMessagingTypes.RemoteMessage
  ) => void;
  backgroundMessageHandler: (
    remoteMessage: FirebaseMessagingTypes.RemoteMessage
  ) => void;
}

class FCMService {
  messageListener: any;

  register = (
    onRegister: RegisterService['onRegister'],
    onNotification: RegisterService['onNotification'],
    onOpenNotification: RegisterService['onOpenNotification'],
    backgroundMessageHandler: RegisterService['backgroundMessageHandler']
  ) => {
    this.checkPermission(onRegister);
    this.createNotificationListeners(
      onRegister,
      onNotification,
      onOpenNotification,
      backgroundMessageHandler
    );
  };

  registerAppWithFCM = async () => {
    await messaging().registerDeviceForRemoteMessages();
  };

  checkPermission = (onRegister: RegisterService['onRegister']) => {
    messaging()
      .hasPermission()
      .then((enabled: any) => {
        if (enabled) {
          // User has permission
          this.getToken(onRegister);
        } else {
          // User don't have permission
          this.requestPermission(onRegister);
        }
      })
      .catch((error: any) => new Error(error));
  };

  getToken = (onRegister: RegisterService['onRegister']) => {
    messaging()
      .getToken()
      .then((fcmToken: any) => {
        if (fcmToken) {
          onRegister(fcmToken);
        } else {
          /* eslint-disable */
          console.log('[FCMService] User does not have a devices token');
          /* eslint-enable */
        }
      })
      .catch((error: any) => new Error(error));
  };

  requestPermission = (onRegister: RegisterService['onRegister']) => {
    messaging()
      .requestPermission()
      .then(() => {
        this.getToken(onRegister);
      })
      .catch((error: any) => new Error(error));
  };

  deleteToken = () => {
    messaging()
      .deleteToken()
      .catch((error: any) => new Error(error));
  };

  createNotificationListeners = (
    onRegister: RegisterService['onRegister'],
    onNotification: RegisterService['onNotification'],
    onOpenNotification: RegisterService['onOpenNotification'],
    backgroundMessageHandler: RegisterService['backgroundMessageHandler']
  ) => {
    // When Application Running on Background
    messaging().onNotificationOpenedApp(
      (remoteMessage: FirebaseMessagingTypes.RemoteMessage) => {
        if (remoteMessage) {
          const notification = remoteMessage;
          onOpenNotification(notification);
        }
      }
    );

    // Recieve notification when the device is silent
    messaging().setBackgroundMessageHandler(
      async (remoteMessage: FirebaseMessagingTypes.RemoteMessage) =>
        backgroundMessageHandler(remoteMessage)
    );

    // When Application open from quit state
    messaging()
      .getInitialNotification()
      .then((remoteMessage: any) => {
        if (remoteMessage) {
          const notification = remoteMessage;
          localNotificationService.cancelAllLocalNotifications();
          onOpenNotification(notification);
        }
      })
      .catch((err: any) => err);

    // Forground state message
    this.messageListener = messaging().onMessage(
      async (remoteMessage: FirebaseMessagingTypes.RemoteMessage) => {
        onNotification(remoteMessage);
      }
    );

    // Triggered when have new Token
    messaging().onTokenRefresh((fcmToken: any) => {
      onRegister(fcmToken);
    });
  };

  unRegister = () => {
    this.messageListener();
  };
}

export const fcmService = new FCMService();
