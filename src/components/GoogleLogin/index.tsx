import React, {useEffect, FunctionComponent} from 'react';

import {
  GoogleSignin,
  statusCodes,
  User
} from '@react-native-community/google-signin';

import Toast from 'react-native-toast-message';
import {SocialLoginButton} from '../SocialLoginButton/SocialLoginButton';
import {configureGoogleSignIn} from './configureGoogleSignIn';

interface IProps {
  handleLogin: (user: User) => void;
  loading: boolean;
}

const GoogleLogin: FunctionComponent<IProps> = ({
  handleLogin,
  loading = false
}) => {
  useEffect(configureGoogleSignIn, []);

  const signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      handleLogin(userInfo);
    } catch (error) {
      console.log('error', error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        Toast.show({
          text1: 'play services not available or outdated',
          type: 'error'
        });
      } else {
        // some other error happened
      }
    }
  };

  return (
    <SocialLoginButton
      loading={loading}
      socialNetwork="google"
      handleAction={signIn}
      label="Continuar con Google"
      icon="google"
    />
  );
};

export default GoogleLogin;
