import {GoogleSignin} from '@react-native-community/google-signin';

export const configureGoogleSignIn = () => {
  GoogleSignin.configure({
    webClientId:
      '988368352997-cr5nbjsmdlgqpei01n2v47h19ov7vsro.apps.googleusercontent.com',
    offlineAccess: true,
    forceCodeForRefreshToken: true,
    iosClientId:
      '988368352997-e0hjqc6qbnln04nteu6erp8a2cdr8gkt.apps.googleusercontent.com'
  });
};
