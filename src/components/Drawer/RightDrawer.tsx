import {Drawer} from 'native-base';
import React from 'react';
import {View, Text} from 'react-native';

const RightDrawer: React.FC = (): JSX.Element => {
  return (
    <Drawer
      content={
        <View>
          <Text>example</Text>
        </View>
      }
      onClose={() => null}></Drawer>
  );
};

export {RightDrawer};
