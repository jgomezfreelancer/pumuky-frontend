import {StyleSheet, TextStyle, ViewStyle} from 'react-native';
import {isIOS} from '../../../../helpers/isIOS';
import colors from '../../../../styles/colors';

interface Styles {
  customInputContainer: ViewStyle;
  container: ViewStyle;
  inputContainer: ViewStyle;
  messageContainer: ViewStyle;
  error: TextStyle;
  input: TextStyle;
  fieldRequired: TextStyle;
  iconContainer: ViewStyle;
}

export const inputStyles: Styles = {
  customInputContainer: {position: 'relative'},
  container: {
    width: '100%',
  },
  inputContainer: {position: 'relative'},
  messageContainer: {
    margin: 10,
    marginTop: 0,
    marginBottom: 0,
  },
  error: {
    color: 'red',
    fontWeight: 'bold',
  },
  input: {
    color: '#000000',
    fontSize: 16,
    backgroundColor: '#ffffff',
    textAlign: 'left',
    width: '100%',
    borderWidth: 1,
    borderColor: colors.borderGrey,
    paddingVertical: 0,
  },
  iconContainer: {
    position: 'absolute',
    top: '28%',
    left: '4%',
  },
  fieldRequired: {
    borderColor: 'red',
    borderWidth: 2,
  },
};

export default StyleSheet.create(inputStyles);
