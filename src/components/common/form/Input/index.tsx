import React, {FunctionComponent} from 'react';
import {
  View,
  Text,
  NativeSyntheticEvent,
  TextInputFocusEventData,
  TextInput
} from 'react-native';
import {Controller} from 'react-hook-form';
import styles from './styles';
import colors from '../../../../styles/colors';

type PropFields = {
  control: any;
  name: string;
  required?: boolean;
  error: any;
  placeholder?: string;
  secureText?: boolean;
  message?: boolean;
  iconName?: string;
  textAlign?: 'left' | 'right' | 'center';
  editable?: boolean;
  onInputBlur?:
    | ((e: NativeSyntheticEvent<TextInputFocusEventData>) => void)
    | undefined;
  customBlue?: boolean;
};

const Input: FunctionComponent<PropFields> = ({
  control,
  name,
  required = false,
  error = null,
  placeholder = '',
  secureText = false,
  message,
  textAlign = 'left',
  editable,
  onInputBlur,
  customBlue
}) => {
  const getInputStyles = () => {
    let inputStyles = styles.input;
    if (!message && error) {
      inputStyles = {...styles.input, ...styles.fieldRequired};
    }
    if (customBlue) {
      inputStyles = {...inputStyles, color: colors.blue, fontWeight: 'bold'};
    }

    return inputStyles;
  };
  const InputContainer = (props: any) => {
    const {onChange, value} = props;
    return (
      <View style={styles.container}>
        {error && message && (
          <View style={styles.messageContainer}>
            <Text style={styles.error}>{error && error}</Text>
          </View>
        )}
        <View style={styles.customInputContainer}>
          <View style={styles.inputContainer}>
            <TextInput
              onEndEditing={onInputBlur}
              onChangeText={onChange}
              value={value}
              placeholder={placeholder}
              secureTextEntry={secureText}
              style={getInputStyles()}
              placeholderTextColor="#c8c8c8"
              editable={editable}
              textAlign={textAlign}
            />
          </View>
        </View>
      </View>
    );
  };

  return (
    <Controller
      control={control}
      render={InputContainer}
      name={name}
      rules={{required: required ? 'Requerido' : false}}
      defaultValue=""
    />
  );
};

export default Input;
