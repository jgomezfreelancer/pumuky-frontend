import React, { FunctionComponent } from 'react';
import { View, Text } from 'react-native';
import { Controller } from 'react-hook-form';
import { TextInput } from 'react-native-gesture-handler';

import styles from './styles';

type PropFields = {
  control: any;
  name: string;
  required?: boolean;
  error: any;
  placeholder?: string;
  secureText?: boolean;
  message?: boolean;
  iconName?: string;
  align?: 'left' | 'right';
  inputType?: string;
};

const Input3: FunctionComponent<PropFields> = ({
  control,
  name,
  required = false,
  error = null,
  placeholder = '',
  secureText = false,
}) => {
  const getInputStyles = () => {
    let inputStyles = styles.input;
    if (error) {
      inputStyles = { ...styles.input, ...styles.fieldRequired };
    }
    return inputStyles;
  };
  const InputContainer = (props: any) => {
    const { onBlur, onChange, value } = props;
    return (
      <View style={styles.container}>
        {error && (
          <View style={styles.messageContainer}>
            <Text style={styles.error}>{error && error}</Text>
          </View>
        )}
        <View style={styles.customInputContainer}>
          <View style={styles.inputContainer}>
            <TextInput
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
              placeholder={placeholder}
              secureTextEntry={secureText}
              style={getInputStyles()}
              placeholderTextColor="#c8c8c8"
            />
          </View>
        </View>
      </View>
    );
  };

  return (
    <Controller
      control={control}
      render={InputContainer}
      name={name}
      rules={{
        required: required ? 'Requerido' : false,
      }}
      defaultValue=""
    />
  );
};

export default Input3;
