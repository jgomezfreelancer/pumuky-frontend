import { StyleSheet, TextStyle, ViewStyle } from 'react-native';
import { isIOS } from '../../../../helpers/isIOS';
import colors from '../../../../styles/colors';

interface Styles {
  customInputContainer: ViewStyle;
  container: ViewStyle;
  inputContainer: ViewStyle;
  messageContainer: ViewStyle;
  error: TextStyle;
  input: TextStyle;
  fieldRequired: TextStyle;
  iconContainer: ViewStyle;
}

export const inputStyles: Styles = {
  customInputContainer: { position: 'relative' },
  container: {
    width: '100%',
  },
  inputContainer: { position: 'relative' },
  messageContainer: {
    margin: 10,
    marginTop: 0,
    marginBottom: 0,
  },
  error: {
    color: 'red',
    fontWeight: 'bold',
  },
  input: {
    color: 'black',
    fontSize: 16,
    backgroundColor: colors.white,
    paddingTop: isIOS ? 0 : 15,
    height: 42,
    textAlign: 'left',
    width: '100%',
    borderColor: '#efefef',
  },
  iconContainer: {
    position: 'absolute',
    top: '28%',
    left: '4%',
  },
  fieldRequired: {
    borderColor: '#e74c3c',
    borderWidth: 1,
  },
};

export default StyleSheet.create(inputStyles);
