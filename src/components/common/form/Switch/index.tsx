import React, { FunctionComponent } from 'react';
import { View, Switch } from 'react-native';
import { Controller } from 'react-hook-form';

import styles from './styles';

type PropFields = {
  control: any;
  name: string;
};

type SwitchProps = {
  onChange: (value: boolean) => void;
  value: boolean;
};

const CustomSwitch: FunctionComponent<PropFields> = ({ control, name }) => {
  const SwitchContainer = (props: SwitchProps) => {
    const { onChange, value } = props;
    return (
      <View style={styles.container}>
        <Switch
          thumbColor="#fff"
          trackColor={{ true: '#44d1e7', false: 'grey' }}
          onValueChange={onChange}
          value={value}
        />
      </View>
    );
  };
  return <Controller control={control} render={SwitchContainer} name={name} defaultValue={false} />;
};

export default CustomSwitch;
