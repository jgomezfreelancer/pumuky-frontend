import { StyleSheet } from 'react-native';
import { BLUE } from '../../../styles/colors';

export default StyleSheet.create({
  header: {
    backgroundColor: 'white',
    elevation: 0,
    borderBottomColor: '#c8c8c8',
    borderBottomWidth: 1,
    paddingLeft: 0,
    paddingRight: 0,
  },
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  leftIcon: {
    paddingLeft: 10,
  },
  rightIcon: {},
  title: {
    color: '#9c9c9c',
    fontSize: 20,
    textAlign: 'center',
    paddingBottom: 10,
    paddingTop: 10,
  },
  cornerLeftTitle: {
    fontWeight: 'bold',
    color: BLUE,
    fontSize: 17,
    paddingLeft: 10,
  },
  cornerRightTitle: {
    fontWeight: 'bold',
    fontSize: 17,
    textAlign: 'center',
  },
  itemContainer: { flex: 1 }
});
