import { StyleSheet } from 'react-native';
import colors from '../../../styles/colors';

export default StyleSheet.create({
  inputContainer: {
    width: '85%',
  },
  button: {
    borderColor: colors.greyBorder,
    borderWidth: 1,
    width: '100%',
    paddingTop: 5,
    paddingBottom: 5,
  },
  buttonContainer: {
    width: '15%',
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'flex-end',
    alignItems: 'flex-end',
  },
  root: { width: '100%' },
  fieldContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
  },
  itemContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  itemsContainer: {
    width: '100%',
    marginTop: 15,
  },
  itemTextContainer: {
    width: '90%',
  },
  itemText: {
    color: colors.blackLight,
  },
  plusButton: {
    backgroundColor: colors.blue,
    borderRadius: 5,
    padding: 8,
    paddingTop: 7,
    paddingBottom: 7
  },
  trashIconContainer: {
    width: '10%',
  },
});
