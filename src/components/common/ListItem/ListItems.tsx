import React, { useRef, useState } from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import colors from '../../../styles/colors';
import Styles from './style';

interface Props {
  selected?: string[];
  onChange: (list: string[]) => void;
  placeholder?: string;
}

const ListItems: React.FC<Props> = ({
  selected = [],
  onChange,
  placeholder,
}): JSX.Element => {
  /* States */
  const [field, setField] = useState<string>('');
  const [list, setList] = useState<string[]>(selected);
  const inputRef = useRef<TextInput | null>(null);

  /* Functions */

  const onChangeField = (text: string) => setField(text);

  const addItem = () => {
    const newList = [...list];
    if (field !== '') {
      newList.push(field);
      setList(newList);
      onChange(newList);
      setField('');
    }
    inputRef.current && inputRef.current.focus();
  };

  const remove = (listItem: string) => {
    const currentList = [...list];
    const newList = currentList.filter(
      (element: string) => element !== listItem,
    );
    setList(newList);
    onChange(newList);
  };

  return (
    <View style={Styles.root}>
      <View style={Styles.fieldContainer}>
        <View style={Styles.inputContainer}>
          <TextInput
            onChangeText={onChangeField}
            value={field}
            placeholderTextColor="#c8c8c8"
            style={Styles.button}
            placeholder={placeholder}
            ref={inputRef}
          />
        </View>
        <View style={Styles.buttonContainer}>
          <TouchableOpacity style={Styles.plusButton} onPress={addItem}>
            <Icon name="plus" color={colors.white} size={25} solid />
          </TouchableOpacity>
        </View>
      </View>
      <View style={Styles.itemsContainer}>
        {list.map((element: string, i: number) => (
          <View key={i} style={Styles.itemContainer}>
            <View style={Styles.trashIconContainer}>
              <TouchableOpacity onPress={() => remove(element)}>
                <Icon name="trash" color={colors.blue} size={20} />
              </TouchableOpacity>
            </View>
            <View style={Styles.itemTextContainer}>
              <Text style={Styles.itemText}>-{element}</Text>
            </View>
          </View>
        ))}
      </View>
    </View>
  );
};

export { ListItems };
