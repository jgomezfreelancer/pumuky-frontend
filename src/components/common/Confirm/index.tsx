import React, {FunctionComponent} from 'react';
import {Text, View} from 'react-native';
import {useDispatch} from 'react-redux';
import setModal from '../../../actions/modal.action';

import Styles from './style';

type Props = {
  handleConfirm: () => void;
  leftText: string;
  rightText: string;
  message: string;
};

const Confirm: FunctionComponent<Props> = ({
  handleConfirm,
  leftText,
  rightText,
  message,
}) => {
  const dispatch = useDispatch();
  const close = () => {
    setModal({
      payload: {
        status: false,
        children: null,
      },
    })(dispatch);
  };
  return (
    <View>
      <View style={Styles.messageContainer}>
        <Text style={Styles.messageText}>{message}</Text>
      </View>
      <View style={Styles.optionsContainer}>
        <View style={Styles.halfGrid}>
          <Text style={Styles.optionsText} onPress={close}>
            {leftText}
          </Text>
        </View>
        <View style={Styles.halfGrid}>
          <Text style={Styles.optionsText} onPress={handleConfirm}>
            {rightText}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default Confirm;
