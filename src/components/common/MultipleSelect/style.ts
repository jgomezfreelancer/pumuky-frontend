import {StyleSheet} from 'react-native';
import { GREY } from '../../../styles/colors';

export default StyleSheet.create({
    listText: {
        color: GREY,
        marginLeft: 10
    },
    image: {
        width: 20,
        height: 20,
        borderRadius: 50,
        marginRight: 10,
        marginTop: 1,
      },
    icon: { marginRight: 10, marginTop: 2 },
    checkbox: { borderRadius: 50, paddingLeft: -20, paddingRight: 3 }
});
