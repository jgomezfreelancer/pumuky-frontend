import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import lodash from 'lodash';

const translationGetters: any = {
  en: () => require('./locales/en.json'),
  es: () => require('./locales/es.json'),
};

export const customTraslate: any = lodash.memoize(
  (key: string, config: any) => i18n.t(key, config),
  (key: string, config: any) => (config ? key + JSON.stringify(config) : key),
);

export const setI18nConfig = () => {
  const fallback = {languageTag: 'en', keySeparator: false};
  const {languageTag} =
    RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
    fallback;
  customTraslate.cache.clear();
  i18n.translations = {[languageTag]: translationGetters[languageTag]()};
  i18n.locale = languageTag;
};
