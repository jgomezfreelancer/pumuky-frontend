import React, {useCallback, useEffect, useRef} from 'react';

import Toast from 'react-native-toast-message';
import {
  NavigationContainer,
  NavigationContainerRef
} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import firebase from '@react-native-firebase/app';
import 'react-native-gesture-handler';
import moment from 'moment';
import 'moment-timezone';

import Dashboard from '../screens/Dashboard';
import MainLogin from '../screens/MainLogin';
import MainLoader from '../screens/MainLoader';
import {useDispatch, useSelector} from 'react-redux';
import {AppRootReducer} from '../reducers';
import {checkLogin, updateFcm} from '../reducers/login.reducer';
import {setI18nConfig} from '../utils/language';
import Login from '../screens/Login';
import sitemap from '../route/sitemap';
import ForgotPassword from '../screens/ForgotPassword';
import Register from '../screens/Register';
import {ENV} from '../../env';
import {fcmService} from '../components/Notification/FCMService';
import {
  INotification,
  IOpenNotification
} from '../reducers/notification.reducer';
import {localNotificationService} from '../components/Notification/LocalNotificationService';
import {FirebaseMessagingTypes} from '@react-native-firebase/messaging';

const {Screen, Navigator} = createStackNavigator();

if (!firebase.apps.length) {
  firebase.initializeApp(ENV.firebase);
}

const Router: React.FC = (): JSX.Element => {
  /* Refs */
  const navigationRef = useRef<NavigationContainerRef | null>(null);

  /* Redux */
  const {
    loginReducer: {checkUser, checkUserLoading, user}
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* Functions */
  const getInitialRoute = (): '' => {
    return checkUser ? sitemap.dashboard : sitemap.mainLogin;
  };

  const onRegister = useCallback(
    (token: string) => {
      if (user.id! > 0) {
        const timezone = moment.tz.guess();
        dispatch(updateFcm(user.id!, {fcm: token, timezone}));
      }
    },
    [dispatch, user]
  );
  /*  Receive notification in active device */
  const onNotification = useCallback((noti: IOpenNotification) => {
    const options = {
      soundName: 'default',
      playSound: true
    };
    const notification = JSON.parse(String(noti.data.notification));
    localNotificationService.showNotification(
      notification.id,
      notification.title,
      notification.body,
      notification,
      options
    );
  }, []);
  /*  Open notification when recieve a notification in background */
  const onOpenFirebaseNotification = useCallback(
    (noti: IOpenNotification & INotification) => {
      console.log('onOpenFirebaseNotification ', noti);
      if (noti.smallIcon) {
        navigationRef.current!.navigate(sitemap.calendar);
      }
    },
    []
  );

  /*  Open notification when recieve a notification with active device */
  const onOpenNotification = useCallback(
    (noti: IOpenNotification & INotification) => {
      console.log('onOpenNotification ', noti);
      if (noti.smallIcon) {
        navigationRef.current!.navigate(sitemap.calendar);
      }
    },
    []
  );

  useEffect(() => {
    setI18nConfig();
    dispatch(checkLogin());
  }, [dispatch]);

  const backgroundMessageHandler = (
    _: FirebaseMessagingTypes.RemoteMessage
  ) => {};

  useEffect(() => {
    if (checkUser && user.id! > 0) {
      fcmService
        .registerAppWithFCM()
        .then((res) => res)
        .catch((err) => new Error(err));
      fcmService.register(
        onRegister,
        onNotification,
        onOpenFirebaseNotification,
        backgroundMessageHandler
      );
      localNotificationService
        .configure(onOpenNotification)
        .then((res) => res)
        .catch((err) => new Error(err));
    }
  }, [
    checkUser,
    user.id,
    onNotification,
    onOpenFirebaseNotification,
    onOpenNotification,
    onRegister
  ]);

  return checkUserLoading ? (
    <MainLoader />
  ) : (
    <NavigationContainer ref={navigationRef}>
      <Navigator
        screenOptions={{headerShown: false}}
        initialRouteName={getInitialRoute()}>
        <Screen name={sitemap.mainLogin} component={MainLogin} />
        <Screen name={sitemap.login} component={Login} />
        <Screen name={sitemap.forgotPassword} component={ForgotPassword} />
        <Screen name={sitemap.register} component={Register} />
        <Screen name={sitemap.dashboard} component={Dashboard} />
      </Navigator>

      <Toast ref={(ref) => Toast.setRef(ref)} />
    </NavigationContainer>
  );
};
export default Router;
