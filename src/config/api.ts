const prefix = 'api/v1';

const API = {
  animal: `${prefix}/animal`,
  animalAge: `${prefix}/animal-age`,
  animalWeight: `${prefix}/animal-weight`,
  vaccine: `${prefix}/vaccine`,
  care: `${prefix}/care`,
  calendarByUser: `${prefix}/user-calendar/get-by-user`,
  forgotPassword: `${prefix}/email/forgot-password`,
  updateUser: `${prefix}/auth/update-user`,
  reminder: `${prefix}/reminder`,
  uploadProfileImage: `${prefix}/auth/upload-image`,
  updateSuscription: `${prefix}/auth/update-suscription`,
  refreshUser: `${prefix}/auth/user-refresh`
};

export default API;
