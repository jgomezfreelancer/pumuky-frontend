import AXIOS from '../config/Axios';
import headers from '../helpers/headers';
import {AxiosResponse} from 'axios';

const multiPart = async () => ({
  ...(await headers()),
  'Content-Type': 'multipart/form-data',
});

const Request = {
  get: async (url: string, params: object = {}): Promise<AxiosResponse> => {
    return AXIOS.get(url, {params, headers: await headers()});
  },
  post: async (url: string, body: object = {}): Promise<AxiosResponse> => {
    return AXIOS.post(url, body, {headers: await headers()});
  },
  put: async (
    url: string,
    body: object = {},
    isMultipart: boolean = false,
  ): Promise<AxiosResponse> => {
    return AXIOS.put(url, body, {
      headers: isMultipart ? await multiPart() : await headers(),
    });
  },
  delete: async (url: string): Promise<AxiosResponse> => {
    return AXIOS.delete(url, {headers: await headers()});
  },
};

export default Request;
