import API from '../config/api';
import Request from '../config/request';
import { IDispatch } from '../interfaces/IDispatch';
import { IReminder } from './reminder.reducer';

const GET_ALL = 'calendar/get_all';
const GET_CALENDAR_LOADING = 'calendar/get_calendar_loading';
const SET_CALENDAR_DATE = 'calendar/set_calendar_date';

export interface ICalendar {
  id: number;
  date: string;
  reminders: IReminder[];
}

interface GetAll {
  type: typeof GET_ALL;
  payload: ICalendar[];
}
interface GetCalendarLoading {
  type: typeof GET_CALENDAR_LOADING;
  payload: boolean;
}

interface SetCalendarDate {
  type: typeof SET_CALENDAR_DATE;
  payload: string;
}

type ActionTypes = SetCalendarDate | GetAll | GetCalendarLoading;

export type IState = {
  selectedCalendarDate: string;
  calendarList: ICalendar[];
  calendarListLoading: boolean;
};

const initialState: IState = {
  selectedCalendarDate: '',
  calendarList: [],
  calendarListLoading: false
};

export const getAllCalendar = (user: number) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: GET_CALENDAR_LOADING,
    payload: true,
  });
  try {
    const { data } = await Request.get(`${API.calendarByUser}/${user}`);
    dispatch({
      type: GET_CALENDAR_LOADING,
      payload: false,
    });
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    return data;
  } catch (error) {
    dispatch({
      type: GET_CALENDAR_LOADING,
      payload: false,
    });
    return error;
  }
};

export const setCalendarDate = (date: string) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: SET_CALENDAR_DATE,
    payload: date,
  });
};

const calendarReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET_ALL:
      return {
        ...state,
        calendarList: action.payload,
      };
    case GET_CALENDAR_LOADING:
      return {
        ...state,
        calendarListLoading: action.payload,
      };
    case SET_CALENDAR_DATE:
      return {
        ...state,
        selectedCalendarDate: action.payload,
      };
    default:
      return state;
  }
};

export default calendarReducer;
