import API from '../config/api';
import Request from '../config/request';
import { IDispatch } from '../interfaces/IDispatch';
import { IAnimalResponse, ImageObject } from './animal.reducer';

export interface Days {
  value: number;
  description: string;
}

export const days: Days[] = [
  { value: 1, description: 'Lu' },
  { value: 2, description: 'Ma' },
  { value: 3, description: 'Mi' },
  { value: 4, description: 'Ju' },
  { value: 5, description: 'Vi' },
  { value: 6, description: 'Sá' },
  { value: 0, description: 'Do' },
];

const GET_ALL = 'care/get_all';
const LOADING = 'care/loading';
const SET_LIST = 'care/set_list';
const SET = 'care/set';

export interface ICare {
  id?: string;
  description?: string;
  note?: number[];
  animal?: IAnimalResponse;
  checked_date?: string;
}

export interface ICareForm {
  id?: string;
  description?: string;
  fileImage?: ImageObject | undefined;
  image?: string;
  date?: string;
  note?: string;
  animal?: IAnimalResponse;
}

interface Set {
  type: typeof SET;
  payload: ICare;
}

interface GetAll {
  type: typeof GET_ALL;
  payload: ICare[];
}

interface SetList {
  type: typeof SET_LIST;
  payload: ICare;
}

interface Loading {
  type: typeof LOADING;
  payload: boolean;
}

type ActionTypes = GetAll | Loading | SetList | Set;

export type IState = {
  list: ICare[];
  care: ICare;
  loading: boolean;
};

const initialState: IState = {
  list: [],
  care: {
    id: '',
    description: '',
    note: [],
  },
  loading: false,
};

export const getAll = () => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const { data } = await Request.get(API.animalAge);
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const storeCare = (care: ICare) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const { data } = await Request.post(API.care, care);
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const updateCare = (care: ICare) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const { data } = await Request.put(`${API.care}/${care.id}`, care);
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const updateCareChecked = (body: {
  id: number;
  animalId: number;
  date: string | null;
}) => async (dispatch: IDispatch<ActionTypes>) => {
  try {
    const { data } = await Request.post(`${API.care}/checked`, body);
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const deleteCare = (body: { id: number; animalId: number }) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const { data } = await Request.post(`${API.care}/delete`, body);
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const setList = (careList: ICare) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: SET_LIST,
    payload: careList,
  });
};

export const setCareArray = (list: ICare[]) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: GET_ALL,
    payload: list,
  });
};

export const setCare = (care: ICare) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: SET,
    payload: care,
  });
};

const careReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET_ALL:
      return {
        ...state,
        list: action.payload,
      };
    case SET:
      return {
        ...state,
        care: { ...state.care, ...action.payload },
      };
    case SET_LIST:
      return {
        ...state,
        list: [...state.list, action.payload],
      };
    case LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
};

export default careReducer;
