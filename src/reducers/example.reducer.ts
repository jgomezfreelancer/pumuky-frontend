import {IDispatch} from '../interfaces/IDispatch';

const GET_EXAMPLE = 'example/get_example';

export interface ICountry {
  id: number;
  description: string;
}

interface GetExample {
  type: typeof GET_EXAMPLE;
  payload: string;
}

type ActionTypes = GetExample;

export type IState = {
  value: string;
};

const initialState: IState = {
  value: 'state exammple',
};

export const getExample = (newValue: string) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: GET_EXAMPLE,
    payload: newValue,
  });
};

const countryReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET_EXAMPLE:
      return {
        ...state,
        value: action.payload,
      };
    default:
      return state;
  }
};

export default countryReducer;
