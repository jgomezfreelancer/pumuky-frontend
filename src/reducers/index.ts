import {combineReducers} from 'redux';

import exampleReducer, {IState as ExampleState} from './example.reducer';
import loginReducer, {IState as LoginState} from './login.reducer';
import animalReducer, {IState as AnimalState} from './animal.reducer';
import animalAgeReducer, {IState as AnimalAgeState} from './animal-age.reducer';
import animalWeightReducer, {
  IState as AnimalWeightState,
} from './animal-weight.reducer';
import vaccineReducer, {IState as VaccineState} from './vaccine.reducer';
import careReducer, {IState as CareState} from './care.reducer';
import calendarReducer, {IState as CalendarState} from './calendar.reducer';
import reminderReducer, {IState as ReminderState} from './reminder.reducer';

const rootReducer = combineReducers({
  loginReducer,
  exampleReducer,
  animalReducer,
  animalAgeReducer,
  animalWeightReducer,
  calendarReducer,
  vaccineReducer,
  careReducer,
  reminderReducer,
});

export type AppRootReducer = {
  loginReducer: LoginState;
  exampleReducer: ExampleState;
  animalAgeReducer: AnimalAgeState;
  animalReducer: AnimalState;
  animalWeightReducer: AnimalWeightState;
  vaccineReducer: VaccineState;
  careReducer: CareState;
  calendarReducer: CalendarState;
  reminderReducer: ReminderState;
};

export default rootReducer;
