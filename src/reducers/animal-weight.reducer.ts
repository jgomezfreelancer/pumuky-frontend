import API from '../config/api';
import Request from '../config/request';
import {IDispatch} from '../interfaces/IDispatch';

const GET_ALL = 'animal-age/get_all';
const LOADING = 'animal-age/loading';

export interface IAnimalWeight {
  id?: number;
  description?: string;
  type?: string;
  created?: string;
  animalId?: number;
}

interface GetAll {
  type: typeof GET_ALL;
  payload: IAnimalWeight[];
}

interface Loading {
  type: typeof LOADING;
  payload: boolean;
}

type ActionTypes = GetAll | Loading;

export type IState = {
  list: IAnimalWeight[];
  loading: boolean;
};

const initialState: IState = {
  list: [],
  loading: false,
};

export const getAll = () => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const {data} = await Request.get(API.animalWeight);
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const storeWeight = (weight: IAnimalWeight) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const {data} = await Request.post(API.animalWeight, weight);
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const setAnimalWeight = (list: IAnimalWeight[]) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: GET_ALL,
    payload: list,
  });
};

const animalWeightReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET_ALL:
      return {
        ...state,
        list: action.payload,
      };

    case LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
};

export default animalWeightReducer;
