import API from '../config/api';
import Request from '../config/request';
import {IDispatch} from '../interfaces/IDispatch';
import {IAnimalResponse, ImageObject} from './animal.reducer';

const GET_ALL = 'vaccine/get_all';
const LOADING = 'vaccine/loading';
const SET_LIST = 'vaccine/set_list';
const SET = 'vaccine/set';

export interface IVaccine {
  id?: string;
  description?: string;
  fileImage?: ImageObject | undefined | null;
  image?: string;
  date?: string;
  note?: string;
  animal?: IAnimalResponse;
  animalId?: number;
}

export interface IVaccineForm {
  id?: string;
  description?: string;
  fileImage?: ImageObject | undefined;
  image?: string;
  date?: string;
  note?: string;
  animal?: IAnimalResponse;
}

interface Set {
  type: typeof SET;
  payload: IVaccine;
}

interface GetAll {
  type: typeof GET_ALL;
  payload: IVaccine[];
}

interface SetList {
  type: typeof SET_LIST;
  payload: IVaccine;
}

interface Loading {
  type: typeof LOADING;
  payload: boolean;
}

type ActionTypes = GetAll | Loading | SetList | Set;

export type IState = {
  list: IVaccine[];
  vaccine: IVaccine;
  loading: boolean;
};

const initialState: IState = {
  list: [],
  vaccine: {
    id: '',
    description: '',
    fileImage: undefined,
    image: '',
    date: '',
    note: '',
    animal: {},
    animalId: 0,
  },
  loading: false,
};

export const getAll = () => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const {data} = await Request.get(API.animalAge);
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const storeVaccine = (vacccine: IVaccine) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const {data} = await Request.post(API.vaccine, vacccine);
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const getVaccineByAnimal = (id: number) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  try {
    const {data} = await Request.get(`${API.vaccine}/get-by-animal/${id}`);
    return data;
  } catch (error) {
    return error;
  }
};

export const updateVaccine = (vacccine: IVaccine) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const {data} = await Request.put(`${API.vaccine}/${vacccine.id}`, vacccine);
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const deleteVaccine = (body: {id: number; animalId: number}) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const {data} = await Request.post(`${API.vaccine}/delete`, body);
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const setList = (vaccineList: IVaccine) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: SET_LIST,
    payload: vaccineList,
  });
};

export const setVaccineArray = (list: IVaccine[]) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: GET_ALL,
    payload: list,
  });
};

export const setVaccine = (vaccine: IVaccine) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: SET,
    payload: vaccine,
  });
};

const vaccineReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET_ALL:
      return {
        ...state,
        list: action.payload,
      };
    case SET:
      return {
        ...state,
        vaccine: {...state.vaccine, ...action.payload},
      };
    case SET_LIST:
      return {
        ...state,
        list: [...state.list, action.payload],
      };
    case LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
};

export default vaccineReducer;
