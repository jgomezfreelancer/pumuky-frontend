import AsyncStorage from '@react-native-community/async-storage';

import API from '../api/Login';
import RequestAPI from '../config/api';
import Message from '../helpers/message';
import {IDispatch} from '../interfaces/IDispatch';
import {User} from '@react-native-community/google-signin';
import Toast from 'react-native-toast-message';
import Request from '../config/request';
import Endpoint from '../config/api';

// const purchaseHistory = {
//   developerPayload: 'null',
//   signatureAndroid:
//     'g3mD8HY0cb6fqJddvd/kiYjLdOmqepsh5q6ZmOpKwvnqfdlAyHoiJoPcm+Sw0Ki7Fs1dPh3qrg8vmMngSqiBWM7zQz1mFJMqXCa5Rl02t++fHu/R5/Huu07VuXER+H/Y1eHFUJ4doE4gM+QD4CZx/biE2AVIf8EnoK619VDRVIVFeSIoB1Msn04ufTBJbusMIuQ031kPvU+1AbLAc0rNiV7AMMTkRcIlVPbzyKv3Ja2+pmndG7Yf97y9UK3LGe229rcyOIT2Zhx57HZrgevAzoUXMm/rSMIFtlwSD58smf8+VSTHCoFJQ6cWEN/d4f5jzsHX64Jqt4swMkyJQDpMyg==',
//   purchaseToken:
//     'dfbknlkokmgkfbpkcpjelcmc.AO-J1Ow7d4yPgoNgbwKwDxEJkE-eIZ8W7XCzjGDTy3eJGEhR-Zvcr-u0HgQxGHDcDSFQ8U6iu4WLDIOeUEz55bWe6_4dYj0i1Q',
//   transactionReceipt: {
//     productId: 'product.pumuky.unique.payment',
//     purchaseToken:
//       'dfbknlkokmgkfbpkcpjelcmc.AO-J1Ow7d4yPgoNgbwKwDxEJkE-eIZ8W7XCzjGDTy3eJGEhR-Zvcr-u0HgQxGHDcDSFQ8U6iu4WLDIOeUEz55bWe6_4dYj0i1Q',
//     purchaseTime: 1619285942494,
//     developerPayload: null
//   },
//   dataAndroid: {
//     productId: 'product.pumuky.unique.payment',
//     purchaseToken:
//       'dfbknlkokmgkfbpkcpjelcmc.AO-J1Ow7d4yPgoNgbwKwDxEJkE-eIZ8W7XCzjGDTy3eJGEhR-Zvcr-u0HgQxGHDcDSFQ8U6iu4WLDIOeUEz55bWe6_4dYj0i1Q',
//     purchaseTime: 1619285942494,
//     developerPayload: null
//   },
//   transactionDate: 1619285942494,
//   productId: 'product.pumuky.unique.payment'
// };

/* Purchase information */
// const dataPurchase = {
//   packageNameAndroid: 'com.pumuky',
//   transactionReceipt: {
//     orderId: 'GPA.3378-6397-6561-91083',
//     packageName: 'com.pumuky',
//     productId: 'monthly.suscripcion.pumuky',
//     purchaseTime: 1619227892603,
//     purchaseState: 0,
//     purchaseToken:
//       'papcaigmeepinnfkfhgedbeg.AO-J1OyjVf01DMtzLyz0pd3cM15BiY2JZH3MFkuVHrJHVa_OajtqTVs4-OxbQib0VwxFJcYF3450MXdNopwZRF191AMuIJLu5g',
//     autoRenewing: true,
//     acknowledged: false
//   },
//   productId: 'monthly.suscripcion.pumuky',
//   obfuscatedAccountIdAndroid: '',
//   purchaseToken:
//     'papcaigmeepinnfkfhgedbeg.AO-J1OyjVf01DMtzLyz0pd3cM15BiY2JZH3MFkuVHrJHVa_OajtqTVs4-OxbQib0VwxFJcYF3450MXdNopwZRF191AMuIJLu5g',
//   transactionId: 'GPA.3378-6397-6561-91083',
//   transactionDate: 1619227892603,
//   dataAndroid: {
//     orderId: 'GPA.3378-6397-6561-91083',
//     packageName: 'com.pumuky',
//     productId: 'monthly.suscripcion.pumuky',
//     purchaseTime: 1619227892603,
//     purchaseState: 0,
//     purchaseToken:
//       'papcaigmeepinnfkfhgedbeg.AO-J1OyjVf01DMtzLyz0pd3cM15BiY2JZH3MFkuVHrJHVa_OajtqTVs4-OxbQib0VwxFJcYF3450MXdNopwZRF191AMuIJLu5g',
//     autoRenewing: true,
//     acknowledged: false
//   },
//   autoRenewingAndroid: true,
//   obfuscatedProfileIdAndroid: '',
//   purchaseStateAndroid: 1,
//   signatureAndroid:
//     'X5lawZas54j4bdCSunZ6QZzlsGLTYJmFvCgxi91CDPjNsnAP3U1mRyRTuGLL8Qe4+zKELlTcuJtDDeP2w4untBtKUmkCVT5U+uBSXyZd+womom31uuMNT4sKajg0QL9T+XHT/FNr3wT2CRti5Ssfps9TPIOjWvHkWKAXFOVao5OyjDDJUfs/JFd/nOsStNV+rrp2ereZSY/TaLTzWxZBLnz4NiELZVdolWBUW185giM9SPbcZQUxkzc33B5i+t21YDbl1oquwJMCczEvY9mgvYNsYEj4W2fyCxAsu30OArCCzcZcSvucoCega0GAa3knd2rZBWRmvl3PNTi21FfAVA==',
//   isAcknowledgedAndroid: false,
//   developerPayloadAndroid: ''
// };

/* Actions */

const SET_USER = 'login/set_user';
const GET_USER_IMAGE = 'login/get_user_image';
const GET_USER_PROFILE_IMAGE = 'login/get_user_profile_image';
const LOGOUT = 'login/logout';
const LOADING = 'login/loading';
const UPLOAD_PROFILE_IMAGE = 'login/upload_profile_image';
const CHECK_USER = 'login/check_user';
const CHECK_USER_LOADING = 'login/check_user_loading';
const SET_USERS_BY_LOCATION = 'login/set_users_by_location';
const UPDATE_STARTER_SETTINGS = 'login/update_starter_settings';
const GET_SERVER_DATE = 'login/get_server_date';
const GET_USER_PETITION = 'login/get_user_petition';
const ACCEPT_LOADING = 'login/accept_loading';

export interface IPurchaseHistory {
  developerPayload: string;
  signatureAndroid: string;
  purchaseToken: string;
  transactionReceipt: ITransactionReceipt;
  dataAndroid: ITransactionReceipt;
  transactionDate: number;
  productId: string;
}

export interface ITransactionReceipt {
  orderId: string;
  packageName: string;
  productId: string;
  purchaseTime: number;
  purchaseState: number;
  purchaseToken: string;
  autoRenewing: boolean;
  acknowledged: boolean;
}
export interface IConfirmPurchase {
  packageNameAndroid: string;
  transactionReceipt: ITransactionReceipt;
  productId: string;
  obfuscatedAccountIdAndroid: string;
  purchaseToken: string;
  transactionId: string;
  transactionDate: number;
  dataAndroid: ITransactionReceipt;
  autoRenewingAndroid: boolean;
  obfuscatedProfileIdAndroid: string;
  purchaseStateAndroid: number;
  signatureAndroid: string;
  isAcknowledgedAndroid: boolean;
  developerPayloadAndroid: string;
}

export type IUserPetition = {
  id?: number;
  user_id?: number;
  description?: string;
  status?: number;
  created?: string;
};

export type IUser = {
  id?: number;
  username?: string;
  first_name?: string;
  last_name?: string;
  social_auth?: number;
  location?: number;
  profile?: number;
  help?: number;
  language_id?: number | null;
  nationality_id?: number | null;
  latitude?: number;
  longitude?: number;
  meters?: number;
  age?: number;
  address?: string;
  cp?: string;
  country_id?: number;
  password?: string;
  country_location?: string;
  ranking?: number | string;
  profileImage?: string;
  picture?: string;
  first_time?: boolean;
  languages?: number[];
  nationalities?: number[];
  limit_distance?: number;
  petition?: IUserPetition;
  notificationId?: number;
  userInterestKeys?: number[];
  birthday?: string;
  isMember?: string;
  petsLimit?: number;
};

export interface IStarterSettings {
  languages?: number[];
  nationalities?: number[];
  profile?: number;
  location?: number;
  limit_distance?: number;
  nationality_id?: number;
  interests?: number[];
}

interface SetUser {
  type: typeof SET_USER;
  payload: IUser;
}

interface SetUploadProfileImage {
  type: typeof UPLOAD_PROFILE_IMAGE;
  payload: boolean;
}
interface GetServerDate {
  type: typeof GET_SERVER_DATE;
  payload: string;
}

interface GetUserPetition {
  type: typeof GET_USER_PETITION;
  payload: IUserPetition;
}

interface Logout {
  type: typeof LOGOUT;
}

interface Loading {
  type: typeof LOADING;
  payload: boolean;
}

interface AcceptLoading {
  type: typeof ACCEPT_LOADING;
  payload: boolean;
}

interface Check {
  type: typeof CHECK_USER;
  payload: boolean;
}

interface CheckLoading {
  type: typeof CHECK_USER_LOADING;
  payload: boolean;
}

interface SetUsersByLocation {
  type: typeof SET_USERS_BY_LOCATION;
  payload: User[];
}

interface GetUserImage {
  type: typeof GET_USER_IMAGE;
  payload: string;
}

interface GetUserProfileImage {
  type: typeof GET_USER_PROFILE_IMAGE;
  payload: string | null;
}

export interface UpdateStarterSettings {
  type: typeof UPDATE_STARTER_SETTINGS;
  payload: IStarterSettings;
}

type ActionTypes =
  | SetUser
  | Logout
  | Loading
  | Check
  | CheckLoading
  | SetUsersByLocation
  | GetUserImage
  | UpdateStarterSettings
  | GetServerDate
  | GetUserPetition
  | GetUserProfileImage
  | AcceptLoading
  | SetUploadProfileImage;

export type IState = {
  user: IUser;
  status: boolean;
  loading: boolean;
  checkUser: boolean;
  checkUserLoading: boolean;
  usersByLocation: User[];
  userImage: string;
  starterSettings: IStarterSettings;
  serverDate: string;
  userPetition: IUserPetition;
  userProfileImage: string | null;
  acceptLoading: boolean;
  uploadProfileImageLoading: boolean;
};

const initialState: IState = {
  user: {},
  status: false,
  loading: false,
  checkUser: false,
  checkUserLoading: false,
  usersByLocation: [],
  userImage: '',
  starterSettings: {
    languages: [],
    nationalities: [],
    location: 0,
    profile: 0,
    limit_distance: 0,
    interests: []
  },
  serverDate: '',
  userPetition: {},
  userProfileImage: null,
  acceptLoading: false,
  uploadProfileImageLoading: false
};

export interface ISocialLogin {
  email: string;
  first_name: string | null;
  last_name: string | null;
  token?: string;
}

export interface IFcm {
  fcm: string;
  timezone: string;
}

export interface ICoord {
  latitude: number;
  longitude: number;
}

export interface ISendHelp {
  id?: number;
  message: string;
  km: number;
  interests: number[];
}

export interface IAcceptHelp {
  requestId: number;
  acceptId: number;
  message: string;
  isPetition: boolean;
}

export interface IRegister {
  first_name: string;
  username: string;
  password: string;
}

export interface IUserInterest {
  userId: number;
  interests: number[];
}

export const login = (body: object) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({type: LOADING, payload: true});
  try {
    const {data, status} = await API.login(body);
    let authResponse: any = [];
    if (status === 200 || status === 201) {
      authResponse = {
        data,
        status
      };
      const {accessToken, user} = data;
      await AsyncStorage.setItem('token', accessToken);
      dispatch({type: SET_USER, payload: user});
      dispatch({type: LOADING, payload: false});
    }
    return authResponse;
  } catch (error) {
    const message = Message.exception(error);
    Toast.show({
      text1: message,
      type: 'error'
    });
    dispatch({type: LOADING, payload: false});
    throw message;
  }
};

export const signup = (body: IRegister) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({type: LOADING, payload: true});
  try {
    const {data, status} = await API.signup(body);
    let authResponse: any = [];
    if (status === 200 || status === 201) {
      authResponse = {
        data,
        status
      };
      const {accessToken, user} = data;
      await AsyncStorage.setItem('token', accessToken);
      dispatch({type: SET_USER, payload: user});
      dispatch({type: LOADING, payload: false});
    }
    return authResponse;
  } catch (error) {
    dispatch({type: LOADING, payload: false});
    throw error;
  }
};

export const sociaLogin = (body: ISocialLogin) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({type: LOADING, payload: true});
  try {
    const {data, status} = await API.socialLogin(body);
    if (status === 200 || status === 201) {
      const {accessToken, user} = data;
      await AsyncStorage.setItem('token', accessToken);
      dispatch({type: SET_USER, payload: user});
      dispatch({type: CHECK_USER, payload: true});
      dispatch({type: LOADING, payload: false});
      return user;
    }
    return {};
  } catch (error) {
    const message = Message.exception(error);
    Toast.show({
      text1: message,
      type: 'error'
    });
    dispatch({type: LOADING, payload: false});
    dispatch({type: CHECK_USER, payload: false});
    throw message;
  }
};

export const logout = (token: string) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  try {
    const {status, data} = await API.logout(token!);
    if (status === 200 || status === 201) {
      await AsyncStorage.clear();
      dispatch({type: LOGOUT});
    }
    return data;
  } catch (error) {
    return error;
  }
};

export const checkLogin = () => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({type: CHECK_USER_LOADING, payload: true});
  try {
    const {data, status} = await API.checkLogin();
    if (status === 200 || status === 201) {
      dispatch({type: CHECK_USER, payload: true});
      dispatch({type: SET_USER, payload: data});
    } else {
      dispatch({type: CHECK_USER, payload: false});
    }
    dispatch({type: CHECK_USER_LOADING, payload: false});
    return [];
  } catch (error) {
    const token = await AsyncStorage.getItem('token');
    if (token) logout(token)(dispatch);
    dispatch({type: CHECK_USER, payload: false});
    dispatch({type: CHECK_USER_LOADING, payload: false});
    return error;
  }
};

export const refreshUser = (id: number) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  try {
    const {data, status} = await Request.get(`${Endpoint.refreshUser}/${id}`);
    if (status === 200 || status === 201) {
      dispatch({type: SET_USER, payload: data});
    }
    return [];
  } catch (error) {
    return error;
  }
};

export const updateUser = (id: number, body: IUser) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({type: LOADING, payload: true});
  try {
    const {data, status} = await Request.put(
      `${Endpoint.updateUser}/${id}`,
      body
    );
    if (status === 200 || status === 201) {
      dispatch({type: SET_USER, payload: data});
      dispatch({type: LOADING, payload: false});
    }
    dispatch({type: LOADING, payload: false});
    return data;
  } catch (error) {
    dispatch({type: LOADING, payload: false});
    return error;
  }
};

export const uploadProfileImage = (body: any) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({type: UPLOAD_PROFILE_IMAGE, payload: true});
  try {
    const {data, status} = await Request.post(
      `${Endpoint.uploadProfileImage}`,
      body
    );
    if (status === 200 || status === 201) {
      dispatch({type: SET_USER, payload: data});
      dispatch({type: UPLOAD_PROFILE_IMAGE, payload: false});
    }
    dispatch({type: UPLOAD_PROFILE_IMAGE, payload: false});
    return data;
  } catch (error) {
    dispatch({type: UPLOAD_PROFILE_IMAGE, payload: false});
    return error;
  }
};

export const forgotPassword = (username: string) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({type: LOADING, payload: true});
  try {
    const {status, data} = await API.forgotPassword(username);
    if (status === 200 || (status === 201 && data)) {
      dispatch({type: LOADING, payload: false});
      Toast.show({
        text1: data.message,
        type: data.status ? 'success' : 'error'
      });
    }
    return data;
  } catch (error) {
    const message = Message.exception(error);
    Toast.show({
      text1: message,
      type: 'error'
    });
    dispatch({type: LOADING, payload: false});
  }
};

export const updateFcm = (id: number, body: IFcm) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({type: LOADING, payload: true});
  try {
    const {status} = await API.updateFcm(id, body);
    if (status === 200 || status === 201) {
      dispatch({type: LOADING, payload: false});
    }
    return [];
  } catch (error) {
    return error;
  }
};

export const updateSuscription = (body: {
  id: number;
  isMember: boolean;
}) => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({type: LOADING, payload: true});
  try {
    const {status} = await Request.post(RequestAPI.updateSuscription, body);
    if (status === 200 || status === 201) {
      dispatch({type: LOADING, payload: false});
    }
    return [];
  } catch (error) {
    return error;
  }
};

const loginReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.payload,
        status: true
      };
    case GET_USER_IMAGE:
      return {
        ...state,
        userImage: action.payload
      };
    case GET_USER_PROFILE_IMAGE:
      return {
        ...state,
        userProfileImage: action.payload
      };
    case UPDATE_STARTER_SETTINGS:
      return {
        ...state,
        starterSettings: action.payload
      };
    case LOGOUT:
      return {
        ...state,
        ...initialState
      };
    case LOADING:
      return {
        ...state,
        loading: action.payload
      };
    case UPLOAD_PROFILE_IMAGE:
      return {
        ...state,
        uploadProfileImageLoading: action.payload
      };
    case ACCEPT_LOADING:
      return {
        ...state,
        acceptLoading: action.payload
      };
    case CHECK_USER:
      return {
        ...state,
        checkUser: action.payload
      };
    case CHECK_USER_LOADING:
      return {
        ...state,
        checkUserLoading: action.payload
      };
    case SET_USERS_BY_LOCATION:
      return {
        ...state,
        usersByLocation: action.payload
      };
    case GET_SERVER_DATE:
      return {
        ...state,
        serverDate: action.payload
      };
    case GET_USER_PETITION:
      return {
        ...state,
        userPetition: action.payload
      };
    default:
      return state;
  }
};

export default loginReducer;
