import API from '../config/api';
import Request from '../config/request';
import {IDispatch} from '../interfaces/IDispatch';
import {IAnimalWeight} from './animal-weight.reducer';
import {ICare} from './care.reducer';
import {IUser} from './login.reducer';
import {IVaccine} from './vaccine.reducer';

const GET = 'animal/get';
const SET = 'animal/set';
const CLEAR_ANIMAL = 'animal/clear_animal';
const SET_CREATE_ANIMAL = 'animal/set_create_animal';
const GET_ANIMALS_BY_USER = 'animal/get_animals_by_user';
const LOADING = 'animal/loading';
const CREATE_ANIMAL_LOADING = 'animal/create_animal_loading';
const GET_ANIMAL_LOADING = 'animal/get_animal_lpading';
const UPLOAD_IMAGE_LOADING = 'animal/upload_image_loading';
const SET_CREATE = 'animal/set_create';
const SET_BOTTOM_TAB = 'animal/set_bottom_tab';

export interface ImageObject {
  data?: string;
  type?: string;
  name?: any;
}

export interface IAnimalReport {
  id: number;
  description: string;
  image: string;
  note: string;
  animal: IAnimalResponse;
}

export interface IAnimalForm {
  id?: number;
  description?: string;
  name?: string;
  age?: string;
  race?: string;
  weight?: IAnimalWeight;
  shop_date?: string;
  food?: string[];
  suplements?: string[];
  maintenance?: string[];
  amount_schedule?: string[];
  notes?: string;
  reception_date?: string;
  userId?: number;
  fileImage?: ImageObject | undefined | string | null;
  technicalImage1?: string | undefined;
  technicalImage2?: string | undefined;
  technicalImage3?: string | undefined;
  techImage1?: ImageObject | undefined | string | null;
  techImage2?: ImageObject | undefined | string | null;
  techImage3?: ImageObject | undefined | string | null;
  vaccines?: IVaccine[];
  cares?: ICare[];
}

export interface ICreateAnimal {
  id?: number;
  description?: string;
  age?: string;
  race?: string;
  weight?: IAnimalWeight;
  shop_date?: string;
  food?: string[];
  suplements?: string[];
  maintenance?: string[];
  amount_schedule?: string[];
  notes?: string;
  reception_date?: string;
  userId?: number;
  fileImage?: ImageObject | undefined | string | null;
  techImage1?: ImageObject | undefined | string | null;
  techImage2?: ImageObject | undefined | string | null;
  techImage3?: ImageObject | undefined | string | null;
}

export interface IAnimalResponse {
  id?: number;
  name?: string;
  description?: string;
  age?: string;
  race?: string;
  weight?: IAnimalWeight;
  shop_date?: string;
  food?: string[];
  suplements?: string[];
  maintenance?: string[];
  amount_schedule?: string[];
  notes?: string;
  image?: string | undefined;
  technicalImage1?: string | undefined;
  technicalImage2?: string | undefined;
  technicalImage3?: string | undefined;
  techImage1?: ImageObject | undefined | string | null;
  techImage2?: ImageObject | undefined | string | null;
  techImage3?: ImageObject | undefined | string | null;
  reception_date?: string;
  userId?: number;
  animalVaccine?: IVaccine[];
  animalCare?: ICare[];
  animalWeight?: IAnimalWeight[];
}

export interface IAnimalUser {
  id: number;
  userId: number;
  animalId: number;
  user: IUser;
  animal: IAnimalResponse;
}

interface Get {
  type: typeof GET;
  payload: IAnimalForm;
}

interface ClearAnimal {
  type: typeof CLEAR_ANIMAL;
}

interface SetCreateAnimal {
  type: typeof SET_CREATE_ANIMAL;
  payload: ICreateAnimal;
}

interface Set {
  type: typeof SET;
  payload: IAnimalForm;
}

interface SetTab {
  type: typeof SET_BOTTOM_TAB;
  payload: number;
}

interface SetCreate {
  type: typeof SET_CREATE;
  payload: boolean;
}

interface Set {
  type: typeof SET;
  payload: IAnimalForm;
}

interface GetAnimalsByUser {
  type: typeof GET_ANIMALS_BY_USER;
  payload: IAnimalResponse[];
}

interface UploadImageLoading {
  type: typeof UPLOAD_IMAGE_LOADING;
  payload: boolean;
}

interface Loading {
  type: typeof LOADING;
  payload: boolean;
}

interface CreateAnimalLoading {
  type: typeof CREATE_ANIMAL_LOADING;
  payload: boolean;
}

interface GetAnimalLoading {
  type: typeof GET_ANIMAL_LOADING;
  payload: boolean;
}

type ActionTypes =
  | GetAnimalsByUser
  | Loading
  | Get
  | Set
  | UploadImageLoading
  | GetAnimalLoading
  | SetCreate
  | SetTab
  | SetCreateAnimal
  | CreateAnimalLoading
  | ClearAnimal;

export type IState = {
  animal: IAnimalResponse;
  createAnimalState: ICreateAnimal;
  animalsByUser: IAnimalResponse[];
  loading: boolean;
  createAnimalLoading: boolean;
  uploadImageLoading: boolean;
  getAnimalLoading: boolean;
  isCreate: boolean;
  tab: number;
};

const initialState: IState = {
  animal: {
    id: 0,
    description: '',
    name: '',
    age: '',
    race: '',
    weight: {
      id: 0,
      description: '',
      type: ''
    },
    shop_date: '',
    food: [],
    suplements: [],
    maintenance: [],
    amount_schedule: [],
    notes: '',
    image: undefined,
    technicalImage1: undefined,
    technicalImage2: undefined,
    technicalImage3: undefined,
    techImage1: undefined,
    techImage2: undefined,
    techImage3: undefined,
    animalVaccine: [],
    animalCare: [],
    animalWeight: []
  },
  createAnimalState: {
    id: 0,
    description: '',
    age: '',
    race: '',
    weight: {
      id: 0,
      description: '',
      type: ''
    },
    shop_date: '',
    reception_date: '',
    food: [],
    suplements: [],
    maintenance: [],
    amount_schedule: [],
    notes: '',
    fileImage: undefined
  },
  animalsByUser: [],
  loading: false,
  uploadImageLoading: false,
  getAnimalLoading: false,
  isCreate: false,
  tab: 0,
  createAnimalLoading: false
};

export const setLoading = (loader: boolean) => (
  dispatch: IDispatch<ActionTypes>
) =>
  dispatch({
    type: LOADING,
    payload: loader
  });

export const getAnimalsByUser = (id: number) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({
    type: LOADING,
    payload: true
  });
  try {
    const {data} = await Request.get(`${API.animal}/user/${id}`);
    dispatch({
      type: GET_ANIMALS_BY_USER,
      payload: data
    });
    dispatch({
      type: LOADING,
      payload: false
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const getAnimal = (id: number) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({
    type: GET_ANIMAL_LOADING,
    payload: true
  });
  try {
    const {data} = await Request.get(`${API.animal}/${id}`);
    dispatch({
      type: GET,
      payload: data
    });
    dispatch({
      type: GET_ANIMAL_LOADING,
      payload: false
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const removeAnimal = (body: {id: number; user: number}) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  try {
    const {data} = await Request.post(`${API.animal}/delete`, body);
    dispatch({
      type: GET_ANIMALS_BY_USER,
      payload: data
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const clearAnimal = () => async (dispatch: IDispatch<ActionTypes>) => {
  dispatch({type: CLEAR_ANIMAL});
};
export const createAnimal = (body: IAnimalForm) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({
    type: LOADING,
    payload: true
  });
  try {
    const {data} = await Request.post(`${API.animal}`, body);
    dispatch({
      type: GET,
      payload: data
    });
    dispatch({
      type: LOADING,
      payload: false
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const updateAnimal = (body: IAnimalForm) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({
    type: LOADING,
    payload: true
  });
  try {
    const {data} = await Request.put(`${API.animal}/${body.id}`, body);
    dispatch({
      type: GET,
      payload: data
    });
    dispatch({
      type: LOADING,
      payload: false
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const setAnimalState = (body: IAnimalForm) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({
    type: GET,
    payload: body
  });
};

export const setCreateAnimalState = (body: ICreateAnimal) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({
    type: SET_CREATE_ANIMAL,
    payload: body
  });
};

export const setIsCreate = (isCreate: boolean) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({
    type: SET_CREATE,
    payload: isCreate
  });
};

export const setBottomTab = (tab: number) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({
    type: SET_BOTTOM_TAB,
    payload: tab
  });
};

export const uploadAnimalImage = (body: any) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  dispatch({
    type: UPLOAD_IMAGE_LOADING,
    payload: true
  });
  try {
    const {data} = await Request.post(`${API.animal}/upload-image/`, body);
    dispatch({
      type: GET,
      payload: data
    });
    dispatch({
      type: UPLOAD_IMAGE_LOADING,
      payload: false
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const getAnimalImage = (image: string) => async (
  dispatch: IDispatch<ActionTypes>
) => {
  try {
    const {data} = await Request.post(`${API.animal}/image`, {image});
    return data;
  } catch (error) {
    return error;
  }
};

const animalReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET:
      return {
        ...state,
        animal: action.payload
      };
    case SET:
      return {
        ...state,
        animal: action.payload
      };
    case CLEAR_ANIMAL:
      return {
        ...state,
        animal: {...initialState.animal}
      };
    case SET_CREATE_ANIMAL:
      return {
        ...state,
        createAnimalState: {...state.createAnimalState, ...action.payload}
      };
    case SET_CREATE:
      return {
        ...state,
        isCreate: action.payload
      };
    case SET_BOTTOM_TAB:
      return {
        ...state,
        tab: action.payload
      };
    case GET_ANIMALS_BY_USER:
      return {
        ...state,
        animalsByUser: action.payload
      };
    case LOADING:
      return {
        ...state,
        loading: action.payload
      };
    case UPLOAD_IMAGE_LOADING:
      return {
        ...state,
        uploadImageLoading: action.payload
      };
    case GET_ANIMAL_LOADING:
      return {
        ...state,
        getAnimalLoading: action.payload
      };
    default:
      return state;
  }
};

export default animalReducer;
