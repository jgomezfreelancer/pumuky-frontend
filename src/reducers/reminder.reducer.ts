import API from '../config/api';
import Request from '../config/request';
import {IDispatch} from '../interfaces/IDispatch';

const GET_ALL = 'reminder/get_all';
const LOADING = 'reminder/loading';

export interface IReminder {
  id?: number;
  description?: string;
  calendarDate?: string;
  calendarId?: number | null;
  userId?: number;
  userCalendarId?: number;
}

interface StoreReminder {
  type: typeof GET_ALL;
  payload: IReminder;
}

interface Loading {
  type: typeof LOADING;
  payload: boolean;
}

type ActionTypes = StoreReminder | Loading;

export type IState = {
  list: IReminder[];
  loading: boolean;
};

const initialState: IState = {
  list: [],
  loading: false,
};

export const storeReminder = (body: IReminder) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const {data} = await Request.post(API.reminder, body);
    dispatch({
      type: GET_ALL,
      payload: data,
    });
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    return error;
  }
};

export const getReminder = (id: number) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  try {
    const {data} = await Request.get(`${API.reminder}/${id}`);
    return data;
  } catch (error) {
    return error;
  }
};

export const updateReminder = (body: IReminder) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  ('');
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const {data} = await Request.put(`${API.reminder}/${body.id}`, body);
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    dispatch({
      type: LOADING,
      payload: false,
    });
    return error;
  }
};

export const removeReminder = (id: number) => async (
  dispatch: IDispatch<ActionTypes>,
) => {
  dispatch({
    type: LOADING,
    payload: true,
  });
  try {
    const {data} = await Request.delete(`${API.reminder}/${id}`);
    dispatch({
      type: LOADING,
      payload: false,
    });
    return data;
  } catch (error) {
    dispatch({
      type: LOADING,
      payload: false,
    });
    return error;
  }
};

const reminderReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case GET_ALL:
      return {
        ...state,
        list: action.payload,
      };

    case LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
};

export default reminderReducer;
