import React from 'react';
import Modal from 'react-native-modal';
import {Text, TouchableHighlight, View} from 'react-native';

import Styles from './styles';
import {TouchableOpacity} from 'react-native-gesture-handler';

interface Iprops {
  status: boolean;
  handleStatus: (status: boolean) => void;
  handleConfirm: () => void;
}

const WarningModal: React.FC<Iprops> = ({
  status,
  handleStatus,
  handleConfirm,
}): JSX.Element => {
  return (
    <View>
      <Modal
        backdropColor="#95a5a6"
        useNativeDriver
        isVisible={status}
        onBackdropPress={() => handleStatus(false)}>
        <View style={Styles.modalView}>
          <View style={Styles.textContainer}>
            <Text style={Styles.title}>
              Esta seguro que desea eliminar esta mascota de su lista?
            </Text>
          </View>
          <View style={Styles.buttonsContainer}>
            <TouchableHighlight
              underlayColor="transparent"
              onPress={() => handleStatus(false)}>
              <Text style={Styles.buttonText}>Cancelar </Text>
            </TouchableHighlight>
            <TouchableHighlight
              underlayColor="transparent"
              onPress={() => handleConfirm()}>
              <Text style={Styles.buttonText}>Eliminar </Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export {WarningModal};
