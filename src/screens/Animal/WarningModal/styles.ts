import {StyleSheet, Dimensions} from 'react-native';
import colors from '../../../styles/colors';

const {width} = Dimensions.get('window');

export default StyleSheet.create({
  centeredRankingView: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    alignSelf: 'center',
    marginLeft: 10,
  },
  content: {
    backgroundColor: 'white',
    padding: 20,
    flexWrap: 'nowrap',
    width,
    justifyContent: 'center',
    flexDirection: 'column',
    paddingTop: 5,
  },
  buttonText: {
    color: colors.blue,
    fontWeight: 'bold',
  },
  buttonsContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  divider: {
    borderWidth: 2,
    width: '15%',
    borderColor: 'grey',
    borderRadius: 15,
  },
  dividerContainer: {
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  modalView: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 15,
  },
  optionTitle: {
    fontSize: 16,
    margin: 10,
  },
  textContainer: {marginBottom: 20},
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
