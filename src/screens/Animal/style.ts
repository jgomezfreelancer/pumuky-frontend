import {StyleSheet} from 'react-native';
import {isIOS} from '../../helpers/isIOS';
import colors from '../../styles/colors';

export default StyleSheet.create({
  cardContainer: {position: 'relative'},
  cardItem: {paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0},
  content: {paddingLeft: 10, paddingRight: 10},
  dummyBackground: {
    backgroundColor: colors.bgGreyLight3,
    width: '100%',
    height: 250,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center'
  },
  fieldDescriptionContainer: {width: '80%'},
  fieldTitleText: {fontSize: 20, fontWeight: 'bold', color: colors.blackLight},
  fileImagesContainer: {
    flex: 1,
    flexDirection: 'column'
  },
  fileImageTextContainer: {width: '100%', marginBottom: 10},
  fileThumbnail: {
    borderRadius: 5,
    width: 100,
    height: 100
  },
  fileContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.blue,
    padding: 15,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 10
  },
  fileImageContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.blue,
    borderRadius: 10,
    height: 100
  },
  fileIconContainer: {marginBottom: 15, marginTop: 10},
  fileText: {
    color: colors.blue,
    fontSize: 16
  },
  image: {width: '100%', height: 250},
  itemListText: {width: '100%'},
  icon: {
    borderColor: colors.transparent,
    borderWidth: 2
  },
  fieldAgeContainer: {paddingRight: 20},
  filesContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    textAlign: 'center'
  },
  formFieldContainer: {paddingRight: 10},
  listItemTitle: {
    fontWeight: 'bold',
    color: colors.blackLight,
    fontSize: 16
  },
  listItemTitlelist: {
    fontWeight: 'bold',
    color: colors.blackLight,
    textAlign: 'left',
    marginBottom: 5,
    marginTop: 5,
    width: '100%',
    fontSize: 16
  },
  listItemDetailContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingRight: 0
  },
  listTitemTextAreaContainer: {display: 'flex', flexDirection: 'column'},
  noteContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    width: '100%',
    marginBottom: 10
  },
  noteText: {
    textAlign: 'left',
    color: colors.blackLight,
    marginBottom: 10,
    width: '100%',
    fontWeight: 'bold',
    fontSize: 16
  },
  plusButton: {
    borderRadius: 25,
    padding: 10,
    borderWidth: 1,
    borderColor: 'white',
    backgroundColor: 'white',
    position: 'absolute',
    bottom: -20,
    right: 10,
    elevation: 3
  },
  plusContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 30
  },
  title: {fontWeight: 'bold', fontSize: 20, color: colors.blackLight},
  textAreaTitle: {width: '100%', fontWeight: 'bold'},
  saveButton: {
    backgroundColor: colors.blue,
    textAlign: 'center',
    width: '100%',
    padding: 15,
    marginTop: 20,
    marginBottom: 20
  },
  saveButtonText: {
    color: colors.white,
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  },
  saveButtonContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  selectAgeContainer: {width: '50%'},
  thumbnail: {
    position: 'relative',
    backgroundColor: colors.bgGreyLight2
  },
  uploadImageContainer: {width: '20%', position: 'relative'},
  uploadImageContainerButton: {position: 'relative'},
  uploadImageContainerButtonIcon: {
    position: 'absolute',
    left: isIOS ? '17%' : '20%',
    top: '20%',
    color: colors.white
  },
  valueFieldText: {
    fontSize: 16,
    color: colors.blue,
    fontWeight: 'bold'
  },
  valueFieldEditText: {
    fontSize: 16,
    color: colors.blue,
    fontWeight: 'bold',
    marginRight: 10
  },
  valueFieldEditAgeText: {
    fontSize: 16,
    color: colors.blue,
    fontWeight: 'bold',
    marginRight: 2
  },
  valueFieldEditButton: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  }
});
