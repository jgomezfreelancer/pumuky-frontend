import React, {useEffect, useState} from 'react';
import Modal from 'react-native-modal';
import {Button} from 'native-base';
import {Platform, Text, Dimensions, View} from 'react-native';
import DateTimePickerModal from '@react-native-community/datetimepicker';
import moment from 'moment';

import Styles from './styles';
import {useDispatch, useSelector} from 'react-redux';
import {setAnimalState} from '../../../reducers/animal.reducer';
import {AppRootReducer} from '../../../reducers';

const {width} = Dimensions.get('screen');

interface Iprops {
  status: boolean;
  handleStatus: (status: boolean) => void;
}

const AgeModal: React.FC<Iprops> = ({status, handleStatus}): JSX.Element => {
  /* States */
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [birthday, setBirthday] = useState<string>(
    moment().format('YYYY-MM-DD')
  );

  /* Redux */
  const {
    animalReducer: {animal}
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* Effects */
  useEffect(() => {
    if (animal.age) {
      setBirthday(moment(animal.age).format('YYYY-MM-DD'));
    }
  }, [animal.age]);

  /* Functions */

  const onDateChange = async (event: any, selectedDate: any) => {
    const currentDate = Platform.OS === 'ios' ? selectedDate : birthday;
    setDatePickerVisibility(Platform.OS === 'ios');
    setBirthday(moment.utc(currentDate).format('YYYY-MM-DD'));
  };

  const getBirthdayLanguage = () => {
    const years = moment().diff(birthday, 'years');
    const months = moment().diff(birthday, 'months') % 12;

    return `${years} ${years === 1 ? 'año' : 'años'} y ${months} ${
      months === 1 ? 'mes' : 'meses'
    }`;
  };

  const submit = () => {
    console.log('flag');
    dispatch(setAnimalState({...animal, age: birthday}));
    setBirthday(moment().format('YYYY-MM-DD'));
    handleStatus(false);
  };

  const close = () => {
    setBirthday(moment().format('YYYY-MM-DD'));
    handleStatus(false);
  };

  return (
    <View>
      <Modal
        backdropColor="#95a5a6"
        useNativeDriver
        isVisible={status}
        onBackdropPress={close}>
        <View style={Styles.modalView}>
          <View style={Styles.textContainer}>
            <Text style={Styles.title}>Edad</Text>
          </View>
          <View style={Styles.textContainer}>
            <Text style={Styles.subTitle}>¿Cuándo nació?</Text>
          </View>
          <View style={Styles.birthdayContainer}>
            {Platform.OS === 'android' && (
              <Text
                style={{textAlign: 'center', fontSize: 20}}
                onPress={() => setDatePickerVisibility(true)}>
                {birthday}
              </Text>
            )}
            {Platform.OS === 'ios' ? (
              <View
                style={{
                  width: '100%',
                  display: 'flex',
                  justifyContent: 'center',
                  paddingLeft: width / 3.9
                }}>
                <DateTimePickerModal
                  testID="dateTimePicker"
                  mode="date"
                  onChange={onDateChange}
                  display="default"
                  value={new Date(birthday)}
                  style={Styles.datePickerTextIOS}
                />
              </View>
            ) : (
              isDatePickerVisible && (
                <DateTimePickerModal
                  testID="dateTimePicker"
                  mode="date"
                  onChange={onDateChange}
                  display="default"
                  value={new Date(birthday)}
                />
              )
            )}
          </View>
          <View style={Styles.messageContainer}>
            <Text style={Styles.messageTitle}>Tu mascota tiene</Text>
            <Text style={Styles.messageSubTitle}>{getBirthdayLanguage()}</Text>
          </View>
          <View style={Styles.buttonContainer}>
            <Button transparent style={Styles.saveButton} onPress={submit}>
              <Text style={Styles.buttonText}>Guardar </Text>
            </Button>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export {AgeModal};
