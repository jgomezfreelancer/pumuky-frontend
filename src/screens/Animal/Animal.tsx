import React, {useEffect, useState} from 'react';

import {
  Container,
  Content,
  Card,
  CardItem,
  Body,
  List,
  ListItem,
  Thumbnail,
  Spinner
} from 'native-base';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  Image,
  TouchableOpacity,
  Text,
  View,
  Platform,
  PermissionsAndroid,
  Dimensions
} from 'react-native';
import moment from 'moment';
import _ from 'lodash';
import ImagePreview from 'react-native-image-preview';

import FooterBottomNavigator from '../../components/FooterBottomNavigator';
import CustomHeader from '../../components/Header';
import {pumukyBackground} from '../../assets/images';
import Styles from './style';
import colors from '../../styles/colors';
import sitemap from '../../route/sitemap';
import ListItems from '../../components/common/ListItem';
import Input from '../../components/common/form/Input';
import {useForm} from 'react-hook-form';
import {useDispatch, useSelector} from 'react-redux';
import ContentLoader, {Rect, Circle} from 'react-content-loader/native';
import DateTimePicker from '@react-native-community/datetimepicker';

import {AppRootReducer} from '../../reducers';
import {RouteProp, useRoute} from '@react-navigation/native';
import {
  createAnimal,
  getAnimal,
  getAnimalsByUser,
  IAnimalForm,
  setAnimalState,
  updateAnimal,
  uploadAnimalImage,
  ImageObject,
  clearAnimal,
  IAnimalResponse,
  setIsCreate,
  removeAnimal,
  getAnimalImage
} from '../../reducers/animal.reducer';
import {
  ImageLibraryOptions,
  ImagePickerResponse,
  launchImageLibrary
} from 'react-native-image-picker';
import WarningModal from './WarningModal';
import Toast from 'react-native-toast-message';
import AgeModal from './AgeModal';
import ReceptionDateModal from './ReceptionDateModal';
import WeightModal from './WeightModal';
import InputTextArea from '../../components/common/form/TextArea';
import {
  getVaccineByAnimal,
  setVaccineArray
} from '../../reducers/vaccine.reducer';
import {setAnimalWeight} from '../../reducers/animal-weight.reducer';
import {setCareArray} from '../../reducers/care.reducer';
import {refreshUser} from '../../reducers/login.reducer';

const {width, height} = Dimensions.get('screen');

const AnimalListSkeleton = (props: any): JSX.Element => (
  <ContentLoader
    speed={1}
    width={width}
    height={height}
    backgroundColor="#bdc3c7"
    foregroundColor="#ecebeb"
    {...props}>
    <Rect x="0" y="20" rx="3" ry="3" width="100%" height="120" />
    <Circle cx="340" cy="160" r="15" />

    <Rect x="0" y="160" rx="3" ry="3" width="75%" height="8" />
    <Rect x="0" y="180" rx="3" ry="3" width="100%" height="8" />
    <Rect x="0" y="200" rx="3" ry="3" width="100%" height="8" />
    <Rect x="0" y="220" rx="3" ry="3" width="100%" height="8" />

    <Rect x="0" y="260" rx="3" ry="3" width="30%" height="8" />
    <Rect x="0" y="280" rx="3" ry="3" width="50%" height="8" />
    <Rect x="0" y="300" rx="3" ry="3" width="50%" height="8" />
    <Rect x="0" y="320" rx="3" ry="3" width="50%" height="8" />

    <Rect x="0" y="360" rx="3" ry="3" width="30%" height="8" />
    <Rect x="0" y="380" rx="3" ry="3" width="50%" height="8" />
    <Rect x="0" y="400" rx="3" ry="3" width="50%" height="8" />
    <Rect x="0" y="420" rx="3" ry="3" width="50%" height="8" />
  </ContentLoader>
);

interface IProps {
  navigation: DrawerNavigationProp<any>;
}

type RouteParamList = {
  animal: {
    id: number;
  };
};

type FormData = {
  name: string;
  description: string;
  race: string;
  age: string;
  weight: string;
  notes: string;
};

const Animal: React.FC<IProps> = ({navigation}): JSX.Element => {
  /* States  */
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [animalImage, setAnimalImage] = useState<{uri: string}>({uri: ''});
  const [objectImage, setObjectImage] = useState<ImageObject | false>(false);
  const [statusModal, setStatusModal] = useState<boolean>(false);
  const [fileImage1, setFileImage1] = useState<{uri: string}>({uri: ''});
  const [isVisbleFileImage1, setIsVisbleFileImage1] = useState<boolean>(false);
  const [isVisbleFileImage2, setIsVisbleFileImage2] = useState<boolean>(false);
  const [isVisbleFileImage3, setIsVisbleFileImage3] = useState<boolean>(false);
  const [fileImage2, setFileImage2] = useState<{uri: string}>({uri: ''});
  const [fileImage3, setFileImage3] = useState<{uri: string}>({uri: ''});
  const [fileObjectImage1, setFileObjectImage1] = useState<ImageObject | false>(
    false
  );
  const [fileObjectImage2, setFileObjectImage2] = useState<ImageObject | false>(
    false
  );
  const [fileObjectImage3, setFileObjectImage3] = useState<ImageObject | false>(
    false
  );
  const [statusAgeModal, setStatusAgeModal] = useState<boolean>(false);
  const [
    statusReceptionDateModal,
    setStatusReceptionDateModal
  ] = useState<boolean>(false);
  const [statusWeightModal, setStatusWeightModal] = useState<boolean>(false);
  const clear = () => {
    setAnimalImage({uri: ''});
    setFileImage1({uri: ''});
    setObjectImage(false);
    setFileImage2({uri: ''});
    setFileImage3({uri: ''});
    setFileObjectImage1(false);
    setFileObjectImage2(false);
    setFileObjectImage3(false);
  };

  /* Redux  */
  const {
    animalWeightReducer: {list: animalWeightList},
    animalReducer: {
      animal,
      getAnimalLoading,
      uploadImageLoading,
      loading,
      isCreate
    },
    loginReducer: {user},
    vaccineReducer: {list},
    careReducer: {list: careList}
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* Form data  */
  const {
    control,
    errors,
    handleSubmit,
    setValue,
    reset,
    watch
  } = useForm<FormData>();
  const watchName = watch('name');

  /* Router */
  const {
    params: {id}
  } = useRoute<RouteProp<RouteParamList, 'animal'>>();

  /* Effects  */

  useEffect(() => {
    dispatch(setAnimalState({...animal, name: watchName}));
  }, [watchName]);

  useEffect(() => {
    if (isCreate) {
      clear();
      dispatch(clearAnimal());
    }
  }, [isCreate, dispatch]);

  useEffect(() => {
    if (animal.image && isCreate) {
      setAnimalImage({uri: ''});
      setFileImage1({uri: ''});
      setObjectImage(false);
      setFileImage2({uri: ''});
      setFileImage3({uri: ''});
      setFileObjectImage1(false);
      setFileObjectImage2(false);
      setFileObjectImage3(false);
    }
  }, [animal.image, isCreate]);

  useEffect(() => {
    if (!isCreate && id > 0) {
      getAnimal(id)(dispatch).then((data: IAnimalResponse) => {
        if (!_.isEmpty(data)) {
          if (data.image) {
            setAnimalImage({uri: data.image});
          }
          if (data.technicalImage1) {
            getAnimalImage(data.technicalImage1)(dispatch).then((res1: any) => {
              setFileImage1({uri: res1});
            });
          }
          if (data.technicalImage2) {
            getAnimalImage(data.technicalImage2)(dispatch).then((res2: any) => {
              setFileImage2({uri: res2});
            });
          }
          if (data.technicalImage3) {
            getAnimalImage(data.technicalImage3)(dispatch).then((res3: any) => {
              setFileImage3({uri: res3});
            });
          }
          if (data.animalVaccine) {
            getVaccineByAnimal(id)(dispatch).then((res4: any) => {
              dispatch(setVaccineArray([...res4]));
            });
          }
          if (data.animalCare) {
            dispatch(setCareArray(data.animalCare));
          }
          if (data.animalWeight) {
            dispatch(setAnimalWeight(data.animalWeight));
          }
          setValue('name', data.name);
          setValue('description', data.description);
          setValue('age', data.age);
          setValue('race', data.race);
          setValue('weight', data.weight);
          setValue('notes', data.notes);
        }
      });
    } else {
      dispatch(clearAnimal());
      setValue('name', '');
      setValue('description', '');
      setValue('age', '');
      setValue('race', '');
      setValue('weight', '');
      setValue('notes', '');
    }
  }, [id, dispatch, setValue, reset, isCreate]);

  /* Functions  */

  const handleLeft = () => {
    if (isEdit) {
      setIsEdit(false);
      dispatch(setIsCreate(false));
    } else {
      dispatch(setIsCreate(true));
      reset();
      navigation.navigate(sitemap.myPets);
    }
  };

  const onSubmit = (form: FormData) => {
    const body: IAnimalForm = {
      ...form,
      id: id,
      userId: user.id!,
      fileImage: objectImage || null,
      techImage1: fileObjectImage1 || null,
      techImage2: fileObjectImage2 || null,
      techImage3: fileObjectImage3 || null,
      vaccines: list,
      cares: careList,
      age: animal.age,
      weight: animal.weight,
      reception_date: animal.reception_date,
      food: animal.food,
      amount_schedule: animal.amount_schedule,
      suplements: animal.suplements,
      maintenance: animal.maintenance
    };
    if (id) {
      updateAnimal(body)(dispatch).then(() => {
        setIsEdit(false);
        Toast.show({
          text1: 'Mascota actualizada!',
          type: 'success',
          position: 'bottom'
        });
      });
      setIsEdit(false);
    } else {
      createAnimal(body)(dispatch).then(() => {
        setIsEdit(false);
        reset();
        dispatch(getAnimalsByUser(user.id!));
        dispatch(refreshUser(user.id!));
        Toast.show({
          text1: 'Mascota creada!',
          type: 'success',
          position: 'bottom'
        });
        navigation.navigate(sitemap.myPets);
      });
    }
  };

  const handleEdit = () => setIsEdit(true);

  const onChangeFood = (foodList: string[]) => {
    dispatch(setAnimalState({...animal, food: foodList}));
  };

  const onChangeAmountSchedule = (amount_scheduleList: string[]) => {
    dispatch(setAnimalState({...animal, amount_schedule: amount_scheduleList}));
  };

  const onChangeSuplements = (suplements: string[]) => {
    dispatch(setAnimalState({...animal, suplements}));
  };

  const onChangeMaintenance = (maintenance: string[]) => {
    dispatch(setAnimalState({...animal, maintenance}));
  };

  const upload = () => {
    const options: ImageLibraryOptions = {
      mediaType: 'photo',
      includeBase64: true,
      maxHeight: 800,
      maxWidth: 600
    };
    launchImageLibrary(options, async (response: ImagePickerResponse) => {
      if (response.didCancel) {
      } else if (response.errorCode) {
        if (Platform.OS === 'android') {
          await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
          )
            .then(() => {
              upload();
            })
            .catch((error) => console.log(error));
        }
      } else {
        const source = {uri: `data:${response.type};base64,${response.base64}`};
        const data = {
          data: response.base64,
          type: response.type,
          name: response.fileName
        };
        setAnimalImage(source);
        setObjectImage(data);
        if (id) {
          uploadAnimalImage({id: animal.id!, file: data})(dispatch).then(() => {
            setAnimalImage(source);
            dispatch(getAnimalsByUser(user.id!));
            Toast.show({
              text1: 'Foto actualizada!',
              type: 'success',
              position: 'bottom'
            });
          });
        }
      }
    });
  };

  const remove = () => {
    setStatusModal(true);
  };

  const handleStatus = (value: boolean) => {
    setStatusModal(value);
  };

  const handleStatusAgeModal = (value: boolean) => {
    setStatusAgeModal(value);
  };

  const handleReceptionDateStatus = (value: boolean) =>
    setStatusReceptionDateModal(value);

  const handleStatusWeightModal = (value: boolean) =>
    setStatusWeightModal(value);

  const getBirthdayLanguage = () => {
    const years = moment().diff(animal.age, 'years');
    const months = moment().diff(animal.age, 'months') % 12;

    return animal.age
      ? `${years} ${years === 1 ? 'año' : 'años'} y ${months} ${
          months === 1 ? 'mes' : 'meses'
        }`
      : ' Definir';
  };

  const fileUpload = (file: number) => {
    const options: ImageLibraryOptions = {
      mediaType: 'photo',
      includeBase64: true,
      maxHeight: 800,
      maxWidth: 600
    };
    launchImageLibrary(options, async (response: ImagePickerResponse) => {
      if (response.didCancel) {
      } else if (response.errorCode) {
        if (Platform.OS === 'android') {
          await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
          )
            .then(() => {
              upload();
            })
            .catch((error) => console.log(error));
        }
      } else {
        const source = {uri: `data:${response.type};base64,${response.base64}`};
        const data: ImageObject = {
          data: response.base64,
          type: response.type,
          name: response.fileName
        };
        if (file === 1) {
          setFileObjectImage1(data);
          setFileImage1(source);
        }
        if (file === 2) {
          setFileObjectImage2(data);
          setFileImage2(source);
        }
        if (file === 3) {
          setFileObjectImage3(data);
          setFileImage3(source);
        }
      }
    });
  };

  const handleConfirm = () => {
    removeAnimal({id: animal.id!, user: user.id!})(dispatch).then(() => {
      setStatusModal(false);
      navigation.navigate(sitemap.myPets);
    });
  };

  if (getAnimalLoading) {
    return (
      <Container>
        <CustomHeader
          handleLeft={handleLeft}
          leftIcon="arrow-left"
          title="Información"
          rightIcon={isEdit ? 'trash' : 'edit'}
          rightColor="white"
          handleRigth={isEdit ? remove : handleEdit}
        />
        <Content style={Styles.content}>
          <AnimalListSkeleton />
        </Content>
        <FooterBottomNavigator />
      </Container>
    );
  }

  const handleWeight = () => {
    if (isCreate) {
      setStatusWeightModal(true);
    } else {
      navigation.navigate(sitemap.animalWeight);
    }
  };

  const getLastWeight = () => {
    if (isCreate) {
      return animal.weight && animal.weight!.description
        ? `${animal.weight!.description} ${animal.weight!.type}`
        : 'Definir';
    }
    const currentList = [...animalWeightList];
    const first = currentList[0];
    return currentList.length > 0
      ? `${first.description} ${first.type}`
      : 'Definir';
  };

  return (
    <Container>
      <CustomHeader
        handleLeft={handleLeft}
        leftIcon="arrow-left"
        title="Información"
        rightIcon={isCreate ? undefined : 'trash'}
        rightColor="white"
        handleRigth={isCreate ? null : remove}
      />
      <Content>
        <List>
          {isEdit ? (
            <ListItem style={Styles.listItemDetailContainer}>
              <View style={Styles.fieldDescriptionContainer}>
                <Text style={Styles.fieldTitleText}>
                  {isCreate ? '¿Qué animal es?' : animal.description}
                </Text>
              </View>
              <View style={Styles.uploadImageContainer}>
                {uploadImageLoading ? (
                  <Spinner color={colors.blue} />
                ) : (
                  <TouchableOpacity
                    onPress={upload}
                    style={Styles.uploadImageContainerButton}>
                    <Thumbnail
                      style={Styles.thumbnail}
                      source={animalImage.uri ? animalImage : {uri: undefined}}
                    />
                    {!animalImage.uri && (
                      <Icon
                        name="camera"
                        size={30}
                        style={Styles.uploadImageContainerButtonIcon}
                      />
                    )}
                  </TouchableOpacity>
                )}
              </View>
            </ListItem>
          ) : (
            <Card style={Styles.cardContainer}>
              <CardItem style={Styles.cardItem}>
                <Body>
                  {animalImage.uri ? (
                    <Image
                      style={Styles.image}
                      source={animalImage.uri ? animalImage : pumukyBackground}
                    />
                  ) : (
                    <View style={Styles.dummyBackground}>
                      <Icon size={80} color={colors.white} name="camera" />
                    </View>
                  )}
                </Body>
                <TouchableOpacity
                  style={Styles.plusButton}
                  onPress={handleEdit}>
                  <Icon size={25} color={colors.blue} name="pen" />
                </TouchableOpacity>
              </CardItem>
            </Card>
          )}

          {!isEdit && isCreate && (
            <ListItem>
              <Text style={Styles.title}>Qué animal es?</Text>
            </ListItem>
          )}
          <ListItem style={Styles.listItemDetailContainer}>
            <Text style={Styles.listItemTitle}>Nombre</Text>

            <View>
              <Input
                control={control}
                name="name"
                textAlign="right"
                required
                error={errors.name ? errors.name.message : null}
                placeholder="Nombre..."
                editable={isEdit}
                customBlue
              />
            </View>
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <Text style={Styles.listItemTitle}>Animal</Text>
            <View>
              <Input
                control={control}
                name="description"
                textAlign="right"
                required
                error={errors.description ? errors.description.message : null}
                placeholder="Animal..."
                editable={isEdit}
                customBlue
              />
            </View>
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <Text style={Styles.listItemTitle}>Raza</Text>
            <View>
              <Input
                control={control}
                name="race"
                textAlign="right"
                error={errors.race ? errors.race.message : null}
                placeholder="Raza..."
                editable={isEdit}
                customBlue
              />
            </View>
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <View>
              <Text style={Styles.listItemTitle}>Edad</Text>
            </View>
            {isEdit ? (
              <View style={Styles.fieldAgeContainer}>
                <TouchableOpacity
                  style={Styles.valueFieldEditButton}
                  onPress={() => setStatusAgeModal(true)}>
                  <Text style={Styles.valueFieldEditAgeText}>
                    {getBirthdayLanguage()}
                  </Text>
                  <Icon
                    style={{marginLeft: 5, marginRight: -5}}
                    name="chevron-right"
                    color={colors.blackLight}
                    size={20}
                  />
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{marginRight: 20}}>
                <Text style={Styles.valueFieldText}>
                  {getBirthdayLanguage()}
                </Text>
              </View>
            )}
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <Text style={Styles.listItemTitle}>Peso</Text>
            {isEdit ? (
              <View style={Styles.formFieldContainer}>
                <TouchableOpacity
                  style={Styles.valueFieldEditButton}
                  onPress={handleWeight}>
                  <Text style={Styles.valueFieldEditText}>
                    {getLastWeight()}
                  </Text>
                  <Icon
                    style={{marginRight: 5}}
                    name="chevron-right"
                    color={colors.blackLight}
                    size={20}
                  />
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{marginRight: 20}}>
                <Text style={Styles.valueFieldText}>{getLastWeight()}</Text>
              </View>
            )}
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <Text style={Styles.listItemTitle}>Fecha de acogida</Text>
            {isEdit ? (
              <View style={Styles.formFieldContainer}>
                <TouchableOpacity
                  style={Styles.valueFieldEditButton}
                  onPress={() => handleReceptionDateStatus(true)}>
                  <Text style={Styles.valueFieldEditText}>
                    {animal.reception_date ? animal.reception_date : 'Definir'}
                  </Text>
                  <Icon
                    style={{marginRight: 5}}
                    name="chevron-right"
                    color={colors.blackLight}
                    size={20}
                  />
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{marginRight: 20}}>
                <Text style={Styles.valueFieldText}>
                  {animal.reception_date ? animal.reception_date : 'Definir'}
                </Text>
              </View>
            )}
          </ListItem>
          <ListItem style={Styles.listTitemTextAreaContainer}>
            <Text style={Styles.listItemTitlelist}>Comida</Text>
            {isEdit ? (
              <ListItems
                selected={animal.food}
                placeholder="Escriba Comida"
                onChange={onChangeFood}
              />
            ) : (
              <React.Fragment>
                {animal.food &&
                  animal.food.map((element: string, i: number) => (
                    <Text key={i} style={Styles.itemListText}>
                      - {element}
                    </Text>
                  ))}
              </React.Fragment>
            )}
          </ListItem>
          <ListItem style={Styles.listTitemTextAreaContainer}>
            <Text style={Styles.listItemTitlelist}>Cantidades y horarios</Text>
            {isEdit ? (
              <ListItems
                selected={animal.amount_schedule}
                placeholder="Escriba Cantidades y horarios"
                onChange={onChangeAmountSchedule}
              />
            ) : (
              <React.Fragment>
                {animal.amount_schedule &&
                  animal.amount_schedule.map((element: string, i: number) => (
                    <Text key={i} style={Styles.itemListText}>
                      - {element}
                    </Text>
                  ))}
              </React.Fragment>
            )}
          </ListItem>
          <ListItem style={Styles.listTitemTextAreaContainer}>
            <Text style={Styles.listItemTitlelist}>Suplementos</Text>
            {isEdit ? (
              <ListItems
                selected={animal.suplements}
                placeholder="Escriba Suplementos"
                onChange={onChangeSuplements}
              />
            ) : (
              <React.Fragment>
                {animal.suplements &&
                  animal.suplements.map((element: string, i: number) => (
                    <Text key={i} style={Styles.itemListText}>
                      - {element}
                    </Text>
                  ))}
              </React.Fragment>
            )}
          </ListItem>
          <ListItem style={Styles.listTitemTextAreaContainer}>
            <Text style={Styles.listItemTitlelist}>Mantenimiento</Text>
            {isEdit ? (
              <ListItems
                selected={animal.maintenance}
                placeholder="Escriba Mantenimiento"
                onChange={onChangeMaintenance}
              />
            ) : (
              <React.Fragment>
                {animal.maintenance &&
                  animal.maintenance.map((element: string, i: number) => (
                    <Text key={i} style={Styles.itemListText}>
                      - {element}
                    </Text>
                  ))}
              </React.Fragment>
            )}
          </ListItem>
          <ListItem style={Styles.listTitemTextAreaContainer}>
            <Text style={Styles.noteText}>Notas</Text>

            <View style={{width: '100%'}}>
              <InputTextArea
                control={control}
                name="notes"
                error={errors.notes ? errors.notes.message : null}
                placeholder="Escribir nota..."
                numberOfLines={4}
                editable={isEdit}
              />
            </View>
          </ListItem>
          <ListItem style={Styles.fileImagesContainer}>
            <View style={Styles.fileImageTextContainer}>
              <Text style={Styles.noteText}>Fichas</Text>
            </View>
            <View style={Styles.filesContainer}>
              <TouchableOpacity
                onPress={() =>
                  isEdit ? fileUpload(1) : setIsVisbleFileImage1(true)
                }>
                {fileImage1.uri !== '' ? (
                  <View>
                    <Image
                      style={Styles.fileThumbnail}
                      source={{uri: fileImage1.uri}}
                    />
                  </View>
                ) : (
                  isEdit && (
                    <View style={Styles.fileContainer}>
                      <View style={Styles.fileIconContainer}>
                        <Icon name="paperclip" color={colors.blue} size={20} />
                      </View>
                      <View>
                        <Text style={Styles.fileText}>Añadir</Text>
                      </View>
                    </View>
                  )
                )}
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  isEdit ? fileUpload(2) : setIsVisbleFileImage2(true)
                }>
                {fileImage2.uri !== '' ? (
                  <View>
                    <Image
                      style={Styles.fileThumbnail}
                      source={{uri: fileImage2.uri}}
                    />
                  </View>
                ) : (
                  isEdit && (
                    <View style={Styles.fileContainer}>
                      <View style={Styles.fileIconContainer}>
                        <Icon name="paperclip" color={colors.blue} size={20} />
                      </View>
                      <View>
                        <Text style={Styles.fileText}>Añadir</Text>
                      </View>
                    </View>
                  )
                )}
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  isEdit ? fileUpload(3) : setIsVisbleFileImage3(true)
                }>
                {fileImage3.uri !== '' ? (
                  <View>
                    <Image
                      style={Styles.fileThumbnail}
                      source={{uri: fileImage3.uri}}
                    />
                  </View>
                ) : (
                  isEdit && (
                    <View style={Styles.fileContainer}>
                      <View style={Styles.fileIconContainer}>
                        <Icon name="paperclip" color={colors.blue} size={20} />
                      </View>
                      <View>
                        <Text style={Styles.fileText}>Añadir</Text>
                      </View>
                    </View>
                  )
                )}
              </TouchableOpacity>
            </View>
          </ListItem>
        </List>
        {isEdit && (
          <View style={Styles.saveButtonContainer}>
            <TouchableOpacity
              style={Styles.saveButton}
              disabled={loading}
              onPress={handleSubmit(onSubmit)}>
              <Text style={Styles.saveButtonText}>
                {loading ? 'ESPERE...' : 'GUARDAR'}
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </Content>
      {fileImage1.uri !== '' && (
        <ImagePreview
          visible={isVisbleFileImage1}
          source={{uri: fileImage1.uri}}
          close={() => setIsVisbleFileImage1(false)}
        />
      )}

      {fileImage2.uri !== '' && (
        <ImagePreview
          visible={isVisbleFileImage2}
          source={{uri: fileImage2.uri}}
          close={() => setIsVisbleFileImage2(false)}
        />
      )}

      {fileImage3.uri !== '' && (
        <ImagePreview
          visible={isVisbleFileImage3}
          source={{uri: fileImage3.uri}}
          close={() => setIsVisbleFileImage3(false)}
        />
      )}

      <WarningModal
        status={statusModal}
        handleConfirm={handleConfirm}
        handleStatus={handleStatus}
      />
      <AgeModal status={statusAgeModal} handleStatus={handleStatusAgeModal} />
      <ReceptionDateModal
        status={statusReceptionDateModal}
        handleStatus={handleReceptionDateStatus}
      />
      <WeightModal
        status={statusWeightModal}
        handleStatus={handleStatusWeightModal}
      />
      <FooterBottomNavigator />
    </Container>
  );
};

export {Animal};
