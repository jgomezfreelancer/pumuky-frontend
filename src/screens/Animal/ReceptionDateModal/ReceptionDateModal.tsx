import React, {useState} from 'react';
import Modal from 'react-native-modal';
import {Button} from 'native-base';
import {Platform, Text, Dimensions, View} from 'react-native';
import DateTimePickerModal from '@react-native-community/datetimepicker';
import moment from 'moment';

import Styles from './styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import {
  setAnimalState,
  setCreateAnimalState
} from '../../../reducers/animal.reducer';
import {AppRootReducer} from '../../../reducers';

const {width} = Dimensions.get('screen');
interface Iprops {
  status: boolean;
  handleStatus: (status: boolean) => void;
}

const ReceptionDateModal: React.FC<Iprops> = ({
  status,
  handleStatus
}): JSX.Element => {
  /* States */
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [receptionDate, setReceptionDate] = useState<string>(
    moment().format('YYYY-MM-DD')
  );

  /* Redux */
  const {
    animalReducer: {animal}
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  const onDateChange = async (event: any, selectedDate: any) => {
    const currentDate = Platform.OS === 'ios' ? selectedDate : receptionDate;
    setDatePickerVisibility(Platform.OS === 'ios');
    setReceptionDate(moment.utc(currentDate).format('YYYY-MM-DD'));
  };

  const submit = () => {
    const beforeDate = new Date(
      Date.UTC(
        Number(moment(receptionDate).format('YYYY')),
        Number(moment(receptionDate).format('MM')),
        Number(moment(receptionDate).format('DD'))
      )
    );
    const newBeforeDate = moment(beforeDate).format('YYYY-MM-DD');
    dispatch(
      setAnimalState({
        ...animal,
        reception_date: newBeforeDate
      })
    );
    handleStatus(false);
  };

  return (
    <View>
      <Modal
        backdropColor="#95a5a6"
        useNativeDriver
        isVisible={status}
        onBackdropPress={() => handleStatus(false)}>
        <View style={Styles.modalView}>
          <View style={Styles.textContainer}>
            <Text style={Styles.title}>Fecha de acogida</Text>
          </View>
          <View style={Styles.textContainer}>
            <Text style={Styles.subTitle}>¿Cuándo llegó?</Text>
          </View>
          <View style={Styles.birthdayContainer}>
            {Platform.OS === 'android' && (
              <Button
                transparent
                onPress={() => setDatePickerVisibility(true)}
                style={Styles.buttonDate}>
                <Text style={Styles.dateText}>{receptionDate}</Text>
              </Button>
            )}
            {Platform.OS === 'ios' ? (
              <View
                style={{
                  width: '100%',
                  display: 'flex',
                  justifyContent: 'center',
                  paddingLeft: width / 3.6
                }}>
                <DateTimePickerModal
                  testID="dateTimePicker"
                  mode="date"
                  onChange={onDateChange}
                  display="default"
                  value={new Date(receptionDate)}
                  style={Styles.datePickerTextIOS}
                />
              </View>
            ) : (
              isDatePickerVisible && (
                <DateTimePickerModal
                  testID="dateTimePicker"
                  mode="date"
                  onChange={onDateChange}
                  display="default"
                  value={new Date(receptionDate)}
                />
              )
            )}
          </View>

          <View style={Styles.buttonContainer}>
            <Button transparent style={Styles.saveButton} onPress={submit}>
              <Text style={Styles.buttonText}>Guardar </Text>
            </Button>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export {ReceptionDateModal};
