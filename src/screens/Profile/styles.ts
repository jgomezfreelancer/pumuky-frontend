import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  datePickerText: {
    borderWidth: 1,
    borderColor: 'black',
    padding: 5
  },
  datePickerTextIOS: {
    padding: 5,
    backgroundColor: 'white',
    flex: 1
  },
  footerContainer: {
    backgroundColor: colors.blue
  },
  footerButtonText: {
    color: colors.white,
    fontSize: 17,
    fontWeight: 'bold'
  },
  listItemTitle: {
    fontWeight: 'bold'
  },
  listItemDetailContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingRight: 20
  },
  image: {width: 80, height: 80, borderRadius: 50},
  thumbnail: {
    opacity: 0.5,
    position: 'relative',
    borderWidth: 3,
    borderColor: 'black'
  }
});
