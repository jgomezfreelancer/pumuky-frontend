import {DrawerNavigationProp} from '@react-navigation/drawer';
import {
  Button,
  Container,
  Content,
  Footer,
  FooterTab,
  ListItem,
  Spinner,
  Thumbnail
} from 'native-base';
import React, {useEffect, useState} from 'react';
import {useForm} from 'react-hook-form';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  Image,
  PermissionsAndroid,
  Platform,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import DateTimePickerModal from '@react-native-community/datetimepicker';
import moment from 'moment';
import {
  ImageLibraryOptions,
  ImagePickerResponse,
  launchImageLibrary
} from 'react-native-image-picker';
import Toast from 'react-native-toast-message';

import Input from '../../components/common/form/Input';
import CustomHeader from '../../components/Header';
import {AppRootReducer} from '../../reducers';
import Styles from './styles';
import {
  IUser,
  updateUser,
  uploadProfileImage
} from '../../reducers/login.reducer';
import colors from '../../styles/colors';
import {pumukyBackground} from '../../assets/images';

interface IProps {
  navigation: DrawerNavigationProp<any>;
}

type FormData = {
  first_name: string;
  last_name: string;
  password: string;
};

const Profile: React.FC<IProps> = ({navigation}) => {
  /* States  */
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [birthday, setBirthday] = useState<string>(
    moment().format('YYYY-MM-DD')
  );
  const [animalImage, setAnimalImage] = useState<{uri: string}>({uri: ''});
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  /* Redux  */
  const {
    loginReducer: {user, loading, uploadProfileImageLoading}
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* Form data  */
  const {control, errors, handleSubmit, setValue} = useForm<FormData>({
    defaultValues: {
      first_name: user.first_name,
      last_name: user.last_name
    }
  });

  /* Effects */

  useEffect(() => {
    setValue('first_name', user.first_name);
    setValue('last_name', user.last_name);
    setValue('last_name', user.last_name);
  }, [user, setValue]);

  useEffect(() => {
    if (user.picture) {
      setAnimalImage({uri: user.picture!});
    }
  }, [setAnimalImage, user.picture]);

  useEffect(() => {
    if (user.birthday) {
      setBirthday(moment(user.birthday).format('YYYY-MM-DD'));
    }
  }, [user.birthday, setBirthday]);

  /* Functions */

  const onSubmit = (form: FormData) => {
    const data = {
      ...form,
      birthday: moment(birthday).format('YYYY-MM-DD')
    };
    updateUser(
      user.id!,
      data
    )(dispatch).then((res: IUser) => {
      setIsEdit(false);
      setValue('first_name', res.first_name);
      setValue('last_name', res.last_name);
      setBirthday(moment(res.birthday).format('YYYY-MM-DD'));
      Toast.show({
        text1: 'Perfil actualizado!',
        type: 'success',
        position: 'bottom'
      });
    });
  };

  const handleBack = () => {
    if (isEdit) {
      setIsEdit(false);
    } else {
      navigation.openDrawer();
    }
  };

  const handleEdit = () => setIsEdit(true);

  const onDateChange = async (event: any, selectedDate: any) => {
    const currentDate = Platform.OS === 'ios' ? selectedDate : birthday;
    setDatePickerVisibility(Platform.OS === 'ios');
    setBirthday(moment.utc(currentDate).format('YYYY-MM-DD'));
  };

  const upload = () => {
    const options: ImageLibraryOptions = {
      mediaType: 'photo',
      includeBase64: true,
      maxHeight: 800,
      maxWidth: 600
    };
    launchImageLibrary(options, async (response: ImagePickerResponse) => {
      if (response.didCancel) {
      } else if (response.errorCode) {
        if (Platform.OS === 'android') {
          await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
          )
            .then(() => {
              upload();
            })
            .catch((error) => console.log(error));
        }
      } else {
        const source = {uri: `data:${response.type};base64,${response.base64}`};
        const data = {
          data: response.base64,
          type: response.type,
          name: response.fileName
        };
        setAnimalImage(source);

        uploadProfileImage({id: user.id!, file: data})(dispatch).then(() => {
          setAnimalImage(source);
          Toast.show({
            text1: 'Foto actualizada!',
            type: 'success',
            position: 'bottom'
          });
        });
      }
    });
  };

  return (
    <Container>
      <CustomHeader
        leftIcon={isEdit ? 'arrow-left' : 'bars'}
        handleLeft={handleBack}
        title="Mi Perfil"
        handleRigth={isEdit ? null : handleEdit}
        rightIcon="edit"
        rightColor="white"
      />
      <Content>
        <ListItem style={Styles.listItemDetailContainer}>
          <View style={{width: '100%', position: 'relative'}}>
            {isEdit ? (
              <View
                style={{
                  width: '100%',
                  position: 'relative',
                  display: 'flex',
                  flexDirection: 'row',
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center'
                }}>
                {uploadProfileImageLoading ? (
                  <Spinner color={colors.blue} />
                ) : (
                  <TouchableOpacity
                    onPress={upload}
                    style={{position: 'relative'}}>
                    <Thumbnail
                      style={Styles.thumbnail}
                      source={
                        animalImage.uri !== '' ? animalImage : pumukyBackground
                      }
                    />
                    <Icon
                      name="plus"
                      size={30}
                      style={{position: 'absolute', left: '25%', top: '20%'}}
                    />
                  </TouchableOpacity>
                )}
              </View>
            ) : (
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center'
                }}>
                {animalImage.uri !== '' ? (
                  <Image style={Styles.image} source={animalImage} />
                ) : (
                  <Icon name="user-circle" size={60} color={colors.blue} />
                )}
              </View>
            )}
          </View>
        </ListItem>
        <ListItem style={Styles.listItemDetailContainer}>
          <Text style={Styles.listItemTitle}>Nombre</Text>
          <View>
            <Input
              control={control}
              name="first_name"
              textAlign="right"
              required
              error={errors.first_name ? errors.first_name.message : null}
              placeholder="Nombre..."
              editable={isEdit}
            />
          </View>
        </ListItem>
        <ListItem style={Styles.listItemDetailContainer}>
          <Text style={Styles.listItemTitle}>Apellido</Text>
          <View>
            <Input
              control={control}
              name="last_name"
              textAlign="right"
              required
              error={errors.last_name ? errors.last_name.message : null}
              placeholder="Apellido..."
              editable={isEdit}
            />
          </View>
        </ListItem>
        <ListItem style={Styles.listItemDetailContainer}>
          <Text style={Styles.listItemTitle}>Fecha de Nacimiento</Text>

          {Platform.OS === 'android' && (
            <View>
              <Text
                onPress={() => (isEdit ? setDatePickerVisibility(true) : {})}
                style={isEdit ? Styles.datePickerText : {}}>
                {birthday}
              </Text>
            </View>
          )}

          {Platform.OS === 'ios' ? (
            <View
              style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'flex-end',
                paddingLeft: 100
              }}>
              <DateTimePickerModal
                testID="dateTimePicker"
                mode="date"
                onChange={onDateChange}
                display="default"
                value={new Date(birthday)}
                style={Styles.datePickerTextIOS}
              />
            </View>
          ) : (
            isDatePickerVisible && (
              <DateTimePickerModal
                testID="dateTimePicker"
                mode="date"
                onChange={onDateChange}
                display="default"
                value={new Date(birthday)}
              />
            )
          )}
        </ListItem>
        <ListItem style={Styles.listItemDetailContainer}>
          <Text style={Styles.listItemTitle}>Contraseña</Text>
          <View>
            <Input
              control={control}
              name="password"
              textAlign="right"
              error={errors.password ? errors.password.message : null}
              placeholder="********"
              editable={isEdit}
              secureText
            />
          </View>
        </ListItem>
        <ListItem style={Styles.listItemDetailContainer}>
          <Text style={Styles.listItemTitle}>
            Puedes crear {user.petsLimit ? user.petsLimit : 1}{' '}
            {!user.petsLimit || user.petsLimit === 1 ? 'Mascota' : 'Mascotas'}
          </Text>
        </ListItem>
      </Content>
      <Footer>
        <FooterTab style={Styles.footerContainer}>
          <Button full onPress={handleSubmit(onSubmit)}>
            <Text style={Styles.footerButtonText}>
              {loading ? 'ESPERE ...' : 'GUARDAR'}
            </Text>
          </Button>
        </FooterTab>
      </Footer>
    </Container>
  );
};

export {Profile};
