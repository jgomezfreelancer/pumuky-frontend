import React from 'react';
import {Text, TouchableHighlight, View, Image} from 'react-native';
import {useForm} from 'react-hook-form';
import {useDispatch, useSelector} from 'react-redux';
import {Container, Content, Header} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

import Input from '../../components/common/form/Input2';
import {IRegister, signup} from '../../reducers/login.reducer';
import Button from '../../components/common/form/Button';
import {useNavigation} from '@react-navigation/native';
import Styles from './style';
import {AppRootReducer} from '../../reducers';
import {customTraslate} from '../../utils/language';
import Toast from 'react-native-toast-message';
import sitemap from '../../route/sitemap';
import {PumukyLogoNew} from '../../assets/images';
interface FormData extends IRegister {
  first_name: string;
  last_name: string;
  username: string;
  email: string;
  password: string;
  password2: string;
}

export type IFB = {
  email: string;
  first_name: string;
};

function Register(): JSX.Element {
  const {handleSubmit, control, errors} = useForm<FormData>();
  const dispatch = useDispatch();
  const {
    loginReducer: {loading}
  } = useSelector((state: AppRootReducer) => state);
  const navigation = useNavigation();

  const onSubmit = async (form: FormData) => {
    if (form.password !== form.password2) {
      Toast.show({
        text1: 'Contraseñas deben ser iguales',
        type: 'error'
      });
    } else {
      signup(form)(dispatch)
        .then(() => {
          navigation.navigate(sitemap.dashboard);
        })
        .catch(() => {
          Toast.show({
            text1: 'Error durante el registro de usuario, intentar de nuevo',
            type: 'error'
          });
        });
    }
  };

  const handleBack = () => {
    navigation.navigate(sitemap.mainLogin);
  };

  return (
    <Container style={Styles.root}>
      <Header style={Styles.headerStyle}>
        <View style={Styles.backButtonContainer}>
          <TouchableHighlight onPress={handleBack} underlayColor="transparent">
            <Icon name="arrow-left" size={25} color="#7b7b7b" />
          </TouchableHighlight>
        </View>
      </Header>
      <Content>
        <View style={Styles.formContainer}>
          <View style={Styles.logoContainer}>
            <Image style={Styles.image} source={PumukyLogoNew} />
          </View>
          <View style={Styles.titleContainer}>
            <Text style={Styles.registerText}>Regístrate</Text>
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="first_name"
              required
              error={errors.first_name ? errors.first_name.message : null}
              placeholder={customTraslate('nombre')}
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="last_name"
              required
              error={errors.last_name ? errors.last_name.message : null}
              placeholder={customTraslate('apellido')}
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="username"
              required
              error={errors.username ? errors.username.message : null}
              placeholder={customTraslate('correo')}
              inputType="email"
            />
            {errors.username && (
              <Text style={Styles.requiredMessage}>
                {errors.username.message}
              </Text>
            )}
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="password"
              required
              error={errors.password ? errors.password.message : null}
              placeholder={customTraslate('contrasena')}
              secureText
              iconName="lock"
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="password2"
              required
              error={errors.password2 ? errors.password2.message : null}
              placeholder={customTraslate('repetirContrasena')}
              secureText
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Button
              loading={loading}
              handleSubmit={handleSubmit(onSubmit)}
              title={customTraslate('registrarse')}
            />
          </View>
        </View>
      </Content>
    </Container>
  );
}

export default Register;
