import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  headerStyle: {
    backgroundColor: colors.blueLight1,
    elevation: 0,
    borderBottomColor: '#c8c8c8',
    paddingLeft: 10,
    paddingRight: 0
  },
  backButtonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  image: {width: 70, height: 120},
  logoContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'flex-start',
    alignItems: 'flex-start'
  },
  formContainer: {
    justifyContent: 'center',
    paddingLeft: 50,
    paddingRight: 50
  },
  registerText: {
    color: colors.blue,
    fontSize: 17,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  fieldContainer: {marginTop: 15},
  forgotPasswordText: {
    color: '#a5a5a5',
    textAlign: 'center',
    textDecorationLine: 'underline'
  },
  backgroundImage: {
    width: '100%',
    flex: 1
  },
  titleContainer: {
    marginTop: 15
  },
  requiredMessage: {paddingLeft: 10, color: 'red', fontWeight: 'bold'},
  root: {backgroundColor: colors.blueLight1}
});
