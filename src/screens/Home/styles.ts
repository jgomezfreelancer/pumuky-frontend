import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  content: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    display: 'flex',
  },
});
