import React from 'react';

import {DrawerNavigationProp} from '@react-navigation/drawer';
import {Text, View} from 'react-native';

import CustomHeader from '../../components/Header';
import Calendar from '../../components/Calendar';

interface IProps {
  navigation: DrawerNavigationProp<any>;
}

const Home: React.FC<IProps> = ({navigation}): JSX.Element => {
  const handleLeft = () => navigation.openDrawer();
  return (
    <View style={{flex: 1}}>
      <CustomHeader handleLeft={handleLeft} leftIcon="bars" title="Example" />
      <Text>Home examplesss</Text>
      <Calendar />
    </View>
  );
};

export {Home};
