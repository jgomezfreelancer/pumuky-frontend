import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    position: 'relative',
    backgroundColor: '#e7f0fe',
  },
  root: {
    width: '100%',
    height: '100%',
    backgroundColor: '#f5f2ef',
    display: 'flex',
    justifyContent: 'center',
  },
  gridContainer: {alignItems: 'center'},
  rowContainer: {alignItems: 'center'},
  text: {color: 'white', fontSize: 16, marginTop: 20},
});
