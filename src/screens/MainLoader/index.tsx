import React from 'react';
import {View, ActivityIndicator, ImageBackground} from 'react-native';
import {Container, Grid, Row} from 'native-base';
import Styles from './style';
import colors from '../../styles/colors';
import {pumukyBackground} from '../../assets/images';

const MainLoader: React.FC = (): JSX.Element => {
  return (
    <Container style={Styles.root}>
      <ImageBackground source={pumukyBackground} style={Styles.background}>
        <Grid style={Styles.gridContainer}>
          <Row style={Styles.rowContainer}>
            <View>
              <ActivityIndicator size="large" color={colors.blue} />
            </View>
          </Row>
        </Grid>
      </ImageBackground>
    </Container>
  );
};

export default MainLoader;
