import {Dimensions, StyleSheet} from 'react-native';
import {isIOS} from '../../helpers/isIOS';
import colors, {BLUE, FONT} from '../../styles/colors';

const {width} = Dimensions.get('screen');

export default StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    position: 'relative',
    backgroundColor: '#e7f0fe'
  },
  root: {
    width: '100%',
    paddingRight: -10
  },
  exploreText: {
    color: colors.blue,
    fontWeight: 'bold',
    fontSize: 17
  },
  gridLogoContainer: {
    paddingBottom: 20,
    paddingLeft: 50,
    paddingRight: 50
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 20
  },
  fieldContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {width: 125, height: 210},
  dividerLine: {
    backgroundColor: '#e2e2e2',
    height: 1,
    flex: 1,
    alignSelf: 'center',
    marginTop: 5
  },
  dividerLineText: {
    alignSelf: 'center',
    paddingHorizontal: 5,
    fontSize: 18,
    color: '#e2e2e2',
    marginLeft: 5,
    marginRight: 5
  },
  textLogoTitle: {
    color: '#53e6f3',
    fontSize: 25
  },
  loginButton: {
    borderRadius: 30,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    margin: 5,
    backgroundColor: BLUE,
    borderColor: BLUE
  },
  registerButton: {
    borderRadius: 30,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '102%',
    backgroundColor: colors.white,
    borderColor: colors.white,
    elevation: 3,
    display: 'flex',
    flexDirection: 'row',
    padding: 14,
    marginTop: 6,
    marginBottom: 2,
    marginLeft: 7
  },
  buttonText: {
    color: colors.grey,
    fontWeight: 'bold',
    fontFamily: FONT
  },
  logo: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 150,
    height: 150,
    borderRadius: 80,
    borderColor: '#fff',
    borderWidth: 5,
    textAlign: 'center'
  },
  logoText: {
    color: '#fff',
    fontSize: 25,
    fontWeight: 'bold'
  },
  logoImage: {
    width: 150,
    height: 150
  },
  orTitleContainer: {
    marginTop: 15,
    justifyContent: 'center',
    padding: isIOS ? 50 : 10,
    paddingRight: isIOS ? 30 : 10
  },
  orTitle: {
    color: colors.blueDark,
    fontSize: 17,
    marginLeft: isIOS ? 31 : 30
  },
  orTitle2: {
    color: colors.blueDark,
    fontWeight: 'bold',
    fontSize: 17,
    marginLeft: 5
  },
  imageBackground: {
    width: '100%',
    flex: 1
  },
  polygonContainer: {width: '100%', position: 'absolute', top: 0},
  polygon1: {
    height: 240,
    width: '100%',
    backgroundColor: colors.blue
  },
  polygon2: {
    height: 0,
    width: 0,
    backgroundColor: 'transparent',
    borderLeftColor: 'transparent',
    borderLeftWidth: width / 2,
    borderRightColor: 'transparent',
    borderRightWidth: width / 2,
    borderTopColor: colors.blue,
    borderTopWidth: 100
  }
});
