/* eslint-disable @typescript-eslint/camelcase */
import React, {useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Text,
  TouchableHighlight,
  View,
  NativeModules
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Container, Grid, Row} from 'native-base';
import {User} from '@react-native-community/google-signin';

import {IUser, sociaLogin} from '../../reducers/login.reducer';
import Styles from './styles';
import GoogleLogin from '../../components/GoogleLogin';
import {useNavigation} from '@react-navigation/native';
import {AppRootReducer} from '../../reducers';
import {setI18nConfig} from '../../utils/language';
import sitemap from '../../route/sitemap';
import {pumukyBackground, PumukyLogoNew} from '../../assets/images';
import colors from '../../styles/colors';
import FacebookLogin from '../../components/FacebookLogin';
import {
  AppleButton,
  appleAuth
} from '@invertase/react-native-apple-authentication';
import {isIOS} from '../../helpers/isIOS';

export type IFB = {
  email: string;
  first_name: string;
  last_name: string;
};

function MainLogin(): JSX.Element {
  const dispatch = useDispatch();
  const {
    loginReducer: {loading}
  } = useSelector((state: AppRootReducer) => state);
  const navigation = useNavigation();

  useEffect(() => {
    setI18nConfig();
    /* Active Mannually developer menu */
    //  const {DevMenu} = NativeModules;
    //  DevMenu.show();
  }, []);

  useEffect(() => {
    // onCredentialRevoked returns a function that will remove the event listener. useEffect will call this function when the component unmounts
    return appleAuth.onCredentialRevoked((res: any) => {
      console.log(
        'If this function executes, User Credentials have been Revoked'
      );
    });
  }, []); // passing in an empty array as the second argument ensures this is only ran once when component mounts initially.

  const handleGoogleLogin = async (form: User) => {
    const {
      user: {email, givenName, familyName}
    } = form;

    const body = {
      email,
      first_name: givenName,
      last_name: familyName
    };
    sociaLogin(body)(dispatch)
      .then((userRes: IUser) => {
        if (userRes.first_time) {
          navigation.navigate(sitemap.dashboard);
        } else {
          navigation.navigate(sitemap.dashboard);
        }
      })
      // eslint-disable-next-line no-console
      .catch((err: any) => console.error(err));
  };

  const handleFacebookLogin = async (form: IFB) => {
    sociaLogin(form)(dispatch)
      .then(() => {
        navigation.navigate(sitemap.dashboard);
      })
      // eslint-disable-next-line no-console
      .catch((err: any) => console.error(err));
  };

  const handleLogin = () => {
    navigation.navigate(sitemap.login);
  };

  const handleRegister = () => {
    navigation.navigate(sitemap.register);
  };

  const handleAppleIdLogin = async () => {
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: appleAuth.Operation.LOGIN,
      requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME]
    });

    const credentialState = await appleAuth.getCredentialStateForUser(
      appleAuthRequestResponse.user
    );
    if (credentialState === 1) {
      // const appleCredential = firebase.auth.AppleAuthProvider.credential(
      //   appleAuthRequestResponse.identityToken,
      //   appleAuthRequestResponse.nonce
      // );
      // console.log('appleCredential ', appleCredential);
      // firebase
      //   .auth()
      //   .signInWithCredential(appleCredential)
      //   .then((res: any) => {
      //     console.log('res', res);
      //   })
      //   .catch((error: any) => {
      //     console.log('error ', error);
      //   });

      const {email, user: uuid, fullName} = appleAuthRequestResponse;

      const body = {
        email: email || 'authWithAppleId@test.com',
        first_name: fullName?.givenName || 'authWithAppleId',
        last_name: fullName?.familyName || 'authWithAppleId',
        uuid
      };

      sociaLogin(body)(dispatch)
        .then((userRes: IUser) => {
          if (userRes.first_time) {
            navigation.navigate(sitemap.dashboard);
          } else {
            navigation.navigate(sitemap.dashboard);
          }
        })
        .catch((err: any) => console.error(err));
    }
  };

  return (
    <Container style={Styles.root}>
      <ImageBackground source={pumukyBackground} style={Styles.background}>
        <View style={Styles.polygonContainer}>
          <View style={Styles.polygon1} />
          <View style={Styles.polygon2} />
        </View>
        <Grid style={Styles.gridLogoContainer}>
          <Row size={10} style={Styles.fieldContainer}></Row>
          <Row size={30} style={Styles.logoContainer}>
            <Image style={Styles.image} source={PumukyLogoNew} />
          </Row>
          <Row size={10} style={Styles.fieldContainer}></Row>
          <Row size={10} />
          <Row size={60}>
            <Grid style={Styles.fieldContainer}>
              <Row size={10} />

              <Row size={17}>
                <AppleButton
                  buttonStyle={AppleButton.Style.BLACK}
                  buttonType={AppleButton.Type.SIGN_IN}
                  style={{
                    width: '100%',
                    height: 45
                  }}
                  onPress={() => handleAppleIdLogin()}
                />
              </Row>

              <Row size={20}>
                <GoogleLogin
                  loading={loading}
                  handleLogin={handleGoogleLogin}
                />
              </Row>
              <Row size={20}>
                <FacebookLogin loading={loading} handle={handleFacebookLogin} />
              </Row>
              <Row size={18}>
                <TouchableHighlight
                  underlayColor={colors.white}
                  style={Styles.registerButton}
                  onPress={handleRegister}>
                  <Text style={Styles.buttonText}>Regístrate</Text>
                </TouchableHighlight>
              </Row>

              <Row size={15} style={Styles.orTitleContainer}>
                <Text style={Styles.orTitle}>¿ Ya tienes una cuenta ?</Text>
                <TouchableHighlight
                  underlayColor="transparent"
                  onPress={handleLogin}>
                  <Text style={Styles.orTitle2}>Iniciar sesión</Text>
                </TouchableHighlight>
              </Row>
            </Grid>
          </Row>
        </Grid>
      </ImageBackground>
    </Container>
  );
}

export {MainLogin};
