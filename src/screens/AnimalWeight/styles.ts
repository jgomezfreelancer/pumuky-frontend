import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  content: { paddingLeft: 10, paddingRight: 15 },
  listItemDetailContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingRight: 0,
  },
  thumbnail: {
    position: 'relative',
    backgroundColor: colors.bgGreyLight2,
  },
  plusContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 30,
  },
  plusButton: {
    borderRadius: 25,
    padding: 10,
    paddingTop: 7,
    paddingBottom: 7,
    borderWidth: 1,
    borderColor: 'white',
    backgroundColor: 'white'
  },
  plusButtonContainer: {
    elevation: 10,
    borderRadius: 25
  },
  title: {
    fontSize: 20,
    color: colors.blackLight,
    fontWeight: 'bold',
  },
  date: {
    fontSize: 16,
    color: colors.blue,
  },
});
