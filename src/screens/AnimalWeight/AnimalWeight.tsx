import React, { useState } from 'react';

import { DrawerNavigationProp } from '@react-navigation/drawer';
import { Container, ListItem, Content, Thumbnail } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Text, View } from 'react-native';
import { useSelector } from 'react-redux';
import moment from 'moment';

import CustomHeader from '../../components/Header';
import { AppRootReducer } from '../../reducers';
import { IAnimalWeight } from '../../reducers/animal-weight.reducer';
import sitemap from '../../route/sitemap';
import Styles from './styles';
import { TouchableOpacity } from 'react-native-gesture-handler';
import colors from '../../styles/colors';
import WeightModal from '../Animal/WeightModal';

interface IProps {
  navigation: DrawerNavigationProp<any>;
}

const AnimalWeight: React.FC<IProps> = ({ navigation }): JSX.Element => {
  /* States */
  const [statusModal, setStatusModal] = useState<boolean>(false);
  /* Redux  */
  const {
    animalReducer: { animal },
    animalWeightReducer: { list },
  } = useSelector((state: AppRootReducer) => state);

  /* Functions  */
  const handleLeft = () => {
    navigation.navigate(sitemap.animal);
  };

  const handleCreate = () => {
    setStatusModal(true);
  };

  const handleStatusAgeModal = (booleanValue: boolean) => {
    setStatusModal(booleanValue);
  };

  return (
    <Container>
      <CustomHeader
        handleLeft={handleLeft}
        leftIcon="arrow-left"
        title="Información"
      />
      <Content style={Styles.content}>
        <ListItem style={Styles.listItemDetailContainer}>
          <Text style={Styles.title}>Peso</Text>
          <Thumbnail
            style={Styles.thumbnail}
            source={{ uri: animal.image! ? animal.image : undefined }}
          />
        </ListItem>
        {list.map((element: IAnimalWeight, i: number) => (
          <ListItem key={i} style={Styles.listItemDetailContainer}>
            <Text>
              {element.description} {element.type}
            </Text>
            <Text style={Styles.date}>
              {moment(element.created).format('DD-MM-YYYY')}
            </Text>
          </ListItem>
        ))}
        <View style={Styles.plusContainer}>
          <View style={Styles.plusButtonContainer}>
            <TouchableOpacity style={Styles.plusButton} onPress={handleCreate}>
              <Icon size={30} color={colors.blue} name="plus" />
            </TouchableOpacity>
          </View>
        </View>
        <WeightModal status={statusModal} handleStatus={handleStatusAgeModal} />
      </Content>
    </Container>
  );
};

export { AnimalWeight };
