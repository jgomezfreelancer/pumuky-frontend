import React from 'react';
import {Text, TouchableHighlight, View, Image} from 'react-native';
import {useForm} from 'react-hook-form';
import {useDispatch, useSelector} from 'react-redux';
import {Container, Content, Header} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

import Input from '../../components/common/form/Input2';
import {login} from '../../reducers/login.reducer';
import Button from '../../components/common/form/Button';
import {useNavigation} from '@react-navigation/native';
import Styles from './style';
import {AppRootReducer} from '../../reducers';
import sitemap from '../../route/sitemap';
import {PumukyLogoNew} from '../../assets/images';

type FormData = {
  username: string;
  password: string;
};

export type IFB = {
  email: string;
  first_name: string;
  last_name: string;
};

function Login(): JSX.Element {
  const {handleSubmit, control, errors} = useForm<FormData>();
  const dispatch = useDispatch();
  const {
    loginReducer: {
      loading,
      user: {first_time}
    }
  } = useSelector((state: AppRootReducer) => state);
  const navigation = useNavigation();

  const onSubmit = async (form: FormData) => {
    login(form)(dispatch)
      .then(() => {
        if (first_time) {
          navigation.navigate(sitemap.dashboard);
        } else {
          navigation.navigate(sitemap.dashboard);
        }
      })
      .catch((err) => new Error(err));
  };

  const handleBack = () => {
    navigation.navigate(sitemap.mainLogin);
  };

  const handleForgotPassword = () => {
    navigation.navigate(sitemap.forgotPassword);
  };
  if (errors.username) {
  }
  return (
    <Container style={Styles.root}>
      <Header style={Styles.headerStyles}>
        <View style={Styles.backButtonContainer}>
          <TouchableHighlight onPress={handleBack} underlayColor="transparent">
            <Icon name="arrow-left" size={25} color="#7b7b7b" />
          </TouchableHighlight>
        </View>
      </Header>
      <Content>
        <View style={Styles.loginContainer}>
          <View style={Styles.logoContainer}>
            <Image style={Styles.image} source={PumukyLogoNew} />
          </View>
          <View style={Styles.titleContainer}>
            <Text style={Styles.loginText}>Iniciar sesión</Text>
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="username"
              required
              error={errors.username ? errors.username.message : null}
              placeholder="Usuario"
              iconName="envelope"
              inputType="email"
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="password"
              required
              error={errors.password ? errors.password.message : null}
              placeholder="Clave"
              secureText
              iconName="lock"
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Text
              style={Styles.forgotPasswordText}
              onPress={handleForgotPassword}>
              Has olvidado tu contraseña ?
            </Text>
          </View>
          <View style={Styles.fieldContainer}>
            <Button
              loading={loading}
              handleSubmit={handleSubmit(onSubmit)}
              title="Iniciar sesión"
            />
          </View>
        </View>
      </Content>
    </Container>
  );
}

export default Login;
