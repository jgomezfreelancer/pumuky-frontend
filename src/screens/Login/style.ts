import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  backgroundImage: {
    width: '100%',
    flex: 1
  },
  backButtonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  fieldContainer: {marginTop: 15},
  forgotPasswordText: {
    color: '#a5a5a5',
    textAlign: 'center',
    textDecorationLine: 'underline'
  },
  headerStyles: {
    backgroundColor: colors.blueLight1,
    elevation: 0,
    borderBottomColor: '#c8c8c8',
    paddingLeft: 10,
    paddingRight: 0
  },
  image: {width: 70, height: 120},
  loginContainer: {
    justifyContent: 'center',
    paddingLeft: 50,
    paddingRight: 50
  },
  logoContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'flex-start',
    alignItems: 'flex-start'
  },
  loginText: {
    color: colors.blue,
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 17
  },
  root: {backgroundColor: colors.blueLight1},
  titleContainer: {marginTop: 50}
});
