import React, {useState} from 'react';
import Modal from 'react-native-modal';
import {Text, TouchableHighlight, View, TextInput} from 'react-native';
import moment from 'moment';

import Styles from './styles';
import {useDispatch} from 'react-redux';
import {
  setAnimalState,
  setCreateAnimalState
} from '../../../reducers/animal.reducer';
import {Radio} from 'native-base';
import colors from '../../../styles/colors';
import {color} from 'react-native-reanimated';
import Input from '../../../components/common/form/Input';

interface Iprops {
  status: boolean;
  handleStatus: (status: boolean) => void;
}

const WeightModal: React.FC<Iprops> = ({status, handleStatus}): JSX.Element => {
  /* States */
  const [unit, setUnit] = useState<number>(1);
  const [weight, setWeight] = useState<string>('1');

  /* Redux */
  const dispatch = useDispatch();

  /* Functions */
  const submit = () => {
    dispatch(
      setCreateAnimalState({
        weight: {description: weight, type: unit === 1 ? 'Kg' : 'g'}
      })
    );
    handleStatus(false);
  };

  const handleRadio = (unitValue: number) => {
    setUnit(unitValue);
  };

  const onChangeWeight = (value: string) => setWeight(value);

  return (
    <View>
      <Modal
        backdropColor="#95a5a6"
        useNativeDriver
        isVisible={status}
        onBackdropPress={() => handleStatus(false)}>
        <View style={Styles.modalView}>
          <View style={Styles.textContainer}>
            <Text style={Styles.title}>Peso</Text>
          </View>
          <View style={Styles.textContainer}>
            <Text style={Styles.subTitle}>
              Hoy {moment().format('MMMM Do YYYY')}
            </Text>
          </View>
          <View style={Styles.howMuchWeightContainer}>
            <Text style={Styles.howMuchWeightText}>¿Cuánto pesa?</Text>
          </View>
          <View style={Styles.weightContainer}>
            <View style={Styles.inputContainer}>
              <TextInput
                style={Styles.input}
                onChangeText={onChangeWeight}
                value={weight}
                keyboardType="numeric"
              />
            </View>
            <View style={Styles.unitContainer}>
              <Text style={Styles.weightContainerText}>
                {unit === 1 ? 'Kg' : 'g'}
              </Text>
            </View>
          </View>
          <View style={Styles.weightUnitContainer}>
            <Text style={Styles.weightUnitText}>Escoge una unidad de peso</Text>
          </View>
          <View style={Styles.selecWeightUnitContainer}>
            <View style={Styles.radioContainerLeft}>
              <View>
                <Radio
                  color={colors.blue}
                  selectedColor={colors.blue}
                  selected={unit === 1}
                  onPress={() => handleRadio(1)}
                />
              </View>
              <View>
                <Text style={Styles.radioText}>Kg</Text>
              </View>
            </View>
            <View style={Styles.radioContainerRight}>
              <View>
                <Radio
                  color={colors.blue}
                  selectedColor={colors.blue}
                  selected={unit === 2}
                  onPress={() => handleRadio(2)}
                />
              </View>
              <View>
                <Text style={Styles.radioText}>g</Text>
              </View>
            </View>
          </View>

          <View style={Styles.buttonContainer}>
            <TouchableHighlight
              underlayColor="transparent"
              style={Styles.saveButton}
              onPress={submit}>
              <Text style={Styles.buttonText}>Guardar </Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export {WeightModal};
