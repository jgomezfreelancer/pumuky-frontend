import {StyleSheet, Dimensions} from 'react-native';
import colors from '../../../styles/colors';

const {width} = Dimensions.get('window');

export default StyleSheet.create({
  birthdayContainer: {
    marginBottom: 20,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#D6D6D6',
    paddingTop: 10,
    paddingBottom: 10,
  },
  centeredRankingView: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    alignSelf: 'center',
    marginLeft: 10,
  },
  content: {
    backgroundColor: 'white',
    padding: 20,
    flexWrap: 'nowrap',
    width,
    justifyContent: 'center',
    flexDirection: 'column',
    paddingTop: 5,
  },
  buttonText: {
    color: colors.white,
    fontSize: 20,
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: colors.blue,
    marginLeft: -20,
    marginRight: -20,
  },
  buttonsContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  dateText: {textAlign: 'center', fontSize: 20},
  divider: {
    borderWidth: 2,
    width: '15%',
    borderColor: 'grey',
    borderRadius: 15,
  },
  dividerContainer: {
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  modalView: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 15,
  },
  messageContainer: {marginTop: 20, marginBottom: 20},
  messageTitle: {
    textAlign: 'center',
    fontSize: 16,
    color: colors.blue,
  },
  messageSubTitle: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.blue,
  },
  optionTitle: {
    fontSize: 16,
    margin: 10,
  },
  textContainer: {marginBottom: 20},
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 30,
    color: colors.blue,
  },
  subTitle: {
    textAlign: 'center',
    fontSize: 16,
  },
  saveButton: {padding: 15, paddingTop: 13, paddingBottom: 13},
});
