import React, {useState} from 'react';
import Modal from 'react-native-modal';
import {Text, TouchableHighlight, View} from 'react-native';
// import DateTimePickerModal ;
import moment from 'moment';

import Styles from './styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch} from 'react-redux';
import {setCreateAnimalState} from '../../../reducers/animal.reducer';

interface Iprops {
  status: boolean;
  handleStatus: (status: boolean) => void;
}

const ReceptionDateModal: React.FC<Iprops> = ({
  status,
  handleStatus
}): JSX.Element => {
  /* States */
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [receptionDate, setReceptionDate] = useState<string>(
    moment().format('YYYY-MM-DD')
  );

  /* Redux */
  const dispatch = useDispatch();

  // const onDateChange = async (date: any) => {
  //   setReceptionDate(moment(date).format('YYYY-MM-DD'));
  //   setDatePickerVisibility(false);
  // };

  const submit = () => {
    dispatch(setCreateAnimalState({reception_date: receptionDate}));
    handleStatus(false);
  };

  return (
    <View>
      <Modal
        backdropColor="#95a5a6"
        useNativeDriver
        isVisible={status}
        onBackdropPress={() => handleStatus(false)}>
        <View style={Styles.modalView}>
          <View style={Styles.textContainer}>
            <Text style={Styles.title}>Fecha de acogida</Text>
          </View>
          <View style={Styles.textContainer}>
            <Text style={Styles.subTitle}>¿Cuándo llegó?</Text>
          </View>
          <View style={Styles.birthdayContainer}>
            <TouchableHighlight
              underlayColor="transparent"
              onPress={() => setDatePickerVisibility(true)}>
              <Text style={Styles.dateText}>{receptionDate}</Text>
            </TouchableHighlight>
          </View>

          <View style={Styles.buttonContainer}>
            <TouchableHighlight
              underlayColor="transparent"
              style={Styles.saveButton}
              onPress={submit}>
              <Text style={Styles.buttonText}>Guardar </Text>
            </TouchableHighlight>
          </View>
        </View>
        {/* <DateTimePickerModal
          date={new Date(receptionDate)}
          isVisible={isDatePickerVisible}
          mode="date"
          onConfirm={onDateChange}
          onCancel={() => setDatePickerVisibility(false)}
        /> */}
      </Modal>
    </View>
  );
};

export {ReceptionDateModal};
