import React, {useState} from 'react';

import {Container, Content, List, ListItem, Thumbnail} from 'native-base';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  TouchableOpacity,
  Text,
  View,
  Platform,
  PermissionsAndroid,
  Image,
} from 'react-native';
import moment from 'moment';

import FooterBottomNavigator from '../../components/FooterBottomNavigator';
import CustomHeader from '../../components/Header';
import Styles from './style';
import sitemap from '../../route/sitemap';
import Input from '../../components/common/form/Input';
import {useForm} from 'react-hook-form';
import {useDispatch, useSelector} from 'react-redux';

import {AppRootReducer} from '../../reducers';
import {
  ICreateAnimal,
  setCreateAnimalState,
} from '../../reducers/animal.reducer';
import {
  ImageLibraryOptions,
  ImagePickerResponse,
  launchImageLibrary,
} from 'react-native-image-picker';
import AgeModal from './AgeModal';
import WeightModal from './WeightModal';
import ReceptionDateModal from './ReceptionDateModal';
import ListItems from '../../components/common/ListItem';
import InputTextArea from '../../components/common/form/TextArea';
import colors from '../../styles/colors';

interface IProps {
  navigation: DrawerNavigationProp<any>;
}

type FormData = {
  name: string;
  description: string;
  race: string;
  age: number;
  weight: string;
  notes: string;
};

interface ImageObject {
  data?: string;
  type?: string;
  name?: any;
}

const CreateAnimal: React.FC<IProps> = ({navigation}): JSX.Element => {
  /* States  */
  const [animalImage, setAnimalImage] = useState<{uri: string}>({uri: ''});
  const [fileImage1, setFileImage1] = useState<{uri: string}>({uri: ''});
  const [fileImage2, setFileImage2] = useState<{uri: string}>({uri: ''});
  const [fileImage3, setFileImage3] = useState<{uri: string}>({uri: ''});
  const [fileObjectImage1, setFileObjectImage1] = useState<ImageObject | false>(
    false,
  );
  const [fileObjectImage2, setFileObjectImage2] = useState<ImageObject | false>(
    false,
  );
  const [fileObjectImage3, setFileObjectImage3] = useState<ImageObject | false>(
    false,
  );
  const [statusAgeModal, setStatusAgeModal] = useState<boolean>(false);
  const [
    statusReceptionDateModal,
    setStatusReceptionDateModal,
  ] = useState<boolean>(false);
  const [statusWeightModal, setStatusWeightModal] = useState<boolean>(false);
  const [objectImage, setObjectImage] = useState<ImageObject | false>(false);

  /* Redux  */
  const {
    animalReducer: {
      createAnimalState,
      getAnimalLoading,
      uploadImageLoading,
      loading,
    },
    loginReducer: {user},
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* Form data  */
  const {control, errors, handleSubmit} = useForm<FormData>();

  /* Functions  */
  const handleLeft = () => {
    navigation.navigate(sitemap.myPets);
  };

  /* Functions  */

  const onSubmit = (form: FormData) => {
    const body: ICreateAnimal = {
      ...createAnimalState,
      image: objectImage,
      userId: user.id!,
      ...form,
    };
  };

  const onChangeFood = (foodList: string[]) => {
    dispatch(setCreateAnimalState({food: foodList}));
  };

  const onChangeAmountSchedule = (amount_scheduleList: string[]) => {
    dispatch(
      setCreateAnimalState({
        amount_schedule: amount_scheduleList,
      }),
    );
  };

  const onChangeSuplements = (suplements: string[]) => {
    dispatch(setCreateAnimalState({suplements}));
  };

  const onChangeMaintenance = (maintenance: string[]) => {
    dispatch(setCreateAnimalState({maintenance}));
  };

  const upload = () => {
    const options: ImageLibraryOptions = {
      mediaType: 'photo',
      includeBase64: true,
      maxHeight: 800,
      maxWidth: 600,
    };
    launchImageLibrary(options, async (response: ImagePickerResponse) => {
      if (response.didCancel) {
      } else if (response.errorCode) {
        if (Platform.OS === 'android') {
          await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          )
            .then(() => {
              upload();
            })
            .catch((error) => console.log(error));
        }
      } else {
        const source = {uri: `data:${response.type};base64,${response.base64}`};
        const data: ImageObject = {
          data: response.base64,
          type: response.type,
          name: response.fileName,
        };
        setObjectImage(data);
        setAnimalImage(source);
      }
    });
  };

  const fileUpload = (file: number) => {
    const options: ImageLibraryOptions = {
      mediaType: 'photo',
      includeBase64: true,
      maxHeight: 800,
      maxWidth: 600,
    };
    launchImageLibrary(options, async (response: ImagePickerResponse) => {
      if (response.didCancel) {
      } else if (response.errorCode) {
        if (Platform.OS === 'android') {
          await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          )
            .then(() => {
              upload();
            })
            .catch((error) => console.log(error));
        }
      } else {
        const source = {uri: `data:${response.type};base64,${response.base64}`};
        const data: ImageObject = {
          data: response.base64,
          type: response.type,
          name: response.fileName,
        };
        if (file === 1) {
          setFileObjectImage1(data);
          setFileImage1(source);
        }
        if (file === 2) {
          setFileObjectImage2(data);
          setFileImage2(source);
        }
        if (file === 3) {
          setFileObjectImage3(data);
          setFileImage3(source);
        }
      }
    });
  };

  const remove = () => setStatusAgeModal(true);

  const handleStatus = (value: boolean) => setStatusAgeModal(value);

  const handleReceptionDateStatus = (value: boolean) =>
    setStatusReceptionDateModal(value);

  const handleStatusWeightModal = (value: boolean) =>
    setStatusWeightModal(value);

  const getBirthdayLanguage = () => {
    const years = moment().diff(createAnimalState.age, 'years');
    const months = moment().diff(createAnimalState.age, 'months') % 12;

    return createAnimalState.age
      ? `${years} ${years === 1 ? 'año' : 'años'} y ${months} ${
          months === 1 ? 'mes' : 'meses'
        }`
      : ' Definir';
  };

  return (
    <Container>
      <CustomHeader
        handleLeft={handleLeft}
        leftIcon="arrow-left"
        title="Información"
      />
      <Content>
        <List>
          <ListItem style={Styles.listItemDetailContainer}>
            <View style={Styles.fieldDescriptionContainer}>
              <Text style={Styles.fieldTitleText}>¿Qué animal es? Create</Text>
            </View>
            <View style={Styles.uploadImageContainer}>
              <TouchableOpacity
                onPress={upload}
                style={Styles.uploadImageContainerButton}>
                <Thumbnail
                  style={Styles.thumbnail}
                  source={animalImage.uri ? animalImage : {uri: undefined}}
                />
                {animalImage.uri === '' && (
                  <Icon
                    name="camera"
                    size={30}
                    style={Styles.uploadImageContainerButtonIcon}
                  />
                )}
              </TouchableOpacity>
            </View>
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <Text style={Styles.listItemTitle}>Nombre</Text>
            <View>
              <Input
                control={control}
                name="name"
                textAlign="right"
                required
                error={errors.name ? errors.name.message : null}
                placeholder="Escribir..."
              />
            </View>
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <Text style={Styles.listItemTitle}>Animal</Text>
            <View>
              <Input
                control={control}
                name="description"
                textAlign="right"
                required
                error={errors.description ? errors.description.message : null}
                placeholder="Escribir..."
              />
            </View>
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <Text style={Styles.listItemTitle}>Raza</Text>
            <View>
              <Input
                control={control}
                name="race"
                textAlign="right"
                required
                error={errors.race ? errors.race.message : null}
                placeholder="Escribir..."
              />
            </View>
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <Text style={Styles.listItemTitle}>Edad</Text>
            <View style={Styles.fieldAgeContainer}>
              <TouchableOpacity onPress={() => setStatusAgeModal(true)}>
                <Text>{getBirthdayLanguage()}</Text>
              </TouchableOpacity>
            </View>
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <Text style={Styles.listItemTitle}>Peso</Text>
            <View style={Styles.fieldAgeContainer}>
              <TouchableOpacity onPress={() => setStatusWeightModal(true)}>
                <Text>
                  {createAnimalState.weight?.description
                    ? `${createAnimalState.weight.description} ${createAnimalState.weight.type}`
                    : 'Definir'}
                </Text>
              </TouchableOpacity>
            </View>
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <Text style={Styles.listItemTitle}>Fecha de acogida</Text>
            <View style={Styles.fieldAgeContainer}>
              <TouchableOpacity onPress={() => handleReceptionDateStatus(true)}>
                <Text>
                  {createAnimalState.reception_date
                    ? createAnimalState.reception_date
                    : 'Definir'}
                </Text>
              </TouchableOpacity>
            </View>
          </ListItem>
          <ListItem style={Styles.listTitemTextAreaContainer}>
            <ListItems
              selected={createAnimalState.food}
              placeholder="Escriba Comida"
              onChange={onChangeFood}
            />
          </ListItem>
          <ListItem style={Styles.listTitemTextAreaContainer}>
            <ListItems
              selected={createAnimalState.amount_schedule}
              placeholder="Cantidades y Horarios"
              onChange={onChangeAmountSchedule}
            />
          </ListItem>
          <ListItem style={Styles.listTitemTextAreaContainer}>
            <ListItems
              selected={createAnimalState.suplements}
              placeholder="Sumplementos"
              onChange={onChangeSuplements}
            />
          </ListItem>
          <ListItem style={Styles.listTitemTextAreaContainer}>
            <ListItems
              selected={createAnimalState.maintenance}
              placeholder="Mantenimiento"
              onChange={onChangeMaintenance}
            />
          </ListItem>
          <ListItem style={Styles.listTitemTextAreaContainer}>
            <View style={Styles.noteContainer}>
              <Text style={Styles.noteText}>Notas</Text>
            </View>
            <View style={{width: '100%'}}>
              <InputTextArea
                control={control}
                name="notes"
                required
                error={errors.notes ? errors.notes.message : null}
                placeholder="Escribir nota..."
                numberOfLines={4}
              />
            </View>
          </ListItem>
          <ListItem style={Styles.fileImagesContainer}>
            <View style={Styles.fileImageTextContainer}>
              <Text style={Styles.noteText}>Fichas</Text>
            </View>
            <View style={Styles.filesContainer}>
              <TouchableOpacity onPress={() => fileUpload(1)}>
                {fileImage1.uri !== '' ? (
                  <View>
                    <Image
                      style={Styles.fileThumbnail}
                      source={{uri: fileImage1.uri}}
                    />
                  </View>
                ) : (
                  <View style={Styles.fileContainer}>
                    <View style={Styles.fileIconContainer}>
                      <Icon name="paperclip" color={colors.blue} size={20} />
                    </View>
                    <View>
                      <Text style={Styles.fileText}>Añadir</Text>
                    </View>
                  </View>
                )}
              </TouchableOpacity>

              <TouchableOpacity onPress={() => fileUpload(2)}>
                {fileImage2.uri !== '' ? (
                  <View>
                    <Image
                      style={Styles.fileThumbnail}
                      source={{uri: fileImage2.uri}}
                    />
                  </View>
                ) : (
                  <View style={Styles.fileContainer}>
                    <View style={Styles.fileIconContainer}>
                      <Icon name="paperclip" color={colors.blue} size={20} />
                    </View>
                    <View>
                      <Text style={Styles.fileText}>Añadir</Text>
                    </View>
                  </View>
                )}
              </TouchableOpacity>

              <TouchableOpacity onPress={() => fileUpload(3)}>
                {fileImage3.uri !== '' ? (
                  <View>
                    <Image
                      style={Styles.fileThumbnail}
                      source={{uri: fileImage3.uri}}
                    />
                  </View>
                ) : (
                  <View style={Styles.fileContainer}>
                    <View style={Styles.fileIconContainer}>
                      <Icon name="paperclip" color={colors.blue} size={20} />
                    </View>
                    <View>
                      <Text style={Styles.fileText}>Añadir</Text>
                    </View>
                  </View>
                )}
              </TouchableOpacity>
            </View>
          </ListItem>
        </List>
        <View style={Styles.saveButtonContainer}>
          <TouchableOpacity
            style={Styles.saveButton}
            disabled={loading}
            onPress={handleSubmit(onSubmit)}>
            <Text style={Styles.saveButtonText}>
              {loading ? 'ESPERE...' : 'GUARDAR'}
            </Text>
          </TouchableOpacity>
        </View>
      </Content>
      <AgeModal status={statusAgeModal} handleStatus={handleStatus} />
      <ReceptionDateModal
        status={statusReceptionDateModal}
        handleStatus={handleReceptionDateStatus}
      />
      <WeightModal
        status={statusWeightModal}
        handleStatus={handleStatusWeightModal}
      />
      <FooterBottomNavigator />
    </Container>
  );
};

export {CreateAnimal};
