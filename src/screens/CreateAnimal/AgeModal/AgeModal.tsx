import React, {useState} from 'react';
import Modal from 'react-native-modal';
import {Platform, Text, TouchableHighlight, View} from 'react-native';
import DateTimePickerModal from '@react-native-community/datetimepicker';
import moment from 'moment';

import Styles from './styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch} from 'react-redux';
import {
  setAnimalState,
  setCreateAnimalState
} from '../../../reducers/animal.reducer';

interface Iprops {
  status: boolean;
  handleStatus: (status: boolean) => void;
}

const AgeModal: React.FC<Iprops> = ({status, handleStatus}): JSX.Element => {
  /* States */
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [birthday, setBirthday] = useState<string>(
    moment().format('YYYY-MM-DD')
  );

  /* Redux */
  const dispatch = useDispatch();

  const onDateChange = async (event: any, selectedDate: any) => {
    const currentDate = selectedDate || birthday;
    setDatePickerVisibility(Platform.OS === 'ios');
    setBirthday(moment(currentDate).format('YYYY-MM-DD'));
  };

  const getBirthdayLanguage = () => {
    const years = moment().diff(birthday, 'years');
    const months = moment().diff(birthday, 'months') % 12;

    return `${years} ${years === 1 ? 'año' : 'años'} y ${months} ${
      months === 1 ? 'mes' : 'meses'
    }`;
  };

  const submit = () => {
    dispatch(setCreateAnimalState({age: birthday}));
    handleStatus(false);
  };

  return (
    <View>
      <Modal
        backdropColor="#95a5a6"
        useNativeDriver
        isVisible={status}
        onBackdropPress={() => handleStatus(false)}>
        <View style={Styles.modalView}>
          <View style={Styles.textContainer}>
            <Text style={Styles.title}>Edad</Text>
          </View>
          <View style={Styles.textContainer}>
            <Text style={Styles.subTitle}>¿Cuándo nació?</Text>
          </View>
          <View style={Styles.birthdayContainer}>
            <Text
              style={{textAlign: 'center', fontSize: 20}}
              onPress={() => setDatePickerVisibility(true)}>
              {birthday}
            </Text>
          </View>
          <View style={Styles.messageContainer}>
            <Text style={Styles.messageTitle}>Tu mascota tiene</Text>
            <Text style={Styles.messageSubTitle}>{getBirthdayLanguage()}</Text>
          </View>
          <View style={Styles.buttonContainer}>
            <TouchableHighlight
              underlayColor="transparent"
              style={Styles.saveButton}
              onPress={submit}>
              <Text style={Styles.buttonText}>Guardar </Text>
            </TouchableHighlight>
          </View>
        </View>
        {isDatePickerVisible && (
          <DateTimePickerModal
            date={new Date(birthday)}
            mode="date"
            onChange={onDateChange}
            display="default"
            value={new Date(birthday)}
          />
        )}
      </Modal>
    </View>
  );
};

export {AgeModal};
