import React from 'react';

import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItem
} from '@react-navigation/drawer';
import 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager
} from 'react-native-fbsdk';

import Home from '../Home';
import MyPets from '../MyPets';

import Styles from './styles';
import {Text} from 'react-native';
import {BLUE} from '../../styles/colors';
import sitemap from '../../route/sitemap';
import Animal from '../Animal';
import AsyncStorage from '@react-native-community/async-storage';
import {useDispatch} from 'react-redux';
import {logout} from '../../reducers/login.reducer';
import {GoogleSignin} from '@react-native-community/google-signin';
import {Profile} from '../Profile/Profile';
import CreateAnimal from '../CreateAnimal';
import Report from '../Report';
import Care from '../Care';
import AnimalWeight from '../AnimalWeight';
import Calendar from '../Calendar';
import Reminder from '../Reminder';

const Drawer = createDrawerNavigator();

/* PENDIENTE POR ASIGNAR TYPE!  */
const CustomDrawerContent = (props: any) => {
  /* Redux */
  const dispatch = useDispatch();

  /*  Functions  */
  const handleNavigation = (path: string) => {
    props.navigation.toggleDrawer();
    props.navigation.navigate(path);
  };

  const googleSignOut = async () => {
    try {
      await GoogleSignin.signOut();
    } catch (error) {
      console.error(error);
    }
  };

  const facebookLogout = (accessToken: string | undefined) => {
    const logOut = new GraphRequest(
      'me/permissions/',
      {accessToken, httpMethod: 'DELETE'},
      (error, result) => {
        console.log('LogOut Result---->>>', result);
        if (error) {
          console.log('Error fetching data: ' + error.toString());
        } else {
          LoginManager.logOut();
        }
      }
    );
    new GraphRequestManager().addRequest(logOut).start();
  };

  const handleLogout = async () => {
    const token = await AsyncStorage.getItem('token');
    props.navigation.closeDrawer();
    logout(token!)(dispatch)
      .then(async () => {
        googleSignOut();
        const token = await AccessToken.getCurrentAccessToken();
        facebookLogout(token?.accessToken);
        props.navigation.navigate(sitemap.mainLogin);
      })
      .catch((err: any) => new Error(err));
  };

  return (
    <DrawerContentScrollView {...props}>
      <Text style={Styles.drawerItemTitle}>Menu</Text>
      <DrawerItem
        label="Mis mascotas"
        labelStyle={Styles.labelItem}
        style={Styles.drawerItem}
        icon={({focused, size}) => (
          <Icon color={BLUE} size={size} name={focused ? 'paw' : 'paw'} />
        )}
        onPress={() => handleNavigation(sitemap.myPets)}
      />
      <DrawerItem
        label="Mi perfil"
        labelStyle={Styles.labelItem}
        style={Styles.drawerItem}
        icon={({focused, size}) => (
          <Icon
            color={BLUE}
            size={size}
            name={focused ? 'user-circle' : 'user-circle'}
          />
        )}
        onPress={() => handleNavigation(sitemap.profile)}
      />
      <DrawerItem
        label="Calendario"
        labelStyle={Styles.labelItem}
        style={Styles.drawerItem}
        icon={({focused, size}) => (
          <Icon
            color={BLUE}
            size={size}
            name={focused ? 'calendar-alt' : 'calendar-alt'}
          />
        )}
        onPress={() => handleNavigation(sitemap.calendar)}
      />

      <DrawerItem
        label="Cerrar sesión"
        labelStyle={Styles.labelItem}
        style={Styles.drawerItem}
        icon={({focused, size}) => (
          <Icon
            color={BLUE}
            size={size}
            name={focused ? 'power-off' : 'power-off'}
          />
        )}
        onPress={handleLogout}
      />
    </DrawerContentScrollView>
  );
};

const Dashboard: React.FC = (): JSX.Element => {
  return (
    <Drawer.Navigator
      drawerContent={(props: any) => <CustomDrawerContent {...props} />}
      initialRouteName={sitemap.myPets}>
      <Drawer.Screen
        name={sitemap.home}
        component={Home}
        options={{unmountOnBlur: true}}
      />
      <Drawer.Screen name={sitemap.myPets} component={MyPets} />
      <Drawer.Screen name={sitemap.animal} component={Animal} />
      <Drawer.Screen name={sitemap.report} component={Report} />
      <Drawer.Screen name={sitemap.care} component={Care} />
      <Drawer.Screen name={sitemap.createAnimal} component={CreateAnimal} />
      <Drawer.Screen name={sitemap.animalWeight} component={AnimalWeight} />
      <Drawer.Screen
        name={sitemap.calendar}
        component={Calendar}
        options={{unmountOnBlur: true}}
      />
      <Drawer.Screen
        name={sitemap.reminder}
        component={Reminder}
        options={{unmountOnBlur: true}}
      />
      <Drawer.Screen
        name={sitemap.profile}
        component={Profile}
        options={{unmountOnBlur: true}}
      />
    </Drawer.Navigator>
  );
};
export {Dashboard};
