import React, {useEffect, useState} from 'react';
import Modal from 'react-native-modal';
import {
  Dimensions,
  KeyboardAvoidingView,
  PermissionsAndroid,
  Platform,
  Text,
  View
} from 'react-native';
import {Button, ListItem, Thumbnail} from 'native-base';
import DateTimePickerModal from '@react-native-community/datetimepicker';
import moment from 'moment';
import {
  ImageLibraryOptions,
  ImagePickerResponse,
  launchImageLibrary
} from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/FontAwesome5';

import Styles from './styles';
import {useDispatch, useSelector} from 'react-redux';
import {ImageObject} from '../../../reducers/animal.reducer';
import {AppRootReducer} from '../../../reducers';
import Input from '../../../components/common/form/Input';
import {useForm} from 'react-hook-form';
import InputTextArea from '../../../components/common/form/TextArea';
import {
  setList,
  setVaccine,
  setVaccineArray,
  updateVaccine,
  storeVaccine,
  deleteVaccine
} from '../../../reducers/vaccine.reducer';
const {width} = Dimensions.get('screen');
type FormData = {
  description: string;
  date: string;
  note: string;
};

interface Iprops {
  status: boolean;
  handleStatus: (status: boolean) => void;
}

const VaccineModal: React.FC<Iprops> = ({
  status,
  handleStatus
}): JSX.Element => {
  /* States */
  const [isDatePickerVisible, setDatePickerVisibility] = useState<boolean>(
    false
  );
  const [reportDate, setReportDate] = useState<string>(
    moment().format('YYYY-MM-DD')
  );
  const [reportImage, setReportImage] = useState<{uri: string}>({uri: ''});
  const [objectImage, setObjectImage] = useState<ImageObject | null>(null);

  /* Form data  */
  const {control, errors, handleSubmit, setValue, reset} = useForm<FormData>();

  /* Redux */
  const {
    animalReducer: {isCreate, animal},
    vaccineReducer: {vaccine, list}
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* Effects */
  useEffect(() => {
    if (vaccine.id) {
      setValue('description', vaccine.description);
      setValue('note', vaccine.note);
      setReportDate(vaccine.date!);
      setReportImage((prevState: {uri: string}) => ({
        ...prevState,
        uri: vaccine.image!
      }));
    }
  }, [vaccine, setValue]);

  /* Functions */

  const clear = () => {
    setReportImage({...reportImage, uri: ''});
    setObjectImage(null);
    setReportDate(moment().format('YYYY-MM-DD'));
    reset();
    dispatch(
      setVaccine({
        id: '',
        description: '',
        fileImage: undefined,
        image: '',
        date: '',
        note: '',
        animal: {}
      })
    );
  };

  const generateTemporalId = (): number => {
    if (list.length > 0) {
      return Math.floor(Math.random() * 100);
    }
    return 1;
  };

  const submit = async (form: FormData) => {
    if (isCreate) {
      const data = {
        ...form,
        id: generateTemporalId(),
        fileImage: objectImage,
        image: reportImage.uri,
        date: reportDate
      };
      if (vaccine.id === '') {
        dispatch(setList(data));
      } else {
        const currentList = [...list];
        const newList = currentList.filter(
          (element: any) => element.id !== vaccine.id
        );
        dispatch(setVaccineArray([data, ...newList]));
      }
    } else {
      const data = {
        ...form,
        id: vaccine.id,
        fileImage: objectImage || null,
        image: reportImage.uri,
        date: reportDate,
        animalId: animal.id
      };
      if (vaccine.id) {
        dispatch(updateVaccine(data));
      } else {
        dispatch(storeVaccine(data));
      }
    }
    clear();
    handleStatus(false);
  };

  const onDateChange = async (event: any, selectedDate: anyy) => {
    const currentDate = Platform.OS === 'ios' ? selectedDate : reportDate;
    setDatePickerVisibility(Platform.OS === 'ios');
    setReportDate(moment.utc(currentDate).format('YYYY-MM-DD'));
  };

  const handleShowDate = (value: boolean) => {
    setDatePickerVisibility(value);
  };

  const upload = () => {
    const options: ImageLibraryOptions = {
      mediaType: 'photo',
      includeBase64: true,
      maxHeight: 800,
      maxWidth: 600
    };
    launchImageLibrary(options, async (response: ImagePickerResponse) => {
      if (response.didCancel) {
      } else if (response.errorCode) {
        if (Platform.OS === 'android') {
          await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
          )
            .then(() => {
              upload();
            })
            .catch((error) => console.log(error));
        }
      } else {
        const source = {uri: `data:${response.type};base64,${response.base64}`};
        const data = {
          data: response.base64,
          type: response.type,
          name: response.fileName
        };
        setReportImage(source);
        setObjectImage(data);
      }
    });
  };

  const remove = () => {
    if (isCreate) {
      const currentList = [...list];
      const newList = currentList.filter(
        (element: any) => element.id !== vaccine.id
      );
      dispatch(setVaccineArray([...newList]));
    } else {
      dispatch(deleteVaccine({id: Number(vaccine.id)!, animalId: animal.id!}));
    }
    clear();
    handleStatus(false);
  };

  const close = () => {
    clear();
    handleStatus(false);
  };

  return (
    <View>
      <Modal
        backdropColor="#95a5a6"
        useNativeDriver
        isVisible={status}
        onBackdropPress={close}>
        <KeyboardAvoidingView behavior="position" enabled>
          <View style={Styles.modalView}>
            <View>
              <Text style={Styles.title}>Nuevo Informe</Text>
            </View>
            <ListItem style={Styles.listItemDetailContainer}>
              <View>
                <Text>Titulo</Text>
              </View>
              <View>
                <Input
                  control={control}
                  name="description"
                  textAlign="right"
                  required
                  error={errors.description ? errors.description.message : null}
                  placeholder="Titulo..."
                />
              </View>
            </ListItem>
            <ListItem style={Styles.listItemDetailContainer}>
              <View>
                <Text>Fecha</Text>
              </View>
              <View style={Platform.OS === 'ios' ? {width: '60%'} : {}}>
                {Platform.OS === 'android' && (
                  <Button
                    transparent
                    style={Styles.buttonDate}
                    onPress={() => handleShowDate(true)}>
                    <Text>{reportDate}</Text>
                  </Button>
                )}

                {Platform.OS === 'ios' ? (
                  <View
                    style={{
                      width: '100%',
                      display: 'flex',
                      justifyContent: 'center',
                      paddingLeft: width / 6
                    }}>
                    <DateTimePickerModal
                      testID="dateTimePicker"
                      mode="date"
                      onChange={onDateChange}
                      display="default"
                      value={new Date(reportDate)}
                      style={Styles.datePickerTextIOS}
                    />
                  </View>
                ) : (
                  isDatePickerVisible && (
                    <DateTimePickerModal
                      testID="dateTimePicker"
                      mode="date"
                      onChange={onDateChange}
                      display="default"
                      value={new Date(reportDate)}
                    />
                  )
                )}
              </View>
            </ListItem>
            <ListItem style={Styles.listItemDetailContainer}>
              <View>
                <Text>Añadir imagen</Text>
              </View>
              <View>
                <Button
                  transparent
                  onPress={upload}
                  style={Styles.uploadImageContainerButton}>
                  <Thumbnail
                    style={Styles.thumbnail}
                    source={reportImage.uri ? reportImage : {uri: undefined}}
                  />
                  {!reportImage.uri && (
                    <Icon
                      name="camera"
                      size={20}
                      style={Styles.uploadImageContainerButtonIcon}
                    />
                  )}
                </Button>
              </View>
            </ListItem>
            <ListItem style={Styles.listTitemTextAreaContainer}>
              <View style={Styles.noteContainer}>
                <Text style={Styles.noteText}>Notas</Text>
              </View>
              <View style={Styles.inputTextAreaContainer}>
                <InputTextArea
                  control={control}
                  name="note"
                  required
                  error={errors.note ? errors.note.message : null}
                  placeholder="Escribir nota..."
                  numberOfLines={4}
                />
              </View>
            </ListItem>
            <View style={Styles.buttonContainer}>
              <Button
                transparent
                style={Styles.saveButton}
                onPress={handleSubmit(submit)}>
                <Text style={Styles.buttonText}>Guardar </Text>
              </Button>
            </View>
            {vaccine.id !== '' && (
              <View style={Styles.buttonContainerRemove}>
                <Button transparent style={Styles.saveButton} onPress={remove}>
                  <Text style={Styles.buttonText}>Elminar </Text>
                </Button>
              </View>
            )}
          </View>
        </KeyboardAvoidingView>
      </Modal>
    </View>
  );
};

export {VaccineModal};
