import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  content: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    display: 'flex'
  },
  listItemDetailContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingRight: 0,
    paddingBottom: 25
  },
  plusContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 30
  },
  plusButton: {
    borderRadius: 25,
    padding: 10,
    paddingTop: 7,
    paddingBottom: 7,
    borderWidth: 1,
    borderColor: '#d9d9d9',
    backgroundColor: 'white',
    shadowColor: '#000000',
    shadowOpacity: 0.3,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 1
    }
  },
  plusButtonContainer: {
    elevation: 10,
    borderRadius: 25
  },
  reportItemContainer: {width: '100%', display: 'flex', flexDirection: 'row'},
  reportItemDetail: {width: '75%', marginRight: 5},
  reportItemDetailText1: {
    fontSize: 20,
    color: colors.blackLight,
    fontWeight: 'bold'
  },
  reportItemDetailText2: {
    fontSize: 16,
    color: colors.blackLight
  },
  reportItemDetailText3: {
    fontSize: 16,
    color: colors.blue
  },
  reportPicture: {
    width: '15%',
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: 'red'
  },
  thumbnail: {
    position: 'relative',
    backgroundColor: colors.bgGreyLight4,
    width: 65,
    height: 65,
    borderRadius: 5
  },
  uploadImageContainerButtonIcon: {
    position: 'absolute',
    left: '20%',
    top: '18%',
    color: colors.white
  }
});
