import React, {useState} from 'react';

import {DrawerNavigationProp} from '@react-navigation/drawer';
import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ImagePreview from 'react-native-image-preview';

import CustomHeader from '../../components/Header';
import {Button, Container, Content, ListItem, Thumbnail} from 'native-base';
import FooterBottomNavigator from '../../components/FooterBottomNavigator';
import sitemap from '../../route/sitemap';
import Styles from './styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import colors from '../../styles/colors';
import VaccineModal from './VaccineModal';
import {useDispatch, useSelector} from 'react-redux';
import {AppRootReducer} from '../../reducers';
import {IVaccine, setVaccine} from '../../reducers/vaccine.reducer';
import WarningModal from '../Animal/WarningModal';
import {removeAnimal} from '../../reducers/animal.reducer';

interface IProps {
  navigation: DrawerNavigationProp<any>;
}

const Report: React.FC<IProps> = ({navigation}): JSX.Element => {
  /* States */
  const [statusModal, setStatusModal] = useState<boolean>(false);
  const [fileImage, setFileImage] = useState<{uri: string}>({uri: ''});
  const [isVisbleFileImage, setIsVisbleFileImage] = useState<boolean>(false);
  const [statusWarningModal, setStatusWarningModal] = useState<boolean>(false);

  /* Redux */
  const {
    vaccineReducer: {list},
    animalReducer: {isCreate, animal},
    loginReducer: {user}
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* Functions */
  const handleLeft = () => navigation.navigate(sitemap.myPets);

  const handleStatusModal = (value: boolean) => setStatusModal(value);

  const handleStatusWarningModal = (value: boolean) =>
    setStatusWarningModal(value);

  const handleCreate = () => setStatusModal(true);

  const handleEditVaccine = (vaccine: IVaccine) => {
    dispatch(setVaccine(vaccine));
    setStatusModal(true);
  };

  const remove = () => {
    setStatusWarningModal(true);
  };

  const handleConfirm = () => {
    removeAnimal({id: animal.id!, user: user.id!})(dispatch).then(() => {
      setStatusWarningModal(false);
      navigation.navigate(sitemap.myPets);
    });
  };

  const handleOpenImage = (image: string) => {
    if (image) {
      setIsVisbleFileImage(true);
      setFileImage({...fileImage, uri: image});
    } else {
      setIsVisbleFileImage(false);
    }
  };

  const handleCloseImage = () => {
    setIsVisbleFileImage(false);
    setFileImage({...fileImage, uri: ''});
  };

  return (
    <Container>
      <CustomHeader
        handleLeft={handleLeft}
        leftIcon="arrow-left"
        title="Informes"
        rightIcon={isCreate ? undefined : 'trash'}
        rightColor="white"
        handleRigth={isCreate ? null : remove}
      />
      <Content>
        {list.map((element: IVaccine, i: number) => (
          <ListItem key={i} style={Styles.listItemDetailContainer}>
            <View style={Styles.reportItemContainer}>
              <Button
                transparent
                onPress={() => handleEditVaccine(element)}
                style={{marginTop: 10}}>
                <View style={Styles.reportItemDetail}>
                  <Text style={Styles.reportItemDetailText1}>
                    {element.description}
                  </Text>
                  <Text style={Styles.reportItemDetailText2}>
                    {element.note}
                  </Text>
                  <Text style={Styles.reportItemDetailText3}>
                    {element.date}
                  </Text>
                </View>
              </Button>
              <View style={Styles.reportPicture}>
                <Button onPress={() => handleOpenImage(element.image!)}>
                  <Thumbnail
                    style={Styles.thumbnail}
                    source={{uri: element.image ? element.image : undefined}}
                  />
                  {!element.image && (
                    <Icon
                      name="camera"
                      size={30}
                      style={Styles.uploadImageContainerButtonIcon}
                    />
                  )}
                </Button>
              </View>
            </View>
          </ListItem>
        ))}
        <View style={Styles.plusContainer}>
          <View style={Styles.plusButtonContainer}>
            <TouchableOpacity style={Styles.plusButton} onPress={handleCreate}>
              <Icon size={30} color={colors.blue} name="plus" />
            </TouchableOpacity>
          </View>
        </View>
        {fileImage.uri !== '' && (
          <ImagePreview
            visible={isVisbleFileImage}
            source={{uri: fileImage.uri}}
            close={handleCloseImage}
          />
        )}
      </Content>

      <WarningModal
        status={statusWarningModal}
        handleConfirm={handleConfirm}
        handleStatus={handleStatusWarningModal}
      />
      <VaccineModal status={statusModal} handleStatus={handleStatusModal} />
      <FooterBottomNavigator />
    </Container>
  );
};

export {Report};
