import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  root: {backgroundColor: colors.blueLight1},
  logoContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'flex-start',
    alignItems: 'flex-start'
  },
  headerStyles: {
    backgroundColor: colors.blueLight1,
    elevation: 0,
    borderBottomColor: '#c8c8c8',
    paddingLeft: 10,
    paddingRight: 0
  },
  backButtonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  image: {width: 70, height: 120},
  loginContainer: {
    justifyContent: 'center',
    paddingLeft: 50,
    paddingRight: 50
  },
  fieldContainer: {marginTop: 15},
  infoText: {
    color: '#a5a5a5',
    textAlign: 'center',
    textDecorationLine: 'underline',
    fontSize: 13
  },
  titleContainer: {
    marginTop: 50
  },
  loginText: {
    color: colors.blue,
    fontSize: 17,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  backgroundImage: {
    width: '100%',
    flex: 1
  }
});
