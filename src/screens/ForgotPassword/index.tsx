import React from 'react';
import {Text, TouchableHighlight, View, Image} from 'react-native';
import {useForm} from 'react-hook-form';
import {useDispatch, useSelector} from 'react-redux';
import {Container, Content, Header} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

import Input from '../../components/common/form/Input2';
import Button from '../../components/common/form/Button';
import {useNavigation} from '@react-navigation/native';
import Styles from './style';
import {AppRootReducer} from '../../reducers';
import {forgotPassword} from '../../reducers/login.reducer';
import {customTraslate} from '../../utils/language';
import {PumukyLogoNew} from '../../assets/images';

type FormData = {
  username: string;
};

export type IFB = {
  email: string;
  first_name: string;
  last_name: string;
};

function ForgotPassword(): JSX.Element {
  const {handleSubmit, control, errors} = useForm<FormData>();
  const dispatch = useDispatch();
  const {
    loginReducer: {loading}
  } = useSelector((state: AppRootReducer) => state);
  const navigation = useNavigation();

  const onSubmit = async (form: FormData) => {
    dispatch(forgotPassword(form.username));
  };

  const handleBack = () => {
    navigation.navigate('Login');
  };

  return (
    <Container style={Styles.root}>
      <Header style={Styles.headerStyles}>
        <View style={Styles.backButtonContainer}>
          <TouchableHighlight onPress={handleBack} underlayColor="transparent">
            <Icon name="arrow-left" size={25} color="#7b7b7b" />
          </TouchableHighlight>
        </View>
      </Header>
      <Content>
        <View style={Styles.loginContainer}>
          <View style={Styles.logoContainer}>
            <Image style={Styles.image} source={PumukyLogoNew} />
          </View>
          <View style={Styles.titleContainer}>
            <Text style={Styles.loginText}>Recuperar contraseña</Text>
          </View>
          <View style={Styles.fieldContainer}>
            <Input
              control={control}
              name="username"
              required
              error={errors.username ? errors.username.message : null}
              placeholder={customTraslate('correo')}
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Button
              loading={loading}
              handleSubmit={handleSubmit(onSubmit)}
              title={customTraslate('enviar')}
            />
          </View>
          <View style={Styles.fieldContainer}>
            <Text style={Styles.infoText}>
              {customTraslate('recuperarContrasenaMensaje')}
            </Text>
          </View>
        </View>
      </Content>
    </Container>
  );
}

export default ForgotPassword;
