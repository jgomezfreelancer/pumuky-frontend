import React, {useEffect, useState} from 'react';
import {Platform, Text, View, Dimensions} from 'react-native';
import {
  Button,
  Container,
  Content,
  ListItem,
  Switch,
  Spinner
} from 'native-base';
import DateTimePickerModal from '@react-native-community/datetimepicker';
import moment from 'moment';

import Styles from './styles';
import {useDispatch, useSelector} from 'react-redux';
import {AppRootReducer} from '../../reducers';
import Input3 from '../../components/common/form/Input3';
import {useForm} from 'react-hook-form';
import {
  getReminder,
  removeReminder,
  storeReminder,
  updateReminder
} from '../../reducers/reminder.reducer';
import CustomHeader from '../../components/Header';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import sitemap from '../../route/sitemap';
import {RouteProp, useRoute} from '@react-navigation/native';
import {getAllCalendar} from '../../reducers/calendar.reducer';
import {monthNames} from '../../utils/helpers';
import colors from '../../styles/colors';

const {width} = Dimensions.get('screen');

type FormData = {
  description: string;
  date: string;
  note: string;
};

type RouteParamList = {
  reminder: {
    id: number;
    selectedDate: string;
  };
};

interface IProps {
  navigation: DrawerNavigationProp<any>;
}

const Reminder: React.FC<IProps> = ({navigation}): JSX.Element => {
  /* Router */
  const {
    params: {id: reminderId, selectedDate}
  } = useRoute<RouteProp<RouteParamList, 'reminder'>>();

  /* Redux */
  const {
    loginReducer: {user},
    reminderReducer: {loading}
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* States */
  const [showStart, setShowStart] = useState<boolean>(false);
  const [startDate, setStartDate] = useState<string>(
    moment().format('YYYY-MM-DD HH:mm:ss')
  );
  const [isAlert, setIsAlert] = useState<boolean>(false);

  const [reportImage, setReportImage] = useState<{uri: string}>({uri: ''});

  /* Form data  */
  const {control, errors, handleSubmit, setValue, reset} = useForm<FormData>();

  /* Functions */

  const clear = () => {
    setReportImage({...reportImage, uri: ''});
    reset();
  };

  const submit = async (form: FormData) => {
    if (reminderId) {
      const data = {
        ...form,
        start: `${moment(startDate).format('YYYY-MM-DD')}T${moment(
          startDate
        ).format('HH:mm:ss')}`,
        isAlert,
        id: reminderId,
        userId: user.id
      };
      updateReminder(data)(dispatch).then(() => {
        getAllCalendar(user.id!)(dispatch).then(() => {
          clear();
          navigation.navigate(sitemap.calendar);
        });
      });
    } else {
      const data = {
        ...form,
        calendarDate: moment(selectedDate).format('YYYY-MM-DD'),
        calendarId: null,
        start: `${moment(startDate).format('YYYY-MM-DD')}T${moment(
          startDate
        ).format('HH:mm:ss')}`,
        isAlert,
        userId: user.id!
      };
      storeReminder(data)(dispatch).then(() => {
        getAllCalendar(user.id!)(dispatch).then(() => {
          clear();
          navigation.navigate(sitemap.calendar);
        });
      });
    }
  };

  const remove = () => {
    removeReminder(reminderId)(dispatch).then(() => {
      getAllCalendar(user.id!)(dispatch).then(() => {
        clear();
        navigation.navigate(sitemap.calendar);
      });
    });
  };

  const handleLeft = () => {
    navigation.navigate(sitemap.calendar);
  };

  const parseToday = () => {
    const currentDate = moment(selectedDate);
    const month: string | undefined = monthNames.find(
      (_: string, i: number) => i == currentDate.month()
    );
    return `${month} ${currentDate.format('DD')}, ${currentDate.format(
      'YYYY'
    )} `;
  };

  const onChangeStartDate = (event: any, selectedDate: any) => {
    const currentDate = selectedDate || startDate;
    setShowStart(Platform.OS === 'ios');
    setStartDate(moment(currentDate).format('YYYY-MM-DD HH:mm:ss'));
  };

  const onAlertChange = (alertEvent: boolean) => setIsAlert(alertEvent);

  /* Effects */
  useEffect(() => {
    if (reminderId) {
      getReminder(reminderId)(dispatch).then((data: any) => {
        setValue('description', data.description);
        if (data.start) {
          setStartDate(data.start);
        }
        if (data.is_alert) {
          setIsAlert(data.is_alert);
        }
      });
    }
  }, [dispatch, reminderId, setValue]);

  return (
    <Container>
      <CustomHeader
        handleLeft={handleLeft}
        leftIcon="arrow-left"
        title="Información"
      />
      <Content>
        <View style={Styles.modalView}>
          <View>
            <Text style={Styles.title}>Evento</Text>
          </View>
          <ListItem style={Styles.listItemDetailContainer}>
            <View style={{width: '100%'}}>
              <Text
                style={{textAlign: 'center', fontSize: 16, fontWeight: 'bold'}}>
                {parseToday()}
              </Text>
            </View>
          </ListItem>
          <ListItem style={Styles.descriptionText}>
            <Input3
              control={control}
              name="description"
              required
              error={errors.description ? errors.description.message : null}
              placeholder="Descripción..."
            />
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <View>
              <Text>Empieza</Text>
            </View>
            <View style={{width: '60%'}}>
              {Platform.OS === 'android' && (
                <Button
                  transparent
                  style={Styles.buttonDate}
                  onPress={() => setShowStart(true)}>
                  <Text>{moment(startDate).format('hh:mm A')}</Text>
                </Button>
              )}

              {Platform.OS === 'ios' ? (
                <View
                  style={{
                    width: '100%',
                    display: 'flex',
                    justifyContent: 'center',
                    paddingLeft: width / 3.4
                  }}>
                  <DateTimePickerModal
                    testID="dateTimePicker"
                    mode="time"
                    onChange={onChangeStartDate}
                    display="default"
                    value={new Date(startDate)}
                  />
                </View>
              ) : (
                showStart && (
                  <DateTimePickerModal
                    testID="dateTimePicker"
                    mode="time"
                    onChange={onChangeStartDate}
                    display="default"
                    value={new Date(startDate)}
                  />
                )
              )}
            </View>
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <View>
              <Text>Aviso</Text>
            </View>
            <View>
              <Switch
                thumbColor="#fff"
                trackColor={{true: colors.blue, false: 'grey'}}
                value={isAlert}
                onValueChange={onAlertChange}
              />
            </View>
          </ListItem>
          {/* Funcionality in on hold 
          
          <ListItem style={Styles.listItemDetailContainer}>
            <View>
              <Text>Empieza</Text>
            </View>
            <View>
              <Button
                transparent
                style={Styles.buttonDate}
                onPress={() => handleShowStartDate(true)}>
                <Text>{moment(startDate).format('YYYY-MM-DD')}</Text>
              </Button>
            </View>
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <View>
              <Text>Termina</Text>
            </View>
            <View>
              <Button
                transparent
                style={Styles.buttonDate}
                onPress={() => handleShowEndDate(true)}>
                <Text>{moment(endDate).format('HH:mm')}</Text>
              </Button>
            </View>
          </ListItem> */}
          <View style={Styles.buttonContainer}>
            <Button
              transparent
              style={Styles.saveButton}
              onPress={handleSubmit(submit)}>
              <Text style={Styles.buttonText}>
                {loading ? <Spinner color={colors.white} /> : 'Guardar'}
              </Text>
            </Button>
          </View>
          {reminderId && (
            <View style={Styles.buttonContainerRemove}>
              <Button transparent style={Styles.saveButton} onPress={remove}>
                <Text style={Styles.buttonText}>
                  {loading ? <Spinner color={colors.white} /> : 'Eliminar'}{' '}
                </Text>
              </Button>
            </View>
          )}
        </View>
      </Content>
    </Container>
  );
};

export {Reminder};
