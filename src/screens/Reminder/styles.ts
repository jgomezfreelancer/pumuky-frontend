import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: colors.blue,
    marginLeft: -20,
    marginRight: -20,
    marginTop: 20,
    padding: 2,
  },
  buttonContainerRemove: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: colors.blueLight,
    marginLeft: -20,
    marginRight: -20,
    marginBottom: -20,
    padding: 2,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
  buttonText: {
    color: colors.white,
    fontSize: 20,
  },
  buttonDate: { height: 20 },
  listItemDetailContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingRight: 0,
  },
  descriptionText: { paddingTop: 3, paddingBottom: 3 },
  inputTextAreaContainer: { width: '100%' },
  listTitemTextAreaContainer: { display: 'flex', flexDirection: 'column' },
  modalView: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 15,
  },
  noteContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    width: '100%',
    marginBottom: 10,
  },
  noteText: { textAlign: 'left', color: colors.blackLight },
  saveButton: { padding: 20 },
  title: {
    width: '100%',
    fontSize: 30,
    textAlign: 'center',
    color: colors.blue,
    fontWeight: 'bold',
  },
  thumbnail: {
    position: 'relative',
    backgroundColor: colors.blue,
    width: 40,
    height: 40,
  },
  uploadImageContainerButton: { position: 'relative' },
  uploadImageContainerButtonIcon: {
    position: 'absolute',
    left: '25%',
    top: '30%',
    color: colors.white,
  },
});
