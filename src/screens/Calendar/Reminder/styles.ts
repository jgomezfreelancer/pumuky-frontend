import { StyleSheet } from 'react-native';
import colors from '../../../styles/colors';

export default StyleSheet.create({
  dateText: {
    backgroundColor: colors.blue,
    color: 'white',
    borderRadius: 15,
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 2,
    paddingBottom: 2,
    marginTop: 10,
    fontWeight: 'bold'
  },
  eventText: { fontWeight: 'bold' },
  root: { padding: 10, marginRight: 10, marginTop: 17 },
});
