import React from 'react';

import { Text } from 'react-native';
import moment from 'moment'
import { Body, Card, CardItem } from 'native-base';

import Styles from './styles';
import colors from '../../../styles/colors';

interface ItemProps {
  reminder: any;
  handle: (id: number) => void;
}

const Reminder: React.FC<ItemProps> = ({ reminder, handle }) => {
  return (
    <Card style={Styles.root}>
      <CardItem button onPress={() => handle(reminder)}>
        <Body>
          <Text><Text style={Styles.eventText}>Evento:</Text> {reminder.description}</Text>
          {reminder.start && (
            <Text style={Styles.dateText}>
              {moment(reminder.start).format('hh:mm A')}
            </Text>
          )}
        </Body>
      </CardItem>
    </Card>
  );
};

export { Reminder };
