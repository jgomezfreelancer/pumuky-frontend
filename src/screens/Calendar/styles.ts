import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  calendarIndicatoricon: { fontWeight: 'bold', fontSize: 17, backgroundColor: colors.blue, color: colors.white, borderRadius: 15, paddingLeft: 15, paddingRight: 15 },
  eventButtonContainer: { position: 'absolute', bottom: 20, right: 20 },
  eventButton: { borderRadius: 20, padding: 20, backgroundColor: colors.blue },
  eventButtonText: { color: colors.white, fontWeight: 'bold' },
  root: { flex: 1, position: 'relative' },
});
