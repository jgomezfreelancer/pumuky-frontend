import React, {useEffect, useState} from 'react';
import Modal from 'react-native-modal';
import {Text, View} from 'react-native';
import {Button, ListItem} from 'native-base';
import moment from 'moment';

import Styles from './styles';
import {useDispatch, useSelector} from 'react-redux';
import {AppRootReducer} from '../../../reducers';
import Input from '../../../components/common/form/Input';
import {useForm} from 'react-hook-form';
import {
  setVaccine,
  setVaccineArray,
  deleteVaccine
} from '../../../reducers/vaccine.reducer';
import {storeReminder} from '../../../reducers/reminder.reducer';

type FormData = {
  description: string;
  date: string;
  note: string;
};

interface Iprops {
  status: boolean;
  handleStatus: (status: boolean) => void;
}

const ReminderModal: React.FC<Iprops> = ({
  status,
  handleStatus
}): JSX.Element => {
  /* States */
  const [startDate, setStartDate] = useState<string>(
    moment().format('YYYY-MM-DD HH:mm:ss')
  );

  const [startTime, setStartTime] = useState<string>(
    moment().format('HH:mm:ss')
  );

  const [endDate, setEndDate] = useState<string>(
    moment().add(1, 'hour').format('YYYY-MM-DD HH:mm:ss')
  );
  const [reportImage, setReportImage] = useState<{uri: string}>({uri: ''});

  /* Form data  */
  const {control, errors, handleSubmit, setValue, reset} = useForm<FormData>();

  /* Redux */
  const {
    animalReducer: {isCreate, animal},
    vaccineReducer: {vaccine, list},
    calendarReducer: {selectedCalendarDate},
    loginReducer: {user}
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* Effects */
  useEffect(() => {
    if (vaccine.id) {
      setValue('description', vaccine.description);
      setValue('note', vaccine.note);
      setStartDate(vaccine.date!);
      setReportImage((prevState) => ({...prevState, uri: vaccine.image!}));
    }
  }, [vaccine, setValue]);

  /* Functions */

  const clear = () => {
    setReportImage({...reportImage, uri: ''});
    setStartDate(moment().format('YYYY-MM-DD'));
    reset();
    dispatch(
      setVaccine({
        id: '',
        description: '',
        fileImage: undefined,
        image: '',
        date: '',
        note: '',
        animal: {}
      })
    );
  };

  const generateTemporalId = (): number => {
    if (list.length > 0) {
      return Math.floor(Math.random() * 100);
    }
    return 1;
  };

  const submit = async (form: FormData) => {
    const data = {
      ...form,
      calendar: selectedCalendarDate,
      userId: user.id!
    };
    dispatch(storeReminder(data));
    clear();
    handleStatus(false);
  };

  const remove = () => {
    if (isCreate) {
      const currentList = [...list];
      const newList = currentList.filter(
        (element) => element.id !== vaccine.id
      );
      dispatch(setVaccineArray([...newList]));
    } else {
      dispatch(deleteVaccine({id: Number(vaccine.id)!, animalId: animal.id!}));
    }
    clear();
    handleStatus(false);
  };

  const close = () => {
    clear();
    handleStatus(false);
  };

  return (
    <View>
      <Modal
        backdropColor="#95a5a6"
        useNativeDriver
        isVisible={status}
        onBackdropPress={close}>
        <View style={Styles.modalView}>
          <View>
            <Text style={Styles.title}>Evento</Text>
          </View>
          <ListItem style={Styles.listItemDetailContainer}>
            <View>
              <Input
                control={control}
                name="description"
                textAlign="right"
                required
                error={errors.description ? errors.description.message : null}
                placeholder="Description..."
              />
            </View>
          </ListItem>
          {/* <ListItem style={Styles.listItemDetailContainer}>
            <View>
              <Text>Empieza</Text>
            </View>
            <View>
              <Button
                transparent
                style={Styles.buttonDate}
                onPress={() => handleShowStartDate(true)}>
                <Text>{moment(startDate).format('YYYY-MM-DD')}</Text>
              </Button>
            </View>
          </ListItem>
          <ListItem style={Styles.listItemDetailContainer}>
            <View>
              <Text>Termina</Text>
            </View>
            <View>
              <Button
                transparent
                style={Styles.buttonDate}
                onPress={() => handleShowEndDate(true)}>
                <Text>{moment(endDate).format('HH:mm')}</Text>
              </Button>
            </View>
          </ListItem> */}
          <View style={Styles.buttonContainer}>
            <Button
              transparent
              style={Styles.saveButton}
              onPress={handleSubmit(submit)}>
              <Text style={Styles.buttonText}>Guardar </Text>
            </Button>
          </View>
          {vaccine.id !== '' && (
            <View style={Styles.buttonContainerRemove}>
              <Button transparent style={Styles.saveButton} onPress={remove}>
                <Text style={Styles.buttonText}>Elminar </Text>
              </Button>
            </View>
          )}
        </View>
      </Modal>
    </View>
  );
};

export {ReminderModal};
