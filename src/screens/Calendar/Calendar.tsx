import {DrawerNavigationProp} from '@react-navigation/drawer';
import {Button} from 'native-base';
import React, {useEffect, useState} from 'react';
import {Text, View} from 'react-native';
import {Agenda, DateObject} from 'react-native-calendars';
import moment from 'moment';

import CustomHeader from '../../components/Header';
import Styles from './styles';
import Reminder from './Reminder';
import {useDispatch, useSelector} from 'react-redux';
import sitemap from '../../route/sitemap';
import {setCalendarDate, getAllCalendar} from '../../reducers/calendar.reducer';
import {AppRootReducer} from '../../reducers';
import {IReminder} from '../../reducers/reminder.reducer';

interface IProps {
  navigation: DrawerNavigationProp<any>;
}

const Calendar: React.FC<IProps> = ({navigation}) => {
  /* States */
  const [items, setItems] = useState({});
  const [statusModal, setStatusModal] = useState<boolean>(false);

  /* Redux */
  const {
    calendarReducer: {selectedCalendarDate, calendarList, calendarListLoading},
    loginReducer: {user}
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  const timeToString = (time: any) => {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  };

  // const loadItems = (day: any) => {
  //   setTimeout(() => {
  //     for (let i = -15; i < 85; i++) {
  //       const time = day.timestamp + i * 24 * 60 * 60 * 1000;
  //       const strTime: any = timeToString(time);
  //       if (!items[strTime]) {
  //         items[strTime] = [];
  //         const numItems = Math.floor(Math.random() * 3 + 1);
  //         for (let j = 0; j < numItems; j++) {
  //           items[strTime].push({
  //             name: 'Item for ' + strTime + ' #' + j,
  //             height: Math.max(50, Math.floor(Math.random() * 150)),
  //           });
  //         }
  //       }
  //     }
  //     const newItems: any = {};
  //     Object.keys(items).forEach((key: any) => {
  //       newItems[key] = items[key];
  //     });
  //     setItems(newItems);
  //   }, 1000);
  // };

  const handleLeft = () => navigation.openDrawer();

  const handleSelectReminder = (reminder: IReminder) => {
    const list = [...calendarList];
    const currentDate = list.find(
      (element: any) => element.id === reminder.userCalendarId
    );
    if (currentDate) {
      dispatch(setCalendarDate(currentDate.date));
    }

    navigation.navigate(sitemap.reminder, {
      id: reminder.id,
      selectedDate: selectedCalendarDate
    });
  };

  const handleCreateReminder = () => {
    if (selectedCalendarDate) {
      navigation.navigate(sitemap.reminder, {
        id: null,
        selectedDate: selectedCalendarDate
      });
    }
  };

  const handleSelectedDate = async (date: DateObject) => {
    await dispatch(setCalendarDate(date.dateString));
  };

  const buildCalendar = () => {
    const list = [...calendarList];
    let newList = {};
    list.forEach((element: any) => {
      const date = element.date;
      const reminders = element.reminders;
      if (element.reminders && element.reminders.length > 0) {
        newList = {...newList, [date]: reminders};
      }
    });
    return newList;
  };

  const renderItem = (item: any) => (
    <Reminder reminder={item} handle={handleSelectReminder} />
  );

  /* Effects */

  useEffect(() => {
    dispatch(getAllCalendar(user.id!));
    dispatch(setCalendarDate(moment().format('YYYY-MM-DD')));
  }, []);

  return (
    <View style={Styles.root}>
      <CustomHeader
        handleLeft={handleLeft}
        leftIcon="bars"
        title="Calendario"
      />
      <Agenda
        items={calendarList ? buildCalendar() : undefined}
        renderItem={renderItem}
        onDayPress={(day: any) => handleSelectedDate(day)}
        refreshing={calendarListLoading}
        selected={moment().format('YYYY-MM-DD')}
        renderKnob={() => (
          <View>
            <Text style={Styles.calendarIndicatoricon}>Desplegar</Text>
          </View>
        )}
      />
      <View style={Styles.eventButtonContainer}>
        <Button style={Styles.eventButton} onPress={handleCreateReminder}>
          <Text style={Styles.eventButtonText}>Crear evento</Text>
        </Button>
      </View>
    </View>
  );
};

export {Calendar};
