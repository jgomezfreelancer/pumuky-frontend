import React, {useEffect, useState} from 'react';

import {Container, Content, Card, CardItem, Body} from 'native-base';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  Image,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
  Platform,
  PanResponder,
  NativeModules
} from 'react-native';
import ContentLoader, {Rect} from 'react-content-loader/native';
import * as RNIap from 'react-native-iap';

import CustomHeader from '../../components/Header';
import {pumukyBackground} from '../../assets/images';

import Styles from './styles';
import colors from '../../styles/colors';
import sitemap from '../../route/sitemap';
import {useDispatch, useSelector} from 'react-redux';
import {AppRootReducer} from '../../reducers';
import {
  clearAnimal,
  getAnimalsByUser,
  IAnimalResponse,
  setBottomTab,
  setIsCreate,
  setLoading
} from '../../reducers/animal.reducer';
import PremiumModal from './WarningModal';
import {setVaccineArray} from '../../reducers/vaccine.reducer';
import {setCareArray} from '../../reducers/care.reducer';
import {uniquePaymentPumukyID} from '../../utils/constans';

const items: any = Platform.select({
  android: [uniquePaymentPumukyID],
  ios: [uniquePaymentPumukyID]
});

const onStartShouldSetPanResponder = () => {};

const AnimalListSkeleton = (props: any): JSX.Element => (
  <ContentLoader
    speed={1}
    width={400}
    height={160}
    viewBox="0 0 400 160"
    backgroundColor="#bdc3c7"
    foregroundColor="#ecebeb"
    {...props}>
    <Rect x="0" y="20" rx="3" ry="3" width="100%" height="120" />
    <Rect x="0" y="150" rx="3" ry="3" width="100%" height="8" />
  </ContentLoader>
);

interface IProps {
  navigation: DrawerNavigationProp<any>;
}

const MyPets: React.FC<IProps> = ({navigation}): JSX.Element => {
  /* States */
  const [statusModal, setStatusModal] = useState<boolean>(false);
  const [products, setProducts] = useState([]);
  const [purchased, setPurchased] = useState<boolean>(false);

  /* Redux */
  const {
    animalReducer: {animalsByUser, loading},
    loginReducer: {user}
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* Effects */

  useEffect(() => {
    dispatch(getAnimalsByUser(user.id!));
    dispatch(clearAnimal());
    dispatch(setVaccineArray([]));
    dispatch(setCareArray([]));
    dispatch(setBottomTab(0));
  }, [dispatch, user]);

  const DevMenuTrigger = ({children}) => {
    const {DevMenu} = NativeModules;
    const panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => {
        if (gestureState.numberActiveTouches === 3) {
          DevMenu.show();
        }
      }
    });
    return (
      <View style={{flex: 1}} {...panResponder.panHandlers}>
        {children}
      </View>
    );
  };

  /* Effects */
  useEffect(() => {
    setLoading(true);
    RNIap.initConnection().then(async () => {
      RNIap.getProducts(items)
        .then(async (res: any) => {
          setProducts(res);
          setLoading(false);
        })
        .catch(() => {});
    });

    /* Active Mannually developer menu */
    // const {DevMenu} = NativeModules;
    // DevMenu.show();
  }, []);

  /* Functions */
  const handleLeft = () => navigation.openDrawer();

  const handleEdit = (id: number) => {
    dispatch(setIsCreate(false));
    dispatch(setBottomTab(0));
    navigation.navigate(sitemap.animal, {id});
  };

  const handleStatusModal = (value: boolean) => setStatusModal(value);

  const createAnimal = () => {
    dispatch(clearAnimal());
    dispatch(setVaccineArray([]));
    dispatch(setCareArray([]));
    dispatch(setIsCreate(true));
    dispatch(setBottomTab(0));
    navigation.navigate(sitemap.animal, {id: null});
  };

  const handleCreateAnimal = async () => {
    // const purchaseHistory: RNIap.ProductPurchase[] = await RNIap.getPurchaseHistory();
    // const currentHistory = [...purchaseHistory];
    // const currentPaymentUniqueProducts = currentHistory.filter(
    //   (payment: RNIap.ProductPurchase) =>
    //     payment.productId === uniquePaymentPumukyID
    // );

    if (!user.petsLimit) {
      createAnimal();
    } else {
      if (animalsByUser.length < user.petsLimit!) {
        createAnimal();
      } else {
        setStatusModal(true);
      }
    }
  };

  return (
    <Container>
      <ImageBackground source={pumukyBackground} style={Styles.background}>
        <CustomHeader
          handleLeft={handleLeft}
          leftIcon="bars"
          title="Mis mascotas"
        />

        <Content style={Styles.content}>
          {loading ? (
            <View>
              <AnimalListSkeleton />
              <AnimalListSkeleton />
              <AnimalListSkeleton />
            </View>
          ) : (
            <>
              {animalsByUser.map((element: IAnimalResponse, i: number) => {
                const image: any = element.image
                  ? {
                      uri: element.image
                    }
                  : pumukyBackground;
                return (
                  <Card key={i}>
                    <CardItem style={Styles.cardItem}>
                      <Body>
                        <TouchableOpacity
                          onPress={() => handleEdit(element.id!)}
                          activeOpacity={1}
                          style={{width: '100%'}}>
                          <Image style={Styles.image} source={image} />
                        </TouchableOpacity>
                      </Body>
                    </CardItem>
                    <CardItem footer style={Styles.cardItemFooter}>
                      <Text style={Styles.cardItemFooterText}>
                        {element.name}
                      </Text>
                    </CardItem>
                  </Card>
                );
              })}
              <View style={Styles.plusContainer}>
                <View style={Styles.plusButtonContainer}>
                  <TouchableOpacity
                    style={Styles.plusButton}
                    onPress={handleCreateAnimal}>
                    <Icon size={30} color={colors.blue} name="plus" />
                  </TouchableOpacity>
                </View>
              </View>
            </>
          )}
        </Content>
        <PremiumModal status={statusModal} handleStatus={handleStatusModal} />
      </ImageBackground>
    </Container>
  );
};

export {MyPets};
