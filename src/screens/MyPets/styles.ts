import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    position: 'relative',
    backgroundColor: '#e7f0fe'
  },
  cardItem: {paddingLeft: 0, paddingRight: 0, paddingTop: 0},
  cardItemFooter: {paddingTop: 0},
  cardItemFooterText: {fontWeight: 'bold', color: colors.greyDark},
  content: {paddingLeft: 10, paddingRight: 10},
  plusButton: {
    borderRadius: 25,
    padding: 10,
    paddingTop: 7,
    paddingBottom: 7,
    borderWidth: 1,
    borderColor: '#d9d9d9',
    backgroundColor: 'white',
    shadowColor: '#000000',
    shadowOpacity: 0.3,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 1
    }
  },
  plusButtonContainer: {
    elevation: 10,
    borderRadius: 25
  },
  image: {width: '100%', height: 200},
  icon: {
    borderColor: colors.transparent,
    borderWidth: 2
  },
  plusContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 30
  }
});
