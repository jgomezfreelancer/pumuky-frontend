import {StyleSheet, Dimensions} from 'react-native';
import colors from '../../../styles/colors';

const {width} = Dimensions.get('window');

export default StyleSheet.create({
  amountTitle: {
    fontSize: 25,
    color: colors.blue,
    fontWeight: 'bold',
  },
  centeredRankingView: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    alignSelf: 'center',
    marginLeft: 10,
  },
  content: {
    backgroundColor: 'white',
    padding: 20,
    flexWrap: 'nowrap',
    width,
    justifyContent: 'center',
    flexDirection: 'column',
    paddingTop: 5,
  },
  buttonText: {
    color: colors.blue,
    fontWeight: 'bold',
  },
  buttonsContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  divider: {
    borderWidth: 2,
    width: '15%',
    borderColor: 'grey',
    borderRadius: 15,
  },
  dividerContainer: {
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  featureContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 10,
  },
  featureTitleContainer: {marginLeft: 10},
  featureTitle: {fontSize: 18, color: colors.grey, fontWeight: 'bold'},
  modalView: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 15,
  },
  nextButtonContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 10,
    marginLeft: -20,
    marginRight: -20,
    marginTop: 20,
  },
  nextButton: {width: '100%', backgroundColor: colors.blue, padding: 10},
  nextButtonTitle: {textAlign: 'center', fontSize: 16, color: colors.white},
  optionTitle: {
    fontSize: 16,
    margin: 10,
  },
  onePay: {
    fontSize: 20,
    color: '#C7C7C7',
    marginBottom: 10,
  },
  textContainer: {marginBottom: 20},
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    color: 'black',
  },
  title2: {
    fontSize: 20,
    fontStyle: 'italic',
    color: colors.blue,
    textAlign: 'center',
    marginBottom: 10,
  },
});
