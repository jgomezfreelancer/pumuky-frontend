import React, {useEffect, useState} from 'react';

import Modal from 'react-native-modal';
import * as RNIap from 'react-native-iap';
import Toast from 'react-native-toast-message';
import {Platform, Text, TouchableOpacity, View} from 'react-native';
import {appleAuth} from '@invertase/react-native-apple-authentication';

import Styles from './styles';
import {refreshUser, updateSuscription} from '../../../reducers/login.reducer';
import {useDispatch, useSelector} from 'react-redux';
import {AppRootReducer} from '../../../reducers';
import {
  clearAnimal,
  setBottomTab,
  setIsCreate
} from '../../../reducers/animal.reducer';
import {setVaccineArray} from '../../../reducers/vaccine.reducer';
import {setCareArray} from '../../../reducers/care.reducer';
import {useNavigation} from '@react-navigation/core';
import sitemap from '../../../route/sitemap';
import {
  inAppPurchaseSecret,
  uniquePaymentPumukyID
} from '../../../utils/constans';
import {isIOS} from '../../../helpers/isIOS';

const items: any = Platform.select({
  android: [uniquePaymentPumukyID],
  ios: [uniquePaymentPumukyID]
});

interface Iprops {
  status: boolean;
  handleStatus: (status: boolean) => void;
}

const PremiumModal: React.FC<Iprops> = ({
  status,
  handleStatus
}): JSX.Element => {
  /* States */
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState<boolean>(false);

  /* Router */
  const navigation = useNavigation();

  /* Redux */
  const {
    loginReducer: {user}
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* Functions */

  const handleCreateAnimal = async () => {
    dispatch(clearAnimal());
    dispatch(setVaccineArray([]));
    dispatch(setCareArray([]));
    dispatch(setIsCreate(true));
    dispatch(setBottomTab(0));
    navigation.navigate(sitemap.animal, {id: null});
  };

  const handleSuscription = async () => {
    if (products.length > 0) {
      const sku = products[0];
      setLoading(true);

      const requestPurchaseRes = await RNIap.requestPurchase(
        sku['productId'],
        false
      );
      console.log('requestPurchaseRes ', requestPurchaseRes);
      if (Platform.OS === 'android') {
        if (requestPurchaseRes && requestPurchaseRes.transactionReceipt) {
          await RNIap.consumePurchaseAndroid(
            requestPurchaseRes.purchaseToken || ''
          );
          updateSuscription({id: user.id || 0, isMember: true})(dispatch).then(
            () => {
              dispatch(refreshUser(user.id!));
              handleCreateAnimal();
              setLoading(false);
            }
          );
          handleStatus(false);
        } else {
          updateSuscription({id: user.id || 0, isMember: false})(dispatch).then(
            () => {
              dispatch(refreshUser(user.id!));
              setLoading(false);
            }
          );
          handleStatus(false);
        }
      }
    } else {
      setLoading(false);
      Toast.show({
        text1: 'Compra cancelada, intente nuevamente',
        type: 'error',
        position: 'bottom'
      });
    }
  };

  const close = () => {
    setLoading(false);
    handleStatus(false);
  };

  const validate = async (receip: string) => {
    const body = {
      'receipt-data': receip,
      'password:': inAppPurchaseSecret
    };
    const result = await RNIap.validateReceiptIos(body, true);
    return result;
  };

  const onAppleButtonPress = async () => {
    // performs login request
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: appleAuth.Operation.LOGIN,
      requestedScopes: []
    });

    // get current authentication state for user
    // /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
    const credentialState = await appleAuth.getCredentialStateForUser(
      appleAuthRequestResponse.user
    );
    // use credentialState response to ensure the user is authenticated
    if (credentialState === appleAuth.State.AUTHORIZED) {
      handleSuscription();
    }
  };

  /* Effects */
  useEffect(() => {
    RNIap.initConnection().then(async (res: any) => {
      RNIap.getProducts(items)
        .then(async (res: any) => {
          setProducts(res);
        })
        .catch((err: any) => new Error(err));
    });

    RNIap.purchaseUpdatedListener((purchase: RNIap.ProductPurchase) => {
      if (isIOS) {
        const receipt = purchase.transactionReceipt;
        const isValid = validate(receipt);
        if (isValid) {
          RNIap.clearTransactionIOS();
          updateSuscription({id: user.id || 0, isMember: true})(dispatch).then(
            () => {
              dispatch(refreshUser(user.id!));
              handleCreateAnimal();
              setLoading(false);
            }
          );
          handleStatus(false);
        } else {
          Toast.show({
            text1: 'La compra no fue procesada, intente de nuevo',
            type: 'error',
            position: 'bottom'
          });
        }
      }
    });
    RNIap.purchaseErrorListener(async (err: any) => {
      navigation.navigate(sitemap.myPets);
      setLoading(false);
      if (err.code && err.code === 'E_USER_CANCELLED') {
        Toast.show({
          text1: 'Compra cancelada',
          type: 'error',
          position: 'bottom'
        });
      }
    });
  }, []);

  return (
    <View>
      <Modal useNativeDriver isVisible={status} onBackdropPress={close}>
        <View style={Styles.modalView}>
          <View style={Styles.textContainer}>
            <Text style={Styles.title}>Añade otra mascota</Text>
          </View>
          <View>
            <Text style={Styles.title2}>
              Por cada mascota que quieras añadir en Pumuky
            </Text>
          </View>
          <View>
            <View style={Styles.featureContainer}>
              <Text style={Styles.onePay}>Único pago de</Text>
              <Text style={Styles.amountTitle}>1,99€</Text>
            </View>
            <View style={Styles.nextButtonContainer}>
              <TouchableOpacity
                onPress={isIOS ? onAppleButtonPress : handleSuscription}
                disabled={loading}
                style={Styles.nextButton}>
                <Text style={Styles.nextButtonTitle}>SUSCRIBIRME</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export {PremiumModal};
