import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  careContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    width: '100%',
    paddingLeft: 10
  },
  careTitleContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    width: '70%'
  },
  careTitle: {
    fontSize: 16,
    color: colors.blackLight,
    marginLeft: 10
  },
  content: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    display: 'flex'
  },
  checkBox: {
    borderRadius: 5
  },
  divider: {color: colors.grey, fontWeight: 'bold', fontSize: 16},
  dayActive: {color: colors.blue, fontWeight: 'bold', fontSize: 16},
  daySingle: {color: colors.grey, fontWeight: 'bold', fontSize: 16},
  daysContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    width: '30%'
  },
  dayTitle: {
    fontSize: 16,
    color: colors.blackLight
  },
  titleContainer: {
    display: 'flex',
    justifyContent: 'space-around',
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    paddingLeft: 20
  },
  titleTextContainer: {width: '50%'},
  titleText: {fontSize: 20, fontWeight: 'bold', color: colors.blackLight},
  plusIconContainer: {
    width: '50%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'flex-end',
    paddingRight: 20
  },
  plusContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 30
  },
  plusButton: {
    borderRadius: 25,
    padding: 10,
    paddingTop: 7,
    paddingBottom: 7,
    borderColor: '#d9d9d9',
    backgroundColor: 'white',
    shadowColor: '#000000',
    shadowOpacity: 0.3,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 1
    }
  },
  plusButtonContainer: {
    elevation: 10,
    borderRadius: 25
  }
});
