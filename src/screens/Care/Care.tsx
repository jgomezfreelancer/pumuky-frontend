import React, { useState } from 'react';

import { DrawerNavigationProp } from '@react-navigation/drawer';
import { Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import moment from 'moment';

import CustomHeader from '../../components/Header';
import { Button, Container, Content, ListItem, CheckBox } from 'native-base';
import FooterBottomNavigator from '../../components/FooterBottomNavigator';
import sitemap from '../../route/sitemap';
import colors from '../../styles/colors';
import CareModal from './CareModal';

import Styles from './styles';
import { useDispatch, useSelector } from 'react-redux';
import { AppRootReducer } from '../../reducers';
import {
  ICare,
  days,
  setCare,
  updateCareChecked,
} from '../../reducers/care.reducer';
import WarningModal from '../Animal/WarningModal';
import { removeAnimal } from '../../reducers/animal.reducer';
import { TouchableOpacity } from 'react-native-gesture-handler';

interface IProps {
  navigation: DrawerNavigationProp<any>;
}

const Care: React.FC<IProps> = ({ navigation }): JSX.Element => {
  /* States */
  const [statusModal, setStatusModal] = useState<boolean>(false);
  const [selectedCares, setSelectedCares] = useState<string[]>([]);
  const [statusWarningModal, setStatusWarningModal] = useState<boolean>(false);
  const [option, setOption] = useState<string>('all')

  /* Redux */
  const {
    careReducer: { list },
    animalReducer: { isCreate, animal },
    loginReducer: { user },
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* Functions */
  const handleLeft = () => navigation.navigate(sitemap.myPets);

  const handleStatusModal = (value: boolean) => setStatusModal(value);

  const handleStatusWarningModal = (value: boolean) =>
    setStatusWarningModal(value);

  const onChangeCheck = (careItem: string) => {
    const currentList = [...selectedCares];
    const exist = currentList.find((element) => element === careItem);
    if (exist) {
      const newList = currentList.filter((element) => element !== careItem);
      setSelectedCares(newList);
      dispatch(
        updateCareChecked({
          id: Number(careItem),
          animalId: animal.id!,
          date: null,
        }),
      );
    } else {
      const newList = [...selectedCares];
      newList.push(careItem);
      setSelectedCares(newList);
      dispatch(
        updateCareChecked({
          id: Number(careItem),
          animalId: animal.id!,
          date: moment().format('YYYY-MM-DD'),
        }),
      );
    }
  };

  const getNotes = (notes: number[]) => {
    let string = '';
    notes.forEach((element: number, i: number) => {
      const day = days.find(
        (dayElement) => dayElement.value === Number(element),
      );
      string += `${notes.length === i + 1 ? `${day?.description}` : `${day?.description}, `
        }`;
    });
    return string;
  };

  const handleEditCare = (care: ICare) => {
    dispatch(setCare(care));
    setStatusModal(true);
  };

  const remove = () => {
    setStatusWarningModal(true);
  };

  const handleConfirm = () => {
    removeAnimal({ id: animal.id!, user: user.id! })(dispatch).then(() => {
      setStatusWarningModal(false);
      navigation.navigate(sitemap.myPets);
    });
  };

  const getCheckedDate = (currentCare: ICare) => {
    if (currentCare.checked_date) {
      const now = moment().format('YYYY-MM-DD');
      const date = moment(currentCare.checked_date).format('YYYY-MM-DD');
      if (now === date) {
        return true;
      } else {
        updateCareChecked({
          id: Number(currentCare.id),
          animalId: animal.id!,
          date: null,
        });
        return false;
      }
    }
    return false;
  };

  const handleOption = (currentOption: string) => setOption(currentOption)

  const getCurrentList = (): ICare[] => {
    const currentList = [...list];
    if (option === 'today') {
      const newlist: ICare[] = []
      const newdate = moment()
      const currentDay = newdate.day();
      currentList.forEach(element => {
        if (element.note && element.note.length > 0) {
          const isToday = element.note.find((element: string) => element === String(currentDay));
          if (isToday) {
            newlist.push({ ...element, note: [currentDay] });
          }
        }

      });
      return newlist;
    }
    return currentList;
  }

  return (
    <Container>
      <CustomHeader
        handleLeft={handleLeft}
        leftIcon="arrow-left"
        title="Cuidados"
        rightIcon={isCreate ? undefined : 'trash'}
        rightColor="white"
        handleRigth={isCreate ? null : remove}
      />
      <Content>
        <ListItem>
          <View style={Styles.titleContainer}>
            <View style={Styles.titleTextContainer}>
              <Text style={Styles.titleText}>{animal.name}</Text>
            </View>
            <View style={Styles.plusIconContainer}>
              <Button transparent onPress={() => handleOption('all')} >
                <Text style={option === 'all' ? Styles.dayActive : Styles.daySingle} >Todos</Text>
              </Button>
              <Text style={Styles.divider} > | </Text>
              <Button transparent onPress={() => handleOption('today')} >
                <Text style={option === 'today' ? Styles.dayActive : Styles.daySingle}>Hoy</Text>
              </Button>
            </View>
          </View>
        </ListItem>
        {getCurrentList().length > 0 && getCurrentList().map((element: ICare, i: number) => {
          const checkDate = getCheckedDate(element);
          return (
            <ListItem key={i}>
              <View style={Styles.careContainer}>
                <View style={Styles.careTitleContainer}>
                  <CheckBox
                    checked={!!checkDate}
                    onPress={() => onChangeCheck(element.id!)}
                    color={colors.blackLight}
                    style={Styles.checkBox}
                  />
                  <Button transparent onPress={() => handleEditCare(element)}>
                    <Text style={Styles.careTitle}>{element.description}</Text>
                  </Button>
                </View>
                <View style={Styles.daysContainer}>
                  <View>
                    <Text style={Styles.dayTitle}>
                      {getNotes(element.note!)}
                    </Text>
                  </View>
                </View>
              </View>
            </ListItem>
          );
        })}
        <View style={Styles.plusContainer}>
          <View style={Styles.plusButtonContainer} >
            <TouchableOpacity style={Styles.plusButton} onPress={() => handleStatusModal(true)}>
              <Icon size={30} color={colors.blue} name="plus" />
            </TouchableOpacity>
          </View>
        </View>
      </Content>
      <WarningModal
        status={statusWarningModal}
        handleConfirm={handleConfirm}
        handleStatus={handleStatusWarningModal}
      />
      <CareModal status={statusModal} handleStatus={handleStatusModal} />
      <FooterBottomNavigator />
    </Container>
  );
};

export { Care };
