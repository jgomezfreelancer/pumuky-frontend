import React, {useEffect, useState} from 'react';
import Modal from 'react-native-modal';
import {KeyboardAvoidingView, Text, View} from 'react-native';
import {Button, ListItem} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {v4 as uuidv4} from 'uuid';

import Styles from './styles';
import {useDispatch, useSelector} from 'react-redux';
import {AppRootReducer} from '../../../reducers';
import Input from '../../../components/common/form/Input';
import {useForm} from 'react-hook-form';
import colors from '../../../styles/colors';
import {
  setCare,
  setCareArray,
  setList,
  updateCare,
  storeCare,
  deleteCare
} from '../../../reducers/care.reducer';

interface Days {
  value: number;
  description: string;
}

export const days: Days[] = [
  {value: 1, description: 'Lu'},
  {value: 2, description: 'Ma'},
  {value: 3, description: 'Mi'},
  {value: 4, description: 'Ju'},
  {value: 5, description: 'Vi'},
  {value: 6, description: 'Sá'},
  {value: 0, description: 'Do'}
];

type FormData = {
  description: string;
  date: string;
  note: string;
};

interface Iprops {
  status: boolean;
  handleStatus: (status: boolean) => void;
}

const CareModal: React.FC<Iprops> = ({status, handleStatus}): JSX.Element => {
  /* Form data  */
  const {control, errors, handleSubmit, setValue, reset} = useForm<FormData>();
  const [selectedDays, setSelectedDays] = useState<number[]>([]);

  /* Redux */
  const {
    animalReducer: {isCreate, animal},
    careReducer: {care, list}
  } = useSelector((state: AppRootReducer) => state);
  const dispatch = useDispatch();

  /* Functions */

  const clear = () => {
    setSelectedDays([]);
    reset();
    dispatch(
      setCare({
        id: '',
        description: '',
        note: [],
        animal: {}
      })
    );
  };

  const generateTemporalId = (): number => {
    if (list.length > 0) {
      return Math.floor(Math.random() * 100);
    }
    return 1;
  };

  const submit = async (form: FormData) => {
    if (isCreate) {
      const data = {
        id: generateTemporalId(),
        description: form.description,
        note: selectedDays.slice().sort((a: any, b: any) => a - b)
      };
      if (care.id === '') {
        dispatch(setList(data));
      } else {
        const currentList = [...list];
        const newList = currentList.filter((element) => element.id !== care.id);
        dispatch(setCareArray([data, ...newList]));
      }
    } else {
      const currentNotes = selectedDays.slice().sort((a: any, b: any) => a - b);
      const data = {
        id: care.id,
        description: form.description,
        note: currentNotes.map((element) => Number(element)),
        animalId: animal.id
      };
      if (care.id) {
        dispatch(updateCare(data));
      } else {
        dispatch(storeCare(data));
      }
    }
    clear();
    handleStatus(false);
  };

  const remove = () => {
    if (isCreate) {
      const currentList = [...list];
      const newList = currentList.filter((element) => element.id !== care.id);
      dispatch(setCareArray([...newList]));
    } else {
      dispatch(deleteCare({id: Number(care.id)!, animalId: animal.id!}));
    }
    clear();
    handleStatus(false);
  };

  const onChangeDays = (day: number) => {
    const currentList = [...selectedDays];
    const exist = currentList.find((element: any) => element === String(day));
    if (exist) {
      const newList = currentList.filter(
        (element: any) => String(element) !== String(day)
      );
      setSelectedDays(newList);
    } else {
      const newList = [...selectedDays];
      newList.push(String(day));
      setSelectedDays(newList);
    }
  };

  const close = () => {
    clear();
    handleStatus(false);
  };

  /* Effects */

  useEffect(() => {
    if (care.id !== '') {
      setValue('description', care.description);
      setSelectedDays(care.note!);
    }
  }, [setValue, care]);

  return (
    <View>
      <Modal
        backdropColor="#95a5a6"
        useNativeDriver
        isVisible={status}
        onBackdropPress={close}>
        <KeyboardAvoidingView behavior="position" enabled>
          <View style={Styles.modalView}>
            <ListItem style={Styles.listItemDetailContainer}>
              <Text style={Styles.title}>
                {care.id ? 'Cuidado' : 'Nuevo cuidado'}
              </Text>
            </ListItem>
            <ListItem style={Styles.listItemDetailContainer}>
              <View>
                <Text style={Styles.fieldTitle}>Titulo</Text>
              </View>
              <View>
                <Input
                  control={control}
                  name="description"
                  textAlign="right"
                  required
                  error={errors.description ? errors.description.message : null}
                  placeholder="Titulo..."
                />
              </View>
            </ListItem>
            <ListItem style={Styles.listaItemDaysContainer}>
              <View style={Styles.noteContainer}>
                <Text style={Styles.noteText}>Nota</Text>
              </View>
              <View style={Styles.daysContainer}>
                {days.map((day: Days, i: number) => {
                  const checked = selectedDays.find(
                    (selectedDay: any) =>
                      Number(selectedDay) === day.value ||
                      (day.value === 0 && Number(selectedDay) === day.value)
                  );
                  return (
                    <View key={i} style={Styles.dayContainer}>
                      <View style={Styles.dayTextContainer}>
                        <Text style={Styles.dayText}>{day.description}</Text>
                      </View>
                      <View style={Styles.dayButtonContainer}>
                        <Button
                          style={Styles.buttonDay}
                          onPress={() => onChangeDays(day.value)}>
                          <View style={Styles.buttonCircle} />
                          {checked && (
                            <Icon
                              name="circle"
                              size={20}
                              color={colors.blue}
                              solid
                              style={Styles.dayButtonIcon}
                            />
                          )}
                        </Button>
                      </View>
                    </View>
                  );
                })}
              </View>
            </ListItem>
            <View style={Styles.buttonContainer}>
              <Button
                transparent
                style={Styles.saveButton}
                onPress={handleSubmit(submit)}>
                <Text style={Styles.buttonText}>Guardar </Text>
              </Button>
            </View>
            {care.id !== '' && (
              <View style={Styles.buttonContainerRemove}>
                <Button transparent style={Styles.saveButton} onPress={remove}>
                  <Text style={Styles.buttonText}>Elminar </Text>
                </Button>
              </View>
            )}
          </View>
        </KeyboardAvoidingView>
      </Modal>
    </View>
  );
};

export {CareModal};
