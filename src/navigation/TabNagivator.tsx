// ./navigation/TabNavigator.js

import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import sitemap from '../route/sitemap';
import Home from '../screens/Home';
import MyPets from '../screens/MyPets';

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name={sitemap.home} component={Home} />
      <Tab.Screen name={sitemap.myPets} component={MyPets} />
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;
