import ActivePaw from './menu-pumuki-paw-activo.svg';
import InactivePaw from './menu-pumuki-paw-inactivo.svg';
import ActiveSyringe from './menu-pumuki-jeringa-activo.svg';
import InactiveSyringe from './menu-pumuki-jeringa-inactivo.svg';
import ActivePlate from './menu-pumuki-plato-activo.svg';
import InactivePlate from './menu-pumuki-plato-inactivo.svg';

export const ActivePawIcon = ActivePaw;
export const InactivePawIcon = InactivePaw;
export const ActiveSyringeIcon = ActiveSyringe;
export const InactiveSyringeIcon = InactiveSyringe;
export const ActivePlateIcon = ActivePlate;
export const InactivePlateIcon = InactivePlate;
