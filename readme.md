# React Native App

## Features

- ReactJS-Typescript with Hooks
- Interfaces and Types
- Auth Login with API REST
- Forms validations with Hooks
- Redux and Redux-Thunk

## Install

In the project directory, you can run:

### `yarn `

## Environment

### copy env and set the values according your needs

### execute command:`cp env-example.ts env.ts`

## Run Android

In the project directory need in two terminals

### `yarn start `

### `yarn android`

after command please r command only if is necesary
