import React from 'react';
import {Provider} from 'react-redux';

import CreateStore from './src/config/store';
import Router from './src/config/router';
import 'react-native-gesture-handler';

const store = CreateStore();

const App = () => {
  return (
    <Provider store={store}>
      <Router />
    </Provider>
  );
};

export default App;
