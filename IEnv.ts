interface IServer {
  url: string;
}

export interface IFirebase {
  apiKey: string;
  projectId: string;
  messagingSenderId: string;
  appId: string;
  databaseURL: string;
  storageBucket: string;
}

interface IChat {
  room: string;
  alert: string;
}

interface IWebSocket {
  chat: IChat;
}

export interface IEnv {
  server: IServer;
  firebase: IFirebase;
}
